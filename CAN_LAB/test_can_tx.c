#include "unity.h"
#include "c_period_callbacks.h"

#include "Mockc_switches.h"
//#include "Mockc_led_display.h"
#include "Mockcan.h"

void test_c_period_init(void)
{
    switches_init_ExpectAndReturn(true);
    //led_display_init_ExpectAndReturn(true);
    CAN_init_ExpectAndReturn(can1, 250, 25, 25, NULL, NULL, true);
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can1);
    TEST_ASSERT_TRUE(C_period_init());
}

void test_c_period_1hz(void)
{
    CAN_is_bus_off_ExpectAndReturn(can1, true);
    CAN_reset_bus_Expect(can1);
    C_period_1Hz(0);
}

void test_c_period_10hz(void)
{
    switches_get_ExpectAndReturn(1, true);
    CAN_tx_ExpectAndReturn(can1, NULL, 0, true);
    CAN_tx_IgnoreArg_msg();
    C_period_10Hz(0);
}
