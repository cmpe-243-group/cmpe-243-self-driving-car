/**
 * @file

 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "c_uart2.h"
#include "c_uart3.h"
#include "c_light_sensor.h"
#include "c_led_display.h"
#include "can.h"





bool C_period_init(void) {
#if 0
    //transmitter
    //uart2_init(38400, 10, 10);
    //receiver
    uart3_init(38400, 10, 10);
    //light sensor
    //light_sensor_init();

    return true;
#endif
    led_display_init();
if(CAN_init(can1, 100, 25, 25, NULL, NULL )){

    CAN_bypass_filter_accept_all_msgs();

    CAN_reset_bus(can1);
    return true;

}

    return false;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
}

void C_period_10Hz(uint32_t count) {
    (void) count;
#if 0
    //Tx Code
    char str[10];
    uint16_t light_sensor_reading = light_sensor_get_raw_value();
    sprintf(str, "%s %d \n", "LS: ", light_sensor_reading);
    //send new line character as well
    for(uint8_t i = 0; i < strlen(str) + 1; i++)
    {
        uart2_putchar(str[i], 0);
    }
#endif
}

void C_period_100Hz(uint32_t count) {
#if 0
    (void) count;
    //Rx Code
    char byte = 0;
    if (uart3_getchar(&byte, 0)) {
        printf("%c", byte);
    }
#endif

    (void)count;
//#ifdef CAN_LAB

    can_msg_t rx_Msg = {0};
    if(CAN_rx(can1, &rx_Msg, 0)){

        printf("Message %#x successfully received\n", rx_Msg.data.bytes[0]);


        if(rx_Msg.data.bytes[0] == 0xAA)
        {
            led_display_set_right_digit('A');
            led_display_set_left_digit('A');
        }
        else
        {
            led_display_set(rx_Msg.data.bytes[0]);

        }
    }

}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}
