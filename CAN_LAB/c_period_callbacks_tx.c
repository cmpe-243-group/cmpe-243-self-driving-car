/**
 * @file

 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */

//#define UART_LAB
#define CAN_LAB

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
//#include "FreeRTOS.h" //for portMAX_DELAY
//#include "printf_lib.h"
#ifdef UART_LAB
#include <string.h>
#include "c_uart2.h"
#include "c_uart3.h"
#include "c_light_sensor.h"
#endif
#ifdef CAN_LAB
#include "c_switches.h"
#include "c_led_display.h"
#include "can.h"
#endif


bool C_period_init(void) {
    //init switch for transmitter board
    switches_init();

    //Transmitter: init CAN1 with 250k baud rate, 25 Rx Queue entries, 25 Tx Queue entries
    if(CAN_init(can1, 100, 25, 25, NULL, NULL))
    {
        //for both CAN controllers, set up filter to accept all messages
        CAN_bypass_filter_accept_all_msgs();
        //Make CAN1 active on CAN bus by sending reset
        CAN_reset_bus(can1);
        return true;
    }
    return false;
}
bool C_period_reg_tlm(void) {
    return true;
}

//every 1000ms
void C_period_1Hz(uint32_t count) {
    (void) count;
#ifdef CAN_LAB
    if(CAN_is_bus_off(can1))
    {
        CAN_reset_bus(can1);
    }
#endif
}

//every 100ms
void C_period_10Hz(uint32_t count) {
    (void) count;

   //transmitter
    can_msg_t txMsg = {0};
    txMsg.msg_id = 0x10;
    txMsg.frame_fields.data_len = 1; //Send 1 byte

    if(switches_get(1))
    {
        txMsg.data.bytes[0] = 0xAA;
    }

    (CAN_tx(can1, &txMsg, 0)) ? printf("Message %#x successfully sent\n", txMsg.data.bytes[0]) : printf("Message %#x failed to send\n", txMsg.data.bytes[0]);
}

//every 10ms
void C_period_100Hz(uint32_t count) {
    (void) count;

}

//every 1ms
void C_period_1000Hz(uint32_t count) {
    (void) count;
}
