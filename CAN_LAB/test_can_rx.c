
//rx code

#include "unity.h"
#include "c_period_callbacks.h"

#include "Mockc_led_display.h"
#include "Mockcan.h"

void test_c_period_init(void)
{
    led_display_init_ExpectAndReturn(true);
    CAN_init_ExpectAndReturn(can1, 100, 25, 25, NULL, NULL, true);
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can1);
    TEST_ASSERT_TRUE(C_period_init());
}

void test_C_period_1Hz(void)
{
    CAN_is_bus_off_ExpectAndReturn(can1, true);
    CAN_reset_bus_Expect(can1);
    C_period_1Hz(0);
}

void test_c_period_100hz(void)
{
    can_msg_t rxMsg = {0};
    rxMsg.data.bytes[0] = 0xAA;
    CAN_rx_ExpectAndReturn(can1, NULL, 0, true);
    CAN_rx_IgnoreArg_msg();
    CAN_rx_ReturnThruPtr_msg(&rxMsg);
    led_display_set_right_digit_Expect('A');
    led_display_set_left_digit_Expect('A');
    C_period_100Hz(0);

    rxMsg.data.bytes[0] = 0x00;
    CAN_rx_ExpectAndReturn(can1, NULL, 0, true);
    CAN_rx_IgnoreArg_msg();
    CAN_rx_ReturnThruPtr_msg(&rxMsg);
    led_display_set_Expect(0);
    C_period_100Hz(0);

}


