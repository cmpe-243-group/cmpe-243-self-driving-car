#include "c_led_display.h"
#include "singleton_template.hpp"
#include "LED_Display.hpp"

bool led_display_init()
{
    return LED_Display::getInstance().init();
}

void led_display_set(char num)
{
    LED_Display::getInstance().setNumber(num);
}

void led_display_set_right_digit(char alpha)
{
    LED_Display::getInstance().setRightDigit(alpha);
}
void led_display_set_left_digit(char alpha)
{
    LED_Display::getInstance().setLeftDigit(alpha);
}





