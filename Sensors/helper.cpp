/*
 * helper.cpp
 *
 *  Created on: Mar 3, 2019
 *      Author: Sag
 */
#include <stdio.h>
#include "io.hpp"
#include "gpio.hpp"
#include "can.h"

#include "c_code/helper.h"
#include "printf_lib.h"



bool canInit(void){
    bool init_can = CAN_init(can1,100,500,500,0,0);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
    if (init_can) puts("can init success\n");
    return init_can;
}

void resetCanIfOff(void){
    if(CAN_is_bus_off(can1)){
        puts("resetting\n");
        CAN_reset_bus(can1);
    }
}

void toggle_led(uint8_t ledNum)
{
    LE.toggle(ledNum);
}
void led_on(uint8_t ledNum)
{
    LE.on(ledNum);
}
void led_off(uint8_t ledNum)
{
    LE.off(ledNum);
}

void set_number(char num)
{
    LED_Display::getInstance().setNumber(num);
}
