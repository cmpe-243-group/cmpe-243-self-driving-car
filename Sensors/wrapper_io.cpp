/*
 * wrapper_io.cpp
 *
 *  Created on: May 18, 2019
 *      Author: Sag
 */

#include <stdio.h>
#include "io.hpp"
#include "gpio.hpp"
#include "c_code/wrapper_io.h"

void toggle_led(uint8_t ledNum)
{
    LE.toggle(ledNum);
}
void led_on(uint8_t ledNum)
{
    LE.on(ledNum);
}
void led_off(uint8_t ledNum)
{
    LE.off(ledNum);
}
void set_number(char num)
{
    LED_Display::getInstance().setNumber(num);
}
