/*
 * wrapper_io.h
 *
 *  Created on: May 18, 2019
 *      Author: Sag
 */

#ifndef WRAPPER_IO_H_
#define WRAPPER_IO_H_

#ifdef __cplusplus
extern "C"{
#endif

#include <stdio.h>

void toggle_led(uint8_t ledNum);

void led_on(uint8_t ledNum);

void led_off(uint8_t ledNum);

void set_number(char num);

#ifdef __cplusplus
}
#endif
#endif /* WRAPPER_IO_H_ */
