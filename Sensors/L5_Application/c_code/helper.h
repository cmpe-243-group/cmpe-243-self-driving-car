/*
 * helper.h
 *
 *  Created on: Mar 3, 2019
 *      Author: Sag
 */

#ifndef HELPER_H_
#define HELPER_H_

#ifdef __cplusplus
extern "C"{
#endif

#include "can.h"
#include <stdint.h>
#include "can.h"

//extern SENSOR_STATUS_t;



//bool receive_sensor(SENSOR_STATUS_t *to);

//void send_on_can(uint8_t cmd_to_motor,MOTOR_CMD_t* to);

bool canInit(void);

void resetCanIfOff(void);

void toggle_led(uint8_t ledNum);

bool sendOnCan(uint64_t data,can_msg_t msg);

void led_on(uint8_t ledNum);            
void led_off(uint8_t ledNum);

void set_number(char num);

#ifdef __cplusplus
}
#endif
#endif /* HELPER_H_ */
