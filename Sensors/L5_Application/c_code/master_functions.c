/*
 * master_helper.c
 *
 *  Created on: Apr 2, 2019
 *      Author: Sag
 */
#if 0
#include "master_functions.h"
#include <stdio.h>
#include <stdbool.h>
#include "printf_lib.h"
#include "can.h"
#include "wrapper_io.h"
//#include "c"
#include "string.h"

////////////////////     mia setting
const uint32_t                             RPM_VALUE_CMD__MIA_MS = 1000;
const RPM_VALUE_CMD_t                      RPM_VALUE_CMD__MIA_MSG = {0};
const uint32_t                             SENSOR_STATUS__MIA_MS = 2000;
const SENSOR_STATUS_t                      SENSOR_STATUS__MIA_MSG = {0};
const uint32_t                             BRIDGE_STOP__MIA_MS = 1000;
const BRIDGE_STOP_t                        BRIDGE_STOP__MIA_MSG = {0};
const uint32_t                             BRIDGE_GO__MIA_MS = 1000;
const BRIDGE_GO_t                          BRIDGE_GO__MIA_MSG = {0};
const uint32_t                             COMPASS_CMD__MIA_MS = 1000;
const COMPASS_CMD_t                        COMPASS_CMD__MIA_MSG = {0};

////////////////////    generated can struct setting

SENSOR_STATUS_t sensor = {0};
RPM_VALUE_CMD_t rpm_val = {0};
BRIDGE_GO_t bridge_go = {0};
BRIDGE_STOP_t bridge_stop = {0};
static MOTOR_CMD_t motor_cmd = {steer_straight,0,0,0,0,0};
GPS_TARGET_HEADING_t target_heading_S = {0};
COMPASS_CMD_t current_heading_S = {0};


////////////////////    local file variables & flags

static STEER_CMD_enum_E cmd_to_motor = steer_straight;
static float distance = 0;
static bool bridge_active = false;
static bool gps_active = false;
static bool slow = false;
static bool reverse_active = false;

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(can1, &can_msg, 0);
}

void init_sucess(void){
    motor_cmd.MASTER_INIT_DEBUG = 1;
}

void master_rx(void){

    can_msg_t can_msg;
    led_off(2);
    led_off(3);


    while(CAN_rx(can1, &can_msg, 0)){
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        if(dbc_decode_SENSOR_STATUS(&sensor,can_msg.data.bytes,&can_msg_hdr)){
        }
        if(dbc_decode_RPM_VALUE_CMD(&rpm_val,can_msg.data.bytes,&can_msg_hdr)){
        }
        if(dbc_decode_BRIDGE_STOP(&bridge_stop,can_msg.data.bytes,&can_msg_hdr)){
            bridge_active = false;
        }
        if(dbc_decode_BRIDGE_GO(&bridge_go,can_msg.data.bytes,&can_msg_hdr)){
            bridge_active = true;
        }
        if(dbc_decode_COMPASS_CMD(&current_heading_S,can_msg.data.bytes,&can_msg_hdr)){

        }
        if(dbc_decode_GPS_TARGET_HEADING(&target_heading_S,can_msg.data.bytes,&can_msg_hdr)){
            gps_active = true;
        }
    }
    if(dbc_handle_mia_SENSOR_STATUS(&sensor,10)){
        led_on(4);
        //set_number(5);
    }
    if(dbc_handle_mia_RPM_VALUE_CMD(&rpm_val,10)){
        led_on(4);
        //set_number(5);
    }

}

void process_received_data(void){
    if(bridge_active){

        cmd_to_motor = get_steer_command(sensor.SENSOR_LEFT,sensor.SENSOR_FRONT,sensor.SENSOR_RIGHT,0);

        if(gps_active){

            try_to_steer_E steer_try;
            steer_try = process_target_heading(current_heading_S.HEADING,target_heading_S.TARGET_HEADING);

            if(steer_try == try_right && sensor.SENSOR_RIGHT>1000){
                cmd_to_motor = steer_right;
            }
            if(steer_try == try_left && sensor.SENSOR_LEFT>1000){
                cmd_to_motor = steer_left;
            }
            if(target_heading_S.DISTANCE>distance){
                set_number(9);
            }
            else if(target_heading_S.DISTANCE<distance){
                set_number(1);
            }
            if(target_heading_S.DISTANCE < 5){

                cmd_to_motor = stop;
                distance = target_heading_S.DISTANCE;
            }
        }
        motor_cmd.STEER_CMD_enum = cmd_to_motor;
        motor_cmd.MASTER_INIT_DEBUG = 1;
        motor_cmd.SPEED_CMD = get_rpm_cmd(rpm_val.RPM_VALUE);
    }
    else{
        motor_cmd.STEER_CMD_enum = stop;
        motor_cmd.SPEED_CMD = 0;
    }
}

try_to_steer_E process_target_heading(float current_heading, float target_heading){

    try_to_steer_E steer_try = try_straight;

    float difference = target_heading - current_heading;
    if((difference)>15 && (difference<=180)){
        steer_try = try_right;
    }
    else if(((difference)<-15) && ((difference)>-180)){
        steer_try = try_left;
    }
#if 0
    if(difference==0){
        steer_try = try_straight;
    }
#endif
#if 1
    if(difference<15 && difference>-15){
        steer_try = try_straight;
    }
#endif

    return steer_try;
}

void master_tx(void){
    set_debug_messages();
    if(dbc_encode_and_send_MOTOR_CMD(&motor_cmd))
     {   led_on(3);}
}

void set_debug_messages(void){
    motor_cmd.MASTER_SEND_LEFT = 0;
    motor_cmd.MASTER_SEND_RIGHT = 0;
    motor_cmd.MASTER_SEND_STRAIGHT = 0;
    if(cmd_to_motor == 2){
        motor_cmd.MASTER_SEND_STRAIGHT = 1;
    }
    if(cmd_to_motor == 3 || cmd_to_motor == 6){
        motor_cmd.MASTER_SEND_RIGHT = 1;
    }
    if(cmd_to_motor == 1 || cmd_to_motor == 4){
        motor_cmd.MASTER_SEND_LEFT = 1;
    }
}

float get_rpm_cmd(uint8_t rpm_value){
    float rpm_cmd;
    //flat rps = 6 or 7
    if(rpm_value<3) rpm_cmd = 2.2;
    else if(rpm_value<6 && rpm_value>=3) rpm_cmd = 1.9;
    else if(rpm_value>=8 && rpm_value<9) rpm_cmd = 1.3;
    else if(rpm_value>=9)rpm_cmd = 0.7;
    else rpm_cmd = 1.6;

    if(slow){
        rpm_cmd -= 0.6;
        if(rpm_cmd < 0.7){
            rpm_cmd = 0.7;
        }
    }
    slow = false;
    return rpm_cmd;
}

STEER_CMD_enum_E get_steer_command(uint16_t l, uint16_t c, uint16_t r,uint8_t count){
    if(c<1000){
        slow = true;
    }
    if(c>800){
        if(l<600 || r<600){
            if(l>r){cmd_to_motor = slight_left;}
            else {cmd_to_motor = slight_right;}
        }
        else if(l<100 || r<100){
            cmd_to_motor = brake;}
        else cmd_to_motor = steer_straight;
    }
    else if(c<=800){
        if(c<500){
            if(reverse_active){
                decide_reverse_mode(1,l,r);
            }
            if(!reverse_active){
                cmd_to_motor = brake;
                reverse_active = true;
            }
        }
        else{
           if(l>r){cmd_to_motor = steer_left;}
           else {cmd_to_motor = steer_right;}
        }
    }
    if(cmd_to_motor==6 || cmd_to_motor<5){
        reverse_active = false;
    }
    return cmd_to_motor;
}

#if 1
void decide_reverse_mode(int rps,uint16_t l,uint16_t r){
    if(l<r){cmd_to_motor = left_reverse;}
    else {cmd_to_motor = right_reverse;}
    if(rps == 0) cmd_to_motor = reverse;
}
#endif


#endif
