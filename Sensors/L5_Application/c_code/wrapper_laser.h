/*
 * wrapper_laser.h
 *
 *  Created on: May 16, 2019
 *      Author: Sag
 */

#ifndef WRAPPER_LASER_H_
#define WRAPPER_LASER_H_

#ifdef __cplusplus
extern "C"{
#endif

#include <stdint.h>
#include <stdbool.h>

void reset_Xshut(void);

bool i2c_init(unsigned int speedInKhz);

uint8_t readReg_laser(uint8_t device_address,uint8_t register_address);

bool writeReg_laser(uint8_t deviceAddress, uint8_t registerAddress, uint8_t value);

bool check_device_response(uint8_t device_address);

void setCenterHigh(void);

void setLeftHigh(void);

void setRightHigh(void);

#ifdef __cplusplus
}
#endif
#endif /* WRAPPER_LASER_H_ */
