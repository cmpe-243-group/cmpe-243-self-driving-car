/*
 * laser_sensor.c
 *
 *  Created on: Mar 29, 2019
 *      Author: Sag
 */
#if 1
#include <stdio.h>
#include <stdbool.h>
#include "laser_sensor.h"
#include "wrapper_io.h"
#include "can.h"
#include "wrapper_laser.h"
#include "string.h"
#include "printf_lib.h"

SENSOR_STATUS_t sensor = {0};

Laser_addresses_S sensors_ptr={0};

Laser_values_S laser_values = {0};

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(can1, &can_msg, 0);
}

void sensor_100Hz(void){

}

/// init functions

void init_sensor_addresses(void){
    Laser_addresses_S *ptr=&sensors_ptr;
    ptr->center = 0x55;  //82
    ptr->cur = 0x52;
    ptr->left = 0x44;     //68
    ptr->right = 0x66;    //102
}

void reset_laser_xShut(void){
    reset_Xshut();
}

bool init_i2c_device(unsigned int speedInKhz){
    return i2c_init(speedInKhz);
}

bool set_laser_address(void){
    Laser_addresses_S *sensor_ptr=&sensors_ptr;

    bool ret = true;
    setLeftHigh();
    if(check_i2c_response(DEFAULT_SENSOR_ADDR))
    {
       u0_dbg_printf("left sensor recognized with default addr\n");
        if(write_i2c_reg(DEFAULT_SENSOR_ADDR,I2C_SLAVE_DEVICE_ADDRESS,((sensor_ptr->left >> 1) & 0x7F)))
                    {u0_dbg_printf("address changed to %x\n",sensor_ptr->left);}
    }

    setRightHigh();
    if(check_i2c_response(DEFAULT_SENSOR_ADDR))
    {
       u0_dbg_printf("right sensor recognized with default addr\n");
        if(write_i2c_reg(DEFAULT_SENSOR_ADDR,I2C_SLAVE_DEVICE_ADDRESS,((sensor_ptr->right >> 1) & 0x7F)))
           u0_dbg_printf("address changed to %x\n",sensor_ptr->right);
    }
    if(check_i2c_response(0x66) && !(check_i2c_response(DEFAULT_SENSOR_ADDR))){
       u0_dbg_printf("Device 0x66 is only sensor on i2c bus\n");
    }

    setCenterHigh();
    if(check_i2c_response(DEFAULT_SENSOR_ADDR))
    {
       u0_dbg_printf("center sensor recognized with default addr\n");
        if(write_i2c_reg(DEFAULT_SENSOR_ADDR,I2C_SLAVE_DEVICE_ADDRESS,((sensor_ptr->center >> 1) & 0x7F)))
                    {u0_dbg_printf("address changed to %x\n",sensor_ptr->center);}
    }
    return ret;
}

void init_sensors(void){
    i2c_init_sensors(sensors_ptr.center);
    i2c_init_sensors(sensors_ptr.left);
    i2c_init_sensors(sensors_ptr.right);
}

bool i2c_init_sensors(uint8_t I2CAddr_Laser_sensor){

    bool ret_val=false;
    write_i2c_reg(I2CAddr_Laser_sensor,0x88,0x00);
    write_i2c_reg(I2CAddr_Laser_sensor,0x80,0x01);
    write_i2c_reg(I2CAddr_Laser_sensor,0xFF,0x01);
    write_i2c_reg(I2CAddr_Laser_sensor,0x00,0x00);

    uint8_t stop_var = read_i2c_reg(I2CAddr_Laser_sensor,0x91);
    write_i2c_reg(I2CAddr_Laser_sensor,0x91,stop_var);

    write_i2c_reg(I2CAddr_Laser_sensor,0x00,0x01);
    write_i2c_reg(I2CAddr_Laser_sensor,0xFF,0x00);
    write_i2c_reg(I2CAddr_Laser_sensor,0x80,0x00);
    ret_val= write_i2c_reg(I2CAddr_Laser_sensor,0x00,0x01);
    if(ret_val)u0_dbg_printf("laser sensor at addr: %#x init\n", I2CAddr_Laser_sensor);
    return ret_val;
}

/////// 100Hz functions
void take_readings(int count){
    uint16_t val=0;

    Laser_addresses_S *addr_ptr = &sensors_ptr;
    Laser_values_S *value_ptr = &laser_values;

    //right & left
    //if(count%2==0)
    {
    val = readRangeSingleMillimeters(addr_ptr->right);
    value_ptr->right_sensor_val = val;
    if(val!=20){
        value_ptr->prev_right = val;
    }
    if(val==20){
        value_ptr->right_sensor_val = value_ptr->prev_right;
    }

    val = readRangeSingleMillimeters(addr_ptr->left);
    value_ptr->left_sensor_val = val;
    if(val!=20){
        value_ptr->prev_left = val;
    }
    if(val==20){
        value_ptr->left_sensor_val = value_ptr->prev_left;
    }
    }

    //center
    {
    val = readRangeSingleMillimeters(addr_ptr->center);
    value_ptr->center_sensor_val = val;
    if(val!=20){
        value_ptr->prev_cen = val;
    }
    if(val==20){
        value_ptr->center_sensor_val = value_ptr->prev_cen;
    }
    }

}

void sensor_send_on_can(void){
    sensor.SENSOR_FRONT = get_sensor_val(center);
    sensor.SENSOR_RIGHT = get_sensor_val(right);
    sensor.SENSOR_LEFT  = get_sensor_val(left);
    sensor.SENSORS_HEARTBEAT = 1;

    set_debug_messages();

    dbc_encode_and_send_SENSOR_STATUS(&sensor);
}

//helpers for 100Hz
uint16_t readRangeSingleMillimeters(uint8_t I2CAddr_Laser_sensor){

    write_i2c_reg(I2CAddr_Laser_sensor,0x80,0x01);
    write_i2c_reg(I2CAddr_Laser_sensor,0xFF,0x01);
    write_i2c_reg(I2CAddr_Laser_sensor,0x00,0x00);

    uint8_t stop_var = read_i2c_reg(I2CAddr_Laser_sensor,0x91);
    write_i2c_reg(I2CAddr_Laser_sensor,0x91,stop_var);

    write_i2c_reg(I2CAddr_Laser_sensor,0x00,0x01);
    write_i2c_reg(I2CAddr_Laser_sensor,0xFF,0x00);
    write_i2c_reg(I2CAddr_Laser_sensor,0x80,0x00);
    write_i2c_reg(I2CAddr_Laser_sensor,0x00,0x01);

    uint16_t val1 = read_i2c_reg(I2CAddr_Laser_sensor,0x1E);
    uint16_t val2 = read_i2c_reg(I2CAddr_Laser_sensor,0x1F);
    uint16_t val = (uint16_t) val1 << 8;
    val |= (val2);
    return val;
}

void set_debug_messages(void){
    if(sensor.SENSOR_FRONT<400){sensor.SENSOR_CENTER_BLOCKED = 1;}
    if(sensor.SENSOR_LEFT<400){sensor.SENSOR_LEFT_BLOCKED = 1;}
    if(sensor.SENSOR_RIGHT<400){sensor.SENSOR_RIGHT_BLOCKED = 1;}

}

uint16_t get_sensor_val(sensors_t l){
    led_off(1);
    led_off(2);
    led_off(3);
    Laser_values_S *ptr = &laser_values;
    uint16_t ret=500;

    if(l==left){
        ret = ptr->left_sensor_val;
        if(ret < 400)  led_on(1);
    }
    if(l==right){
        ret = ptr->right_sensor_val;
        if(ret < 400)   led_on(3);
    }
    if(l==center){
        ret = ptr->center_sensor_val;
        if(ret < 400)   led_on(2);
    }
    return ret;
}

/// laser i2c base wrapper calls
uint8_t read_i2c_reg(uint8_t device_address,uint8_t register_address){
    return readReg_laser(device_address,register_address);
}

bool write_i2c_reg(uint8_t deviceAddress, uint8_t registerAddress, uint8_t value){
    return writeReg_laser(deviceAddress,registerAddress,value);
}

bool check_i2c_response(uint8_t device_address){
    return check_device_response(device_address);
}

//can functions
bool canInit(void){
    bool init_can = CAN_init(can1,100,500,500,0,0);
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
    if (init_can) puts("can init success\n");
    return init_can;
}

void resetCanIfOff(void){
    if(CAN_is_bus_off(can1)){
        puts("resetting\n");
        CAN_reset_bus(can1);
    }
}
#endif
