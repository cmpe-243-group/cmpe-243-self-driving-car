#define motor1 0
#define master 0
#define sensor1 1

#if master
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "c_period_callbacks.h"
#include "helper.h"
#include "master_functions.h"
#include "string.h"
#include "printf_lib.h"
//#include "can.h"

bool C_period_init(void) {
    bool init_can = canInit();
    if(init_can)init_sucess();
    return init_can;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    resetCanIfOff();
}

void C_period_10Hz(uint32_t count) {
    (void) count;
}

void C_period_100Hz(uint32_t count) {
    (void) count;

    master_rx();
    process_received_data();
    master_tx();
}


void C_period_1000Hz(uint32_t count) {
    (void) count;
}

#endif

#if sensor1
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "c_period_callbacks.h"
#include "laser_sensor.h"
#include "string.h"
#include "printf_lib.h"


bool C_period_init(void) {

    canInit();
    init_sensor_addresses();
    reset_laser_xShut();
    init_i2c_device(200);
    bool ret_init = set_laser_address();
    init_sensors();

    return ret_init;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    resetCanIfOff();
}

void C_period_10Hz(uint32_t count) {
    (void) count;
}

void C_period_100Hz(uint32_t count) {
    (void) count;
    //sensor_100Hz();
    take_readings(count);
    sensor_send_on_can();

}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}
#endif


#if motor1
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "c_code/helper.h"
#include "c_code/laser_sensor.h"
#include "rpm_wrapper.h"
#include "motor_09Apr.h"
#include "calibrate.h"
#include "printf_lib.h"
#include "motor_rx.h"


bool C_period_init(void) {
    //u0_dbg_printf("init called\n");
    c_PWM_init();
    c_rpm_init();
    canInit();
    return true;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    int n = c_getRpm();
    n++;
}

void C_period_100Hz(uint32_t count) {
    //(void) count;
    CAN_Recieve();
}

void C_period_10Hz(uint32_t count) {
    (void) count;

    //c_execute();
}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}

#endif
