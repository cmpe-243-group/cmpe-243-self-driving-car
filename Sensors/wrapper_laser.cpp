/*
 * wrapper_laser.cpp
 *
 *  Created on: May 16, 2019
 *      Author: Sag
 */
#include <stdio.h>
#include <stdbool.h>
#include "c_code/wrapper_laser.h"
#include "i2c2.hpp"
//#include "c_code/laser_sensor.h"
//#include "string.h"
#include "gpio.hpp"
#include "printf_lib.h"
#include "io.hpp"

    GPIO centerXShut(P0_29);
    GPIO leftXShut(P0_30);
    GPIO rightXShut(P1_19);

void reset_Xshut(void){
    centerXShut.setAsOutput();
    leftXShut.setAsOutput();
    rightXShut.setAsOutput();
    centerXShut.setLow();
    leftXShut.setLow();
    rightXShut.setLow();
}


bool i2c_init(unsigned int speedInKhz){
    bool ret = I2C2::getInstance().init(200);
    if(ret)u0_dbg_printf("i2c init success\n");
    return ret;
}

uint8_t readReg_laser(uint8_t device_address,uint8_t register_address){
    uint8_t ret = I2C2::getInstance().readReg(device_address,register_address);
    return ret;
}

bool writeReg_laser(uint8_t deviceAddress, uint8_t registerAddress, uint8_t value){
    bool write = I2C2::getInstance().writeReg(deviceAddress,registerAddress,value);
    return write;
}

bool check_device_response(uint8_t device_address){
    uint8_t return_val = I2C2::getInstance().checkDeviceResponse(device_address);
    return return_val;
}

void setCenterHigh(void){
    u0_dbg_printf("cen high\n");
    centerXShut.setHigh();
}

void setLeftHigh(void){
    u0_dbg_printf("left high\n");
    leftXShut.setHigh();
}

void setRightHigh(void){
    u0_dbg_printf("right high\n");
    rightXShut.setHigh();
}

