/*
 * laser_sensor.cpp
 *
 *  Created on: Mar 29, 2019
 *      Author: Sag
 */

#include <stdio.h>
#include <stdbool.h>
#include "c_code/laser_sensor.h"
#include "i2c2.hpp"
#include "c_code/helper.h"
#include "gpio.hpp"
#include "printf_lib.h"
#include "io.hpp"

    GPIO centerXShut(P0_29);
    GPIO leftXShut(P0_30);
    GPIO rightXShut(P1_19);

Laser_addresses_S sensors_ptr={0};

Laser_values_S laser_values = {0};

void sensor_100Hz(void){

}

void temp_one_sensor(void){
    Laser_values_S* value_ptr = &laser_values;
    value_ptr->center_sensor_val = readRangeSingleMillimeters(0x52);

    if(value_ptr->center_sensor_val!=20){
        value_ptr->left_sensor_val = value_ptr->center_sensor_val;
    }
    if(value_ptr->center_sensor_val==20){
        u0_dbg_printf("\n");
        value_ptr->center_sensor_val = value_ptr->left_sensor_val;
    }
    u0_dbg_printf("val %lu\n",value_ptr->center_sensor_val);
}

void reset_GPIO(void){
    centerXShut.setAsOutput();
    leftXShut.setAsOutput();
    rightXShut.setAsOutput();
    centerXShut.setLow();
    leftXShut.setLow();
    rightXShut.setLow();
}

bool i2c_init(unsigned int speedInKhz){
    bool ret = I2C2::getInstance().init(200);
    if(ret)u0_dbg_printf("i2c init success\n");
    return ret;
}

void init_sensor_addresses(void){
    Laser_addresses_S *ptr=&sensors_ptr;
    ptr->center = 0x55;  //82
    ptr->cur = 0x52;
    ptr->left = 0x44;     //68
    ptr->right = 0x66;    //102
}

void init_sensors(void){
    i2c_init_sensors(sensors_ptr.center);
    i2c_init_sensors(sensors_ptr.left);
    i2c_init_sensors(sensors_ptr.right);
}

bool i2c_init_sensors(uint8_t I2CAddr_Laser_sensor){

    bool ret_val=false;
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x88,0x00);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x80,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0xFF,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x00,0x00);

    uint8_t stop_var = readReg_laser(I2CAddr_Laser_sensor,0x91);
    writeReg_laser(I2CAddr_Laser_sensor,0x91,stop_var);

    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x00,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0xFF,0x00);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x80,0x00);
    ret_val= writeReg_laser(I2CAddr_Laser_sensor,0x00,0x01);
    if(ret_val)u0_dbg_printf("laser sensor at addr: %#x init\n", I2CAddr_Laser_sensor);
    return ret_val;
}

bool check_sensor_response(uint8_t device_address){
    bool ret = I2C2::getInstance().checkDeviceResponse(device_address);
    //if (ret)   puts("device responds\n");
    return ret;
}

bool check_sensors_status(void){
    bool status = true;
    status &= check_sensor_response(sensors_ptr.center);
    status &= check_sensor_response(sensors_ptr.left);
    status &= check_sensor_response(sensors_ptr.right);
    return status;
}

bool set_laser_address(void){
    Laser_addresses_S *sensor_ptr=&sensors_ptr;

    bool ret = true;

    rightXShut.setHigh();
    if(I2C2::getInstance().checkDeviceResponse(DEFAULT_SENSOR_ADDR))
    {
       u0_dbg_printf("right sensor recognized with default addr\n");
        if(I2C2::getInstance().writeReg(DEFAULT_SENSOR_ADDR,I2C_SLAVE_DEVICE_ADDRESS,((sensor_ptr->right >> 1) & 0x7F)))
           u0_dbg_printf("address changed to %x\n",sensor_ptr->right);
    }
    if(I2C2::getInstance().checkDeviceResponse(0x66) && !(I2C2::getInstance().checkDeviceResponse(DEFAULT_SENSOR_ADDR))){
       u0_dbg_printf("Device 0x66 is only sensor on i2c bus\n");
    }

    leftXShut.setHigh();
    if(I2C2::getInstance().checkDeviceResponse(DEFAULT_SENSOR_ADDR))
    {
       u0_dbg_printf("left sensor recognized with default addr\n");
        if(I2C2::getInstance().writeReg(DEFAULT_SENSOR_ADDR,I2C_SLAVE_DEVICE_ADDRESS,((sensor_ptr->left >> 1) & 0x7F)))
                    {printf("address changed to %x\n",sensor_ptr->left);}
    }

    centerXShut.setHigh();

    if(I2C2::getInstance().checkDeviceResponse(DEFAULT_SENSOR_ADDR))
    {
       u0_dbg_printf("center sensor recognized with default addr\n");
        if(I2C2::getInstance().writeReg(DEFAULT_SENSOR_ADDR,I2C_SLAVE_DEVICE_ADDRESS,((sensor_ptr->center >> 1) & 0x7F)))
                    {printf("address changed to %x\n",sensor_ptr->center);}
    }
    return ret;
}


uint16_t get_sensor_val(sensors_t l){
    led_off(1);
    led_off(2);
    led_off(3);
    Laser_values_S *ptr = &laser_values;
    uint16_t ret=500;
    //u0_dbg_printf("\n");
    if(l==left){
        ret = ptr->left_sensor_val;
        if(ret < 400)  led_on(1);
        //u0_dbg_printf("a L: %d  ",ret);
    }
    if(l==right){
        ret = ptr->right_sensor_val;
        if(ret < 400)   led_on(3);
        //u0_dbg_printf("a R: %d  ",ret);
    }
    if(l==center){
        ret = ptr->center_sensor_val;
        if(ret < 400)   led_on(2);
        //u0_dbg_printf("a c: %d\n",ret);
    }
    return ret;
}

void take_readings(int count){
    uint16_t val=0;

    Laser_addresses_S *addr_ptr = &sensors_ptr;
    Laser_values_S *value_ptr = &laser_values;

    //right
    if(count%2==0){
    val = readRangeSingleMillimeters(addr_ptr->right);
    value_ptr->right_sensor_val = val;
    if(val!=20){
        value_ptr->prev_right = val;
    }
    if(val==20){
        value_ptr->right_sensor_val = value_ptr->prev_right;
    }

    val = readRangeSingleMillimeters(addr_ptr->left);
    value_ptr->left_sensor_val = val;
    if(val!=20){
        value_ptr->prev_left = val;
    }
    if(val==20){
        value_ptr->left_sensor_val = value_ptr->prev_left;
    }
    }

    //left
    if(count%3==1){

    }

    //center
    if(count%2==1){
    val = readRangeSingleMillimeters(addr_ptr->center);
    value_ptr->center_sensor_val = val;
    if(val!=20){
        value_ptr->prev_cen = val;
    }
    if(val==20){
        value_ptr->center_sensor_val = value_ptr->prev_cen;
    }
    }

}


uint16_t readRangeSingleMillimeters(uint8_t I2CAddr_Laser_sensor){
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x80,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0xFF,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x00,0x00);

    uint8_t stop_var = readReg_laser(I2CAddr_Laser_sensor,0x91);
    writeReg_laser(I2CAddr_Laser_sensor,0x91,stop_var);

    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x00,0x01);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0xFF,0x00);
    I2C2::getInstance().writeReg(I2CAddr_Laser_sensor,0x80,0x00);
    writeReg_laser(I2CAddr_Laser_sensor,0x00,0x01);

    uint16_t val1 = readReg_laser(I2CAddr_Laser_sensor,0x1E);
    uint16_t val2 = readReg_laser(I2CAddr_Laser_sensor,0x1F);
    uint16_t val = (uint16_t) val1 << 8;
    val |= (val2);
    return val;
}

uint8_t readReg_laser(uint8_t device_address,uint8_t register_address){
    uint8_t ret = I2C2::getInstance().readReg(device_address,register_address);
    return ret;
}

bool writeReg_laser(uint8_t deviceAddress, uint8_t registerAddress, uint8_t value){
    bool write = I2C2::getInstance().writeReg(deviceAddress,registerAddress,value);
    return write;
}


