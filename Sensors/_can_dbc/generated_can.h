/// DBC file: ../_can_dbc/243.dbc    Self node: 'SENSOR'  (ALL = 0)
/// This file can be included by a source file, for example: #include "generated.h"
#ifndef __GENEARTED_DBC_PARSER
#define __GENERATED_DBC_PARSER
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>



/// Extern function needed for dbc_encode_and_send()
extern bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]);

/// Missing in Action structure
typedef struct {
    uint32_t is_mia : 1;          ///< Missing in action flag
    uint32_t mia_counter_ms : 31; ///< Missing in action counter
} dbc_mia_info_t;

/// CAN message header structure
typedef struct { 
    uint32_t mid; ///< Message ID of the message
    uint8_t  dlc; ///< Data length of the message
} dbc_msg_hdr_t; 

// static const dbc_msg_hdr_t MOTOR_CMD_HDR =                        {  100, 3 };
// static const dbc_msg_hdr_t RPM_VALUE_CMD_HDR =                    {  105, 4 };
// static const dbc_msg_hdr_t COMPASS_CMD_HDR =                      {  110, 4 };
// static const dbc_msg_hdr_t COMPASS_INIT_DEBUG_HDR =               {  115, 1 };
// static const dbc_msg_hdr_t GPS_CURRENT_LAT_LONG_HDR =             {  120, 8 };
// static const dbc_msg_hdr_t GPS_INIT_DEBUG_HDR =                   {  125, 1 };
// static const dbc_msg_hdr_t GPS_FIX_DEBUG_HDR =                    {  130, 1 };
// static const dbc_msg_hdr_t COMPASS_MAN_CAL_DEBUG_HDR =            {  135, 1 };
// static const dbc_msg_hdr_t GPS_HEARTBEAT_HDR =                    {  140, 1 };
// static const dbc_msg_hdr_t GPS_TARGET_HEADING_HDR =               {  145, 8 };
// static const dbc_msg_hdr_t GPS_CHECKPOINT_INDEX_HDR =             {  150, 1 };
static const dbc_msg_hdr_t SENSOR_STATUS_HDR =                    {  200, 7 };
// static const dbc_msg_hdr_t BRIDGE_STOP_HDR =                      {  300, 1 };
// static const dbc_msg_hdr_t BRIDGE_GO_HDR =                        {  310, 1 };
// static const dbc_msg_hdr_t BRIDGE_INIT_DEBUG_HDR =                {  315, 1 };
// static const dbc_msg_hdr_t BRIDGE_HEARTBEAT_HDR =                 {  320, 1 };
// static const dbc_msg_hdr_t BRIDGE_DEST_HDR =                      {  325, 8 };




/// Message: SENSOR_STATUS from 'SENSOR', DLC: 7 byte(s), MID: 200
typedef struct {
    uint16_t SENSOR_FRONT;                    ///< B15:0   Destination: BRIDGE,MASTER
    uint16_t SENSOR_LEFT;                     ///< B31:16   Destination: BRIDGE,MASTER
    uint16_t SENSOR_RIGHT;                    ///< B47:32   Destination: BRIDGE,MASTER
    uint8_t SENSOR_LEFT_BLOCKED : 1;          ///< B48:48   Destination: BRIDGE,MASTER
    uint8_t SENSOR_CENTER_BLOCKED : 1;        ///< B49:49   Destination: BRIDGE,MASTER
    uint8_t SENSOR_RIGHT_BLOCKED : 1;         ///< B50:50   Destination: BRIDGE,MASTER
    uint8_t SENSORS_HEARTBEAT : 1;            ///< B51:51   Destination: BRIDGE,MASTER

    // No dbc_mia_info_t for a message that we will send
} SENSOR_STATUS_t;


/// @{ These 'externs' need to be defined in a source file of your project
/// @}


/// Not generating code for dbc_encode_MOTOR_CMD() since the sender is MASTER and we are SENSOR

/// Not generating code for dbc_encode_RPM_VALUE_CMD() since the sender is MOTOR and we are SENSOR

/// Not generating code for dbc_encode_COMPASS_CMD() since the sender is GPS and we are SENSOR

/// Not generating code for dbc_encode_COMPASS_INIT_DEBUG() since the sender is GPS and we are SENSOR

/// Not generating code for dbc_encode_GPS_CURRENT_LAT_LONG() since the sender is GPS and we are SENSOR

/// Not generating code for dbc_encode_GPS_INIT_DEBUG() since the sender is GPS and we are SENSOR

/// Not generating code for dbc_encode_GPS_FIX_DEBUG() since the sender is GPS and we are SENSOR

/// Not generating code for dbc_encode_COMPASS_MAN_CAL_DEBUG() since the sender is GPS and we are SENSOR

/// Not generating code for dbc_encode_GPS_HEARTBEAT() since the sender is GPS and we are SENSOR

/// Not generating code for dbc_encode_GPS_TARGET_HEADING() since the sender is GPS and we are SENSOR

/// Not generating code for dbc_encode_GPS_CHECKPOINT_INDEX() since the sender is GPS and we are SENSOR

/// Encode SENSOR's 'SENSOR_STATUS' message
/// @returns the message header of this message
static inline dbc_msg_hdr_t dbc_encode_SENSOR_STATUS(uint8_t bytes[8], SENSOR_STATUS_t *from)
{
    uint32_t raw;
    bytes[0]=bytes[1]=bytes[2]=bytes[3]=bytes[4]=bytes[5]=bytes[6]=bytes[7]=0;

    raw = ((uint32_t)(((from->SENSOR_FRONT)))) & 0xffff;
    bytes[0] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B0
    bytes[1] |= (((uint8_t)(raw >> 8) & 0xff)); ///< 8 bit(s) starting from B8

    raw = ((uint32_t)(((from->SENSOR_LEFT)))) & 0xffff;
    bytes[2] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B16
    bytes[3] |= (((uint8_t)(raw >> 8) & 0xff)); ///< 8 bit(s) starting from B24

    raw = ((uint32_t)(((from->SENSOR_RIGHT)))) & 0xffff;
    bytes[4] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B32
    bytes[5] |= (((uint8_t)(raw >> 8) & 0xff)); ///< 8 bit(s) starting from B40

    raw = ((uint32_t)(((from->SENSOR_LEFT_BLOCKED)))) & 0x01;
    bytes[6] |= (((uint8_t)(raw) & 0x01)); ///< 1 bit(s) starting from B48

    raw = ((uint32_t)(((from->SENSOR_CENTER_BLOCKED)))) & 0x01;
    bytes[6] |= (((uint8_t)(raw) & 0x01) << 1); ///< 1 bit(s) starting from B49

    raw = ((uint32_t)(((from->SENSOR_RIGHT_BLOCKED)))) & 0x01;
    bytes[6] |= (((uint8_t)(raw) & 0x01) << 2); ///< 1 bit(s) starting from B50

    raw = ((uint32_t)(((from->SENSORS_HEARTBEAT)))) & 0x01;
    bytes[6] |= (((uint8_t)(raw) & 0x01) << 3); ///< 1 bit(s) starting from B51

    return SENSOR_STATUS_HDR;
}

/// Encode and send for dbc_encode_SENSOR_STATUS() message
static inline bool dbc_encode_and_send_SENSOR_STATUS(SENSOR_STATUS_t *from)
{
    uint8_t bytes[8];
    const dbc_msg_hdr_t hdr = dbc_encode_SENSOR_STATUS(bytes, from);
    return dbc_app_send_can_msg(hdr.mid, hdr.dlc, bytes);
}



/// Not generating code for dbc_encode_BRIDGE_STOP() since the sender is BRIDGE and we are SENSOR

/// Not generating code for dbc_encode_BRIDGE_GO() since the sender is BRIDGE and we are SENSOR

/// Not generating code for dbc_encode_BRIDGE_INIT_DEBUG() since the sender is BRIDGE and we are SENSOR

/// Not generating code for dbc_encode_BRIDGE_HEARTBEAT() since the sender is BRIDGE and we are SENSOR

/// Not generating code for dbc_encode_BRIDGE_DEST() since the sender is BRIDGE and we are SENSOR

/// Not generating code for dbc_decode_MOTOR_CMD() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_RPM_VALUE_CMD() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_COMPASS_CMD() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_COMPASS_INIT_DEBUG() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_GPS_CURRENT_LAT_LONG() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_GPS_INIT_DEBUG() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_GPS_FIX_DEBUG() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_COMPASS_MAN_CAL_DEBUG() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_GPS_HEARTBEAT() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_GPS_TARGET_HEADING() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_GPS_CHECKPOINT_INDEX() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_SENSOR_STATUS() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_BRIDGE_STOP() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_BRIDGE_GO() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_BRIDGE_INIT_DEBUG() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_BRIDGE_HEARTBEAT() since 'SENSOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_BRIDGE_DEST() since 'SENSOR' is not the recipient of any of the signals

#endif
