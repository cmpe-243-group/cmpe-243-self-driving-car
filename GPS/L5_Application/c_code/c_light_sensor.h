#ifndef C_LIGHT_SENSOR_H_
#define C_LIGHT_SENSOR_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool light_sensor_init(void);
uint16_t light_sensor_get_raw_value(void);
uint8_t light_sensor_get_percent_value(void);

#ifdef __cplusplus
}
#endif
#endif /* C_LIGHT_SENSOR_H_ */
