#ifndef CAN_LIB_H_
#define CAN_LIB_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "printf_lib.h"
#include "shared.h"
#include "c_led.h"
#include "can.h"
#include "_can_dbc/generated_can.h"

#define GEO_CAN_BAUD_RATE       (100)
#define GEO_CAN_RX_QUEUE_SIZE   (20)
#define GEO_CAN_TX_QUEUE_SIZE   (20)

#define BRIDGE_DEST_MID         (325)

bool geo_can_init(void);
void geo_can_1Hz(void);
void geo_can_tx_gps_init_debug(void);
void geo_can_tx_compass_init_debug(void);
void geo_can_tx_gps_fix_debug(bool is_fix);
void geo_can_tx_compass_man_cal_debug(void);
void geo_can_tx_gps_heartbeat(void);
void geo_can_tx_heading(float heading);
void geo_can_tx_coords(float curLat, float curLong);
BRIDGE_DEST_t geo_can_handle_rx(void);
void geo_can_tx_target_heading_distance(float heading, float distance);




#endif /* CAN_LIB_H_ */
