

#ifndef GPS_H_
#define GPS_H_
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "FreeRTOS.h"
#include "printf_lib.h"
#include "can_lib.h"
#include "shared.h"
#include "c_led.h"
#include "c_led_display.h"
#include "c_uart2.h"
#include "compass.h"

#define GPS_FIX_LED 1
#define GPS_MIA_LED 2


#define MAX_LONGITUDE 180
#define MAX_LATITUDE 90
#define DELIMITER ","
#define GPS_GPRMC_SENTENCES_ONLY "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"
#define GPS_SET_UPDATE_RATE_10Hz "$PMTK220,100*2F"
#define GPS_SET_BAUD_RATE_57600 "$PMTK251,57600*2C"
#define PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ  "$PMTK220,5000*1B"
#define PMTK_API_SET_FIX_CTL_200_MILLIHERTZ  "$PMTK300,5000,0,0,0,0*18"

#define GPS_CHECKPOINT_MAX_DISTANCE 99999.0



bool gps_init(unsigned int speedInKhz);
void gps_parse(char* buffer);
void gps_1Hz(void);
void gps_10Hz(void);
void gps_50Hz(void);
void reset_checkpoints(void);
void next_checkpoint_handler(void);
void separate_gps_task(void *p);
float calculate_target_heading(float cur_lat, float cur_lng, float dest_lat, float dest_lng);
float calculate_target_distance(float cur_lat, float cur_lng, float dest_lat, float dest_lng);
void build_route();
void calculate_addtl_checkpoints(uint8_t initial_checkpoint_idx);
uint8_t calculate_initial_checkpoint(float cur_lat, float cur_long);
//utility functions for calculating target heading
float to_radian(float degree);
float to_degree(float radian);
double convert_sentence_to_coord(double coordinates, const char *value);



#endif /* GPS_H_ */
