#include <can_lib.h>


BRIDGE_DEST_t bridge_dest_msg = {0.0};

const uint32_t                             BRIDGE_DEST__MIA_MS = 1000;
const BRIDGE_DEST_t                        BRIDGE_DEST__MIA_MSG = {0};

bool geo_can_init()
{
    led_init();
    if(CAN_init(can1, GEO_CAN_BAUD_RATE, GEO_CAN_RX_QUEUE_SIZE, GEO_CAN_TX_QUEUE_SIZE, NULL, NULL))
    {
        //for both CAN controllers, set up filter to accept all messages
        CAN_bypass_filter_accept_all_msgs();
        //Make CAN1 active on CAN bus by sending reset
        CAN_reset_bus(can1);
        return true;
    }
    return false;
}

void geo_can_1Hz()
{
    if(CAN_is_bus_off(can1))
    {
        CAN_reset_bus(can1);
    }
    geo_can_tx_gps_heartbeat();
}


BRIDGE_DEST_t geo_can_handle_rx(void)
{
    can_msg_t can_msg;
    while(CAN_rx(can1, &can_msg, 0))
    {
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        if(can_msg.msg_id == BRIDGE_DEST_MID)
        {
            led_set(3, false);
            dbc_decode_BRIDGE_DEST(&bridge_dest_msg, can_msg.data.bytes, &can_msg_hdr);
        }
    }

    if(dbc_handle_mia_BRIDGE_DEST(&bridge_dest_msg, 10))
    {
        led_set(3, true);
    }

    return bridge_dest_msg;
}

void geo_can_tx_target_heading_distance(float heading, float distance)
{
    GPS_TARGET_HEADING_t gps_target_heading_msg = {0};
    gps_target_heading_msg.TARGET_HEADING = heading;
    gps_target_heading_msg.DISTANCE = distance;
    dbc_encode_and_send_GPS_TARGET_HEADING(&gps_target_heading_msg);
}
void geo_can_tx_gps_init_debug(void)
{
    GPS_INIT_DEBUG_t gps_init_debug_msg = {0};
    gps_init_debug_msg.INIT_DEBUG = 1;
    dbc_encode_and_send_GPS_INIT_DEBUG(&gps_init_debug_msg);
}

void geo_can_tx_gps_fix_debug(bool is_fix)
{
    GPS_FIX_DEBUG_t gps_fix_debug_msg = {0};
    if(is_fix)
    {
        gps_fix_debug_msg.GPS_FIX_DEBUG = 1;
    }
    else
    {
        gps_fix_debug_msg.GPS_FIX_DEBUG = 0;
    }
    dbc_encode_and_send_GPS_FIX_DEBUG(&gps_fix_debug_msg);
}

void geo_can_tx_compass_init_debug(void)
{
    COMPASS_INIT_DEBUG_t compass_init_debug_msg = {0};
    compass_init_debug_msg.INIT_DEBUG = 1;
    dbc_encode_and_send_COMPASS_INIT_DEBUG(&compass_init_debug_msg);
}

void geo_can_tx_compass_man_cal_debug(void)
{
    COMPASS_MAN_CAL_DEBUG_t compass_man_cal_debug_msg = {0};
    compass_man_cal_debug_msg.IS_CAL_DEBUG = 1;
    dbc_encode_and_send_COMPASS_MAN_CAL_DEBUG(&compass_man_cal_debug_msg);
}

void geo_can_tx_gps_heartbeat(void)
{
    GPS_HEARTBEAT_t gps_heartbeat_msg = {0};
    gps_heartbeat_msg.GPS_HEARTBEAT = 1;
    dbc_encode_and_send_GPS_HEARTBEAT(&gps_heartbeat_msg);

}

void geo_can_tx_heading(float heading)
{
    COMPASS_CMD_t compass_cmd = {0};
    compass_cmd.HEADING = heading;
    dbc_encode_and_send_COMPASS_CMD(&compass_cmd);
}

void geo_can_tx_coords(float curLat, float curLong)
{
    GPS_CURRENT_LAT_LONG_t current_lat_lng_cmd = {0};
    current_lat_lng_cmd.CUR_LAT = curLat;
    current_lat_lng_cmd.CUR_LONG = curLong;
    dbc_encode_and_send_GPS_CURRENT_LAT_LONG(&current_lat_lng_cmd);

}

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);

    return CAN_tx(can1, &can_msg, 0);
}
