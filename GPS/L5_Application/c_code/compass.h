

#ifndef COMPASS_H_
#define COMPASS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "printf_lib.h"
#include <string.h>
#include <math.h>
#include "can_lib.h"
#include "c_i2c2.h"
#include "c_led.h"
#include "c_switches.h"
#include "c_led_display.h"
#include "shared.h"


#define COMPASS_DEVICE_ADDR 0x3A

//Register addresses
#define WHO_AM_I          0x0F
#define CTRL1             0x20
#define CTRL2             0x21
#define CTRL3             0x22
#define CTRL4             0x23
#define CTRL5             0x24
#define CTRL6             0x25
#define CTRL7             0x26
#define OUT_X_L_M         0x08
#define OUT_X_H_M         0x09
#define OUT_Y_L_M         0x0A
#define OUT_Y_H_M         0x0B
#define OUT_Z_L_M         0x0C
#define OUT_Z_H_M         0x0D
#define OUT_X_L_A         0x28
#define OUT_X_H_A         0x29
#define OUT_Y_L_A         0x2A
#define OUT_Y_H_A         0x2B
#define OUT_Z_L_A         0x2C
#define OUT_Z_H_A         0x2D


bool compass_init(unsigned int speedInKhz);
double compass_get_heading(void);
void compass_25Hz(void);

#ifdef __cplusplus
}
#endif
#endif /* COMPASS_H_ */
