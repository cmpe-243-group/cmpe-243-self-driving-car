#include "gps.h"


//#define MOCK
static bool gpsFix = false;
float curLat = 0, curLong = 0;
float destLat = 0, destLong = 0;
static BRIDGE_DEST_t dest_msg = {0};



const float gps_checkpoints[12][2] = {
        //10th street parking lot route
        {37.339637, -121.881320}, //0 northwest parking lot corner
        {37.339799, -121.880961}, //1 north lot intermediate
        {37.339906, -121.880728}, //2 northeast lot corner
        {37.339670, -121.880542}, //3 northeast top intermediate
        {37.339361, -121.880310}, //4 east intermediate
        {37.339013, -121.880064}, //5 southeast corner
        {37.338896, -121.880297}, //6 south intermediate
        {37.338786, -121.880541}, //7 southwest corner
        {37.339230, -121.880866}, //8
        {37.339513, -121.881179}, //9
        {37.339388, -121.881005}, //10
        {37.339073, -121.880755}, //11

};
//[row][column] up to 2 adjacent nodes until route calculation is generalized
//make a solution that doesn't use adjacency
const uint8_t adjacency_list[12][3] = {
        {0, 1, 9},
        {1, 0, 2},
        {2, 1, 3},
        {3, 2, 4},
        {4, 3, 5},
        {5, 4, 6},
        {6, 5, 7},
        {7, 6, 11},
        {8, 10, 11},
        {9, 0, 10},
        {10, 9, 8},
        {11, 7, 8},
};

#define MIN_LAT                 (36)
#define MAX_LAT                 (38)
#define MIN_LNG                 (-122)
#define MAX_LNG                 (-120)
#define DEFAULT_CHECKPT_IDX     (255)
#define NUM_CHECKPOINTS         (sizeof(gps_checkpoints)/(sizeof(gps_checkpoints[0])))
uint8_t excluded_checkpoints[12] = {0}; //lookup table to make sure we don't select route checkpoints already selected
float route_checkpoints[30][3] = {0}; //the checkpoints necessary for route are saved here once destination set
float savedDestLat = 0.0, savedDestLong = 0.0;
uint8_t nextCheckpointIndex = 0;
uint8_t numRouteCheckpoints = 0; //the total number of set checkpoints in our route array
bool isCheckpointsDone = false; //essentially communication between our build_route() running in 10Hz and our gps_50Hz()
float target_distance, target_heading;


bool gps_init(unsigned int speedInKhz)
{
    //status logic is dumb, make it not dumb
    bool status = false;
#ifdef GPS
    status |= led_init();
    led_set_all(0x0);
    status |= uart2_init(9600, 100, 100);
    if(status)
    {
        //set fix rate to 5s
        uart2_putline(PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ, 0);
        uart2_putline(PMTK_API_SET_FIX_CTL_200_MILLIHERTZ, 0);
        //return only gpsrmc sentences
        uart2_putline(GPS_GPRMC_SENTENCES_ONLY, 0);
        //set nmea update rate to 10Hz
        uart2_putline(GPS_SET_UPDATE_RATE_10Hz, 0);
        //change baud rate to 57600
        uart2_putline(GPS_SET_BAUD_RATE_57600, 0);
        //reinit uart2 to 57600
        status = uart2_init(57600, 100, 100);
        geo_can_tx_gps_init_debug();
    }
#endif
#ifdef COMPASS
    status |= compass_init(speedInKhz);
#endif
    return status;
}
//reset bus logic at 1Hz if necessary + send heartbeat
//build array of checkpoints along route once destination global set
void gps_1Hz()
{
    geo_can_1Hz();
}



float to_radian(float degree)
{
    return (degree * (M_PI/180.0));
}

float to_degree(float radian)
{
   return (radian * (180.0 / M_PI));
}

float calculate_target_heading(float cur_lat, float cur_lng, float dest_lat, float dest_lng)
{
    float curLatRad, curLngRad, destLatRad, destLngRad, deltaLngRad, targetHeading, x, y;
    curLatRad = to_radian(cur_lat);
    curLngRad = to_radian(cur_lng);
    destLatRad = to_radian(dest_lat);
    destLngRad = to_radian(dest_lng);
    deltaLngRad = destLngRad - curLngRad;

    x = sin(deltaLngRad) * cos(destLatRad);
    y = cos(curLatRad) * sin(destLatRad) - sin(curLatRad) * cos(destLatRad) * cos(deltaLngRad);
    targetHeading = fmodf(to_degree(atan2(x,y)) + 360, 360);
    return targetHeading;
}

float calculate_target_distance(float cur_lat, float cur_lng, float dest_lat, float dest_lng)
{
    float curLatRad, destLatRad, deltaLatRad, deltaLngRad, a, c;
    uint16_t R = 6371 * (10^3); //radius of earth in meters
    curLatRad = to_radian(cur_lat);
    destLatRad = to_radian(dest_lat);
    deltaLatRad = to_radian(dest_lat - cur_lat);
    deltaLngRad = to_radian(dest_lng - cur_lng);
    a = sin(deltaLatRad/2) * sin(deltaLatRad/2) + cos(curLatRad) * cos(destLatRad) * sin(deltaLngRad/2) * sin(deltaLngRad/2);
    c = 2 * atan2(sqrt(a), sqrt(1-a));

    return (R * c * 100);
}
/*
//alt route calculation
void calculate_route_checkpoints(float cur_lat, float cur_long)
{
    bool is_there_checkpoint = false;
    uint8_t checkpoint_idx = 0;
    float checkpoint_distance_to_dest, car_distance_to_checkpoint, src_distance_to_dest;
    src_distance_to_dest = calculate_target_distance(cur_lat, cur_long, savedDestLat, savedDestLong);
    float shortest_checkpoint_distance = GPS_CHECKPOINT_MAX_DISTANCE;
    for(int i = 0; i < NUM_CHECKPOINTS; i++)
    {
        car_distance_to_checkpoint = calculate_target_distance(cur_lat, cur_long, gps_checkpoints[i][0], gps_checkpoints[i][1]);
        checkpoint_distance_to_dest = calculate_target_distance(gps_checkpoints[i][0], gps_checkpoints[i][1], savedDestLat, savedDestLong);
        if((checkpoint_distance_to_dest < src_distance_to_dest) && (car_distance_to_checkpoint < shortest_checkpoint_distance) && !excluded_checkpoints[i])
        {
            shortest_checkpoint_distance = car_distance_to_checkpoint;
            checkpoint_idx = i;
            is_there_checkpoint = true;
        }
    }

    if(is_there_checkpoint)
    {
        route_checkpoints[numRouteCheckpoints][0] = gps_checkpoints[checkpoint_idx][0];
        route_checkpoints[numRouteCheckpoints][1] = gps_checkpoints[checkpoint_idx][1];
        route_checkpoints[numRouteCheckpoints][2] = checkpoint_idx;
        numRouteCheckpoints++;
    }
}

void alt_build_route(void)
{
    bool exitFlag = false;
    if(!isCheckpointsDone)
    {
        if((curLat > MIN_LAT && curLat < MAX_LAT) && (curLong > MIN_LNG && curLong < MAX_LNG) && savedDestLat != 0.00)
        {
            while(!exitFlag)
            {
                uint8_t savedNumRouteCheckpoints = numRouteCheckpoints;
                calculate_route_checkpoints();
                if(numRouteCheckpoints == savedNumRouteCheckpoints)
                {
                    //numRouteCheckpoints didn't increment after, so exitFlag = true
                    exitFlag = true;
                    isCheckpointsDone = true;
                }
            }
        }
    }
}
*/

//purpose is to calculate the immediate next checkpoint, cur_lat and cur_long are either the car's coords or the coords of most recently selected route checkpoint
uint8_t calculate_initial_checkpoint(float cur_lat, float cur_long)
{
    //u0_dbg_printf("calculate_initial_checkpoint\n");
    //car to dest or most recently selected route checkpoint to destination
    bool isThereInitialCheckpoint = false;
    uint8_t checkpoint_index = 0;
    float src_distance_to_dest = calculate_target_distance(cur_lat, cur_long, savedDestLat, savedDestLong);
    float car_distance_to_checkpoint, checkpoint_distance_to_dest;
    float shortest_checkpoint_distance = GPS_CHECKPOINT_MAX_DISTANCE;
    //loop through all our checkpoints and find the closest one to our src that also closes the distance to dest
    for(int i = 0; i < NUM_CHECKPOINTS; i++)
    {
            //cur_lat and cur_long are current car coords
            car_distance_to_checkpoint = calculate_target_distance(cur_lat, cur_long, gps_checkpoints[i][0], gps_checkpoints[i][1]);
            checkpoint_distance_to_dest = calculate_target_distance(gps_checkpoints[i][0], gps_checkpoints[i][1], savedDestLat, savedDestLong);
            if((checkpoint_distance_to_dest < src_distance_to_dest) && (car_distance_to_checkpoint < shortest_checkpoint_distance))
            {
                //led_set(2, true);
                shortest_checkpoint_distance = car_distance_to_checkpoint;
                checkpoint_index = i;
                isThereInitialCheckpoint = true;
            }
    }
    //only if there is a checkpoint that satisfies the above conditions can we say there is an initial checkpoint and can add to route_checkpoints
    if(isThereInitialCheckpoint)
    {
        //u0_dbg_printf("init %d\n", checkpoint_index);
        route_checkpoints[0][0] = gps_checkpoints[checkpoint_index][0];
        route_checkpoints[0][1] = gps_checkpoints[checkpoint_index][1];
        route_checkpoints[0][2] = checkpoint_index;
        numRouteCheckpoints++;
        return checkpoint_index;
    }
    else
    {
        //returning this magic number means we didn't find a checkpoint, assume it's just a straight line to dest
        return DEFAULT_CHECKPT_IDX;
    }

}

//current logic assumes that a checkpoint is adjacent to at most 2 other nodes
//TODO: make this function not dumb
void calculate_addtl_checkpoints(uint8_t initial_checkpoint_idx)
{
    //u0_dbg_printf("calculate_addtl_checkpoints\n");
    uint8_t counter = 1; //we have initial checkpoint at route_checkpoints idx 0, start adding more at idx 1
    int8_t adjacent_checkpoint_idx_1,adjacent_checkpoint_idx_2;
    uint8_t current_checkpoint_idx = initial_checkpoint_idx;
    float adj_checkpoint_distance_to_dest_1, adj_checkpoint_distance_to_dest_2, cur_distance_to_dest;
    bool exitFlag = false;
    while(!exitFlag)
    {
        //at the beginning of each iteration, reset both checkpoint_distance_to_dest to this absolute max constant
        adj_checkpoint_distance_to_dest_1 = adj_checkpoint_distance_to_dest_2 = GPS_CHECKPOINT_MAX_DISTANCE;
        cur_distance_to_dest = calculate_target_distance(gps_checkpoints[current_checkpoint_idx][0], gps_checkpoints[current_checkpoint_idx][1],
                savedDestLat, savedDestLong);
            adjacent_checkpoint_idx_1 = adjacency_list[current_checkpoint_idx][1];
            adjacent_checkpoint_idx_2 = adjacency_list[current_checkpoint_idx][2];

            //first adjacent checkpoint might already considered excluded, consider that scenario
            if(adjacent_checkpoint_idx_1 != -1)
            {
                if(!excluded_checkpoints[adjacent_checkpoint_idx_1])
                {
                    adj_checkpoint_distance_to_dest_1 = calculate_target_distance(gps_checkpoints[adjacent_checkpoint_idx_1][0], gps_checkpoints[adjacent_checkpoint_idx_1][1],
                            savedDestLat, savedDestLong);
                }
            }
            //assuming column 2 of adjacency_list will be -1 if adjacent to only one checkpoint
            if(adjacent_checkpoint_idx_2 != -1)
            {
                //if adjacent_checkpoint_idx_2 is defined and is not in excluded_checkpoints, calculate distance to dest
                if(!excluded_checkpoints[adjacent_checkpoint_idx_2])
                {
                    adj_checkpoint_distance_to_dest_2 = calculate_target_distance(gps_checkpoints[adjacent_checkpoint_idx_2][0], gps_checkpoints[adjacent_checkpoint_idx_2][1],
                            savedDestLat, savedDestLong);
                }
            }
            //if first adjacent checkpoint is closer to destination than current checkpoint we're trying to append to
            if(adj_checkpoint_distance_to_dest_1 < cur_distance_to_dest)
            {
                if(adj_checkpoint_distance_to_dest_1 < adj_checkpoint_distance_to_dest_2)
                {
                    //u0_dbg_printf("%d\n", adjacent_checkpoint_idx_1);
                    route_checkpoints[counter][0] = gps_checkpoints[adjacent_checkpoint_idx_1][0];
                    route_checkpoints[counter][1] = gps_checkpoints[adjacent_checkpoint_idx_1][1];
                    route_checkpoints[counter][2] = adjacent_checkpoint_idx_1;
                    excluded_checkpoints[adjacent_checkpoint_idx_1] = 1;
                    current_checkpoint_idx = adjacent_checkpoint_idx_1;
                    numRouteCheckpoints++;
                    //counter++;
                }
                else
                {
                    //u0_dbg_printf("%d\n", adjacent_checkpoint_idx_2);
                    route_checkpoints[counter][0] = gps_checkpoints[adjacent_checkpoint_idx_2][0];
                    route_checkpoints[counter][1] = gps_checkpoints[adjacent_checkpoint_idx_2][1];
                    route_checkpoints[counter][2] = adjacent_checkpoint_idx_2;
                    excluded_checkpoints[adjacent_checkpoint_idx_2] = 1;
                    current_checkpoint_idx = adjacent_checkpoint_idx_2;
                    numRouteCheckpoints++;
                    //counter++;
                }
                counter++;
            }
            else if(adj_checkpoint_distance_to_dest_2 < cur_distance_to_dest)
            {
                if(adj_checkpoint_distance_to_dest_1 < adj_checkpoint_distance_to_dest_2)
                {
                    //u0_dbg_printf("%d\n", adjacent_checkpoint_idx_1);
                    route_checkpoints[counter][0] = gps_checkpoints[adjacent_checkpoint_idx_1][0];
                    route_checkpoints[counter][1] = gps_checkpoints[adjacent_checkpoint_idx_1][1];
                    route_checkpoints[counter][2] = adjacent_checkpoint_idx_1;
                    excluded_checkpoints[adjacent_checkpoint_idx_1] = 1;
                    current_checkpoint_idx = adjacent_checkpoint_idx_1;
                    numRouteCheckpoints++;
                    //counter++;
                }
                else
                {
                    //u0_dbg_printf("%d\n", adjacent_checkpoint_idx_2);
                    route_checkpoints[counter][0] = gps_checkpoints[adjacent_checkpoint_idx_2][0];
                    route_checkpoints[counter][1] = gps_checkpoints[adjacent_checkpoint_idx_2][1];
                    route_checkpoints[counter][2] = adjacent_checkpoint_idx_2;
                    excluded_checkpoints[adjacent_checkpoint_idx_2] = 1;
                    current_checkpoint_idx = adjacent_checkpoint_idx_2;
                    numRouteCheckpoints++;
                    //counter++;
                }
                counter++;
            }
            else
            {
                //none of the adjacent checkpoints get us close to destination
                exitFlag = true;
            }
    }
}

//this function should determine initial closest checkpoint to destination by looping through every entry in destination and then each subsequent
//checkpoint is calculated by looping through the recently selected checkpoint's adjacency list
void build_route()
{
    //only want to run build_route() once for each destination
    if(!isCheckpointsDone)
    {
        //only go into building route loop if gps has a fix and provides coords that make sense AND we have a set non-zero destination
        //changed savedDestLat != 0.00 to > to get rid of warnings
        if((curLat > MIN_LAT && curLat < MAX_LAT) && (curLong > MIN_LNG && curLong < MAX_LNG) && savedDestLat != 0.00)
        {
            //portENTER_CRITICAL();
            //led_set(2, true);
            uint8_t initial_checkpoint = calculate_initial_checkpoint(curLat, curLong);
            //calculate an initial checkpoint by looping through all entries of list
            //only if there is an initial checkpoint should we bother to calculate addtl ones
            if(initial_checkpoint != DEFAULT_CHECKPT_IDX)
            {
                calculate_addtl_checkpoints(initial_checkpoint);
            }
            //at this point, all the route checkpoints should be found
            isCheckpointsDone = true; //set this to true so build_route won't run again until a new destination has been set
            //geo_can_tx_checkpoint_idx(route_checkpoints, numRouteCheckpoints);
            //portEXIT_CRITICAL();
        }

    }
}

void gps_10Hz(void)
{
    build_route(); //even after "optimization", still won't run in 100Hz, 10Hz is fine
    /*
    if((curLat > 36 && curLat < 38) && (curLong > -122 && curLong < -120))
    {
        u0_dbg_printf("%f, %f\n", curLat, curLong);
    }
    */
}
// xxx Create a separate task
void separate_gps_task(void *p)
{

    char line[100] = {'\0'};
    while (1) {
       if ( uart2_gets(line, sizeof(line), portMAX_DELAY)) {

           char valid_invalid[2]={0};
           char dummy[10];
           char south_or_north[10] = { 0};
           float longitude = 0;
           float latitude = 0;

           // $GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E*62
           if (5 == sscanf(line, "$GPRMC,%[^,],%2[^,],%f,%2[^,],%f", dummy, valid_invalid, &longitude, south_or_north, &latitude)) {
               printf("%s, %f, %f\n", valid_invalid, longitude, latitude);
               portENTER_CRITICAL();
               curLat = latitude;
               curLong = longitude;
               u0_dbg_printf("%f, %f\n", latitude, longitude);
               portEXIT_CRITICAL();
           }
           else {
               puts("Failed");
           }

       }
    }
}

void reset_checkpoints(void)
{
    //user should not be willy nilly changing destination, start off with clearing our arrays
    memset(excluded_checkpoints, 0, sizeof(excluded_checkpoints));
    memset(route_checkpoints, 0, sizeof(route_checkpoints));
    nextCheckpointIndex = 0;
    savedDestLat = destLat;
    savedDestLong = destLong;
    numRouteCheckpoints = 0;
    isCheckpointsDone = false; //enable build_route

}

void next_checkpoint_handler(void)
{
    //geo_can_tx_next_checkpoint(nextCheckpointIndex);
    //geo_can_tx_checkpoint_idx(route_checkpoints[nextCheckpointIndex][2]); //transmit checkpoint idx to bridge
    target_heading = calculate_target_heading(curLat, curLong, route_checkpoints[nextCheckpointIndex][0], route_checkpoints[nextCheckpointIndex][1]);
    target_distance = calculate_target_distance(curLat, curLong, route_checkpoints[nextCheckpointIndex][0], route_checkpoints[nextCheckpointIndex][1]);
    geo_can_tx_target_heading_distance(target_heading,target_distance);
    if(target_distance < 5) //0.5 meters away from our current checkpoint, set next checkpoint to aim for
    {
        nextCheckpointIndex++;
    }
}

void gps_50Hz(void)
{

#ifdef MOCK
curLat = 37.336878;
curLong = -121.882149;
dest_msg.DEST_LAT = 37.335850;
dest_msg.DEST_LNG = -121.881705;
#endif

#if 1
    // xxx: Bug here
    char buffer[80] = {'\0'};
    bool active = false;
    active = uart2_gets(buffer, 80, 0);
#endif
    // xxx: Move this logic to the GPS task
#if 1
    //u0_dbg_printf("%s\r\n", buffer); //print the raw GPRMC sentence
    if(active && buffer != NULL  && strstr(buffer, "$GPRMC"))
    {
        gps_parse(buffer);
    }
#endif
    if(gpsFix)
    {
        led_set(GPS_FIX_LED, true);
    }
    else
    {
        led_set(GPS_FIX_LED, false);
    }

    geo_can_tx_gps_fix_debug(gpsFix);
    //if coords don't make sense (i.e. not within reasonable bounds of SJSU campus), don't send them
    //gps latlng only sent on can bus when there's a fix

    // xxx: should be moved to a separate function
    if((curLat > 36 && curLat < 38) && (curLong > -122 && curLong < -120))
    {
        //u0_dbg_printf("curLat: %f, curLng: %f\n", curLat, curLong); //print our lat and lng after parsing
        geo_can_tx_coords(curLat, curLong);
        dest_msg = geo_can_handle_rx(); //can rx loop to get destination
        //dest_msg will only be non-zero once app has selected a dest and sent to bridge and finally to gps
        //basically, only overwrite our destLat and destLong when the received one is not mia
        //changed != to > to get rid of warnings
        if(dest_msg.DEST_LAT != 0.00 && dest_msg.DEST_LNG != 0.00)
        {
            destLat = dest_msg.DEST_LAT;
            destLong = dest_msg.DEST_LNG;
            //if we receive a non-zero destination that's different from previously saved dest, reset our globals
            if(destLat != savedDestLat || destLong != savedDestLong)
            {

                //portENTER_CRITICAL();
                reset_checkpoints();
                //portEXIT_CRITICAL();
            }
        }
        //move this outside because dest_msg will become MIA, and therefore zero
        if(savedDestLat != 0.0 || savedDestLong != 0.0)
        {
            //if there are no checkpoints, then both operands will be 0 and this route_checkpoints logic will be skipped
            if(nextCheckpointIndex <= (numRouteCheckpoints - 1))
            {
                //led_toggle(2);
                led_set(2, false);
                led_display_set(route_checkpoints[nextCheckpointIndex][2]); //show on LED display the index of gps_checkpoints that corresponds to this route checkpoint
                //portENTER_CRITICAL();
                next_checkpoint_handler();
                //portEXIT_CRITICAL();

            }
            else
            {
                led_display_set(99);
                led_set(2, true);

                target_distance = calculate_target_distance(curLat, curLong, savedDestLat, savedDestLong);
                geo_can_tx_target_heading_distance(calculate_target_heading(curLat, curLong, savedDestLat, savedDestLong), target_distance);
            }

            /*
            else
            {
                led_set(2, true);
                //portENTER_CRITICAL();
                //no more checkpoints, check if destination is past our last one
                target_distance = calculate_target_distance(curLat, curLong, savedDestLat, savedDestLong);
                geo_can_tx_target_heading_distance(calculate_target_heading(curLat, curLong, savedDestLat, savedDestLong), target_distance);
                //portEXIT_CRITICAL();
            }
            */
        }
    }

}

#if 1
void gps_parse(char* buffer)
{
    char *token;
    uint8_t counter = 0;
    //Example valid sentence: $GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E*62
    //Example invalid sentence: $GPRMC,,V,,,,,,,,,,N*53
    //token is 0-indexed (ie. GPRMC is token 0)
    while((token = strtok_r(buffer, DELIMITER, &buffer)))
    {
        if(counter == 2)
        {
            if(strcmp(token, "A") == 0)
            {
                gpsFix = true;
            }
            else if(strcmp(token, "V") == 0)
            {
                //u0_dbg_printf("gps invalid data\n");
                gpsFix = false;
            }
            else
            {
                //if we get here, our buffer is garbage (out of sync) in some fashion, discard it and try again with next buffer
                //u0_dbg_printf("Wrong token: %s\n", token);
                return;
            }
        }

        if(gpsFix)
        {
            if(counter == 3)
           {
               curLat = atof(token);
               curLat = convert_sentence_to_coord(curLat, "m");
               led_set(3, true);
           }
           if(counter == 4)
           {
               if(strcmp(token, "S") == 0)
               {
                   curLat *= -1; //lat is neg if S given
               }

           }
           if(counter == 5)
           {
               curLong = atof(token);
               curLong = convert_sentence_to_coord(curLong, "p");
           }
           if(counter == 6)
           {
               if(strcmp(token, "W") == 0)
               {
                   curLong *= -1;
               }
           }
        }
        counter++;
        if(counter >= 7)
        {
            break;
        }
    }
}
#endif

//Formula from
//http://yopero-tech.blogspot.com/2013/08/parsing-nmea-sentences-in-c.html
double convert_sentence_to_coord(double coordinates, const char *value)
{
    if((*value == 'm') && coordinates < 0.0 && coordinates > MAX_LATITUDE)
    {
        return 0;
    }
    if(*value == 'p' && (coordinates < 0.0 && coordinates > MAX_LONGITUDE))
    {
        return 0;
    }

    int b = coordinates/100;
    double c = (coordinates/100 - b) * 100;
    c /= 60;
    c += b;
    return c;
}
