#include "c_i2c2.h"
#include "singleton_template.hpp"
#include "i2c2.hpp"

bool i2c2_init(unsigned int speedInKhz)
{
    return I2C2::getInstance().init(speedInKhz);
}

uint8_t i2c2_read_reg(uint8_t deviceAddress, uint8_t registerAddress)
{
    return I2C2::getInstance().readReg(deviceAddress, registerAddress);
}

void i2c2_write_reg(uint8_t deviceAddress, uint8_t registerAddress, uint8_t value)
{
    I2C2::getInstance().writeReg(deviceAddress, registerAddress, value);
}




