/**
 * @file

 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */



#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "shared.h"
#include "FreeRTOS.h"
//#include "printf_lib.h"
#ifdef BRIDGE
#include "bridge.h"
#endif
#include "gps.h"

bool C_period_init(void) {
    geo_can_init();
    gps_init(SYS_CFG_I2C2_CLK_KHZ); // xxx: gps_compass_init();
    return true;
}
bool C_period_reg_tlm(void) {
    return true;
}

//every 1000ms
void C_period_1Hz(uint32_t count) {
    (void) count;
#ifdef GPS
        gps_1Hz();
#endif
}

//every 100ms
void C_period_10Hz(uint32_t count) {
    (void) count;
    //handle all the complex route calculations in slower task
    gps_10Hz();

}

//every 10ms
void C_period_100Hz(uint32_t count) {
    (void) count;

    // xxx: Compass could move to 50Hz
#ifdef GPS
    //playing with speed of gps parsing periodic to see how it affects stability of location coords but keep compass at 50Hz ~ 100Hz
    if(count % 4 == 0)
    {
        compass_25Hz();
    }
    if(count % 4 == 0)
    {
        // xxx: Get rid of all GPS UART handling from periodics
        gps_50Hz(); // xxx: Should be 25Hz
    }
#endif
}

//every 1ms
void C_period_1000Hz(uint32_t count) {
    (void) count;
}
