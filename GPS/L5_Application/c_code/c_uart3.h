
//receiver
#ifndef C_UART3_H_
#define C_UART3_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool uart3_init(unsigned int baudRate, int rxQSize, int txQSize);
bool uart3_getchar(char *byte, uint32_t timeout_ms);
//char out, unsigned int timeout
void uart3_putchar(char byte,  unsigned int timeout);
void uart3_putline(const char* pBuff, unsigned int timeout);
//bool uart3_flush();


#ifdef __cplusplus
}
#endif
#endif /* C_UART3_H_ */
