#ifndef C_SWITCHES_H_
#define C_SWITCHES_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool switches_init(void);
bool switches_get(int num);

#ifdef __cplusplus
}
#endif
#endif /* C_SWITCHES_H_ */
