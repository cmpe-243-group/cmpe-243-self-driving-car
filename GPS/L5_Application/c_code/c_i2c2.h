#ifndef C_I2C2_H_
#define C_I2C2_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool i2c2_init(unsigned int speedInKhz);
uint8_t i2c2_read_reg(uint8_t deviceAddress, uint8_t registerAddress);
void i2c2_write_reg(uint8_t deviceAddress, uint8_t registerAddress, uint8_t value);

#ifdef __cplusplus
}
#endif
#endif /* C_I2C2_H_ */
