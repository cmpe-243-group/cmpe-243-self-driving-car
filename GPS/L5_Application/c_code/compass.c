
#include "compass.h"

#ifdef COMPASS

double cal_offset;
bool calibrated;

/*
 * Configure LSM303D compass with correct config register settings
 */
bool compass_init(unsigned int speedInKhz)
{
    led_display_init();
    switches_init();
    cal_offset = 0.0;
    calibrated = false;
    if(i2c2_init(speedInKhz))
    {
        //Accelerometer in power-down mode by default, ,must be power-down if magnetic sensor to be used in 100Hz
        //I2C2::getInstance().writeReg(COMPASS_DEVICE_ADDR, CTRL1, 0x57); //set accel to 50Hz and all axis enabled
        i2c2_write_reg(COMPASS_DEVICE_ADDR, CTRL5, 0x04);//set Magnetic data rate to 50Hz with low resolution (0x70 if high resolution desired)
        //I2C2::getInstance().writeReg(COMPASS_DEVICE_ADDR, CTRL6, 0x00); //set to +- 2 gauss
        i2c2_write_reg(COMPASS_DEVICE_ADDR, CTRL7, 0x00);//select continuous-conversion mode for magnetic sensor (default: powerdown)
        geo_can_tx_compass_init_debug();
        return true;
    }
    return false;
}

double compass_get_heading()
{
    int16_t mx, my;
    double curHeading;
    //u0_dbg_printf("whoAmI: %#x\n", I2C2::getInstance().readReg(COMPASS_DEVICE_ADDR, WHO_AM_I));
    mx = (int16_t) i2c2_read_reg(COMPASS_DEVICE_ADDR, OUT_X_H_M) << 8 | i2c2_read_reg(COMPASS_DEVICE_ADDR, OUT_X_L_M);
    my = (int16_t) i2c2_read_reg(COMPASS_DEVICE_ADDR, OUT_Y_H_M) << 8 | i2c2_read_reg(COMPASS_DEVICE_ADDR, OUT_Y_L_M);
    curHeading = atan2(my,mx);
    if(curHeading < 0)
    {
        curHeading += 2 * M_PI;
    }
    curHeading = ((curHeading * 180)/M_PI);
    //apply a calibration offset
    if(calibrated)
    {
        curHeading -= cal_offset;
    }
    curHeading = 360 - curHeading;
    //adjust value until it's within 0-360 range
    if(curHeading < 0)
    {
        curHeading = 360 + curHeading;
    }
    return curHeading;
}

/*
 * Heading: Angle between the device's x axis and the magnetic north measured clockwise from top of device
 * Pitch: Rotating around the device's Y axis with the device's X axis moving upwards, pitch is positive and increasing
 * Roll: Rotating around the device's X axis with the device's Y axis moving downwards, roll is positive and increasing
 */
void compass_25Hz()
{
    //manual calibration logic, when sw1 is pressed, the current heading returned is the new 0
    if(switches_get(1))
    {
        calibrated = true;
        cal_offset = compass_get_heading();
        led_set(4, true);
        geo_can_tx_compass_man_cal_debug();
    }

    double curHeading = compass_get_heading();
    //led_display_set(curHeading/10);
    geo_can_tx_heading(curHeading);

}
#endif

