
#include "c_led.h"
#include "singleton_template.hpp"
#include "led.hpp"

bool led_init()
{
    return LED::getInstance().init();
}
void led_set(uint8_t ledNum, bool on)
{
    LED::getInstance().set(ledNum, on);
}

void led_toggle(uint8_t ledNum)
{
    LED::getInstance().toggle(ledNum);
}

void led_set_all(uint8_t value)
{
    LED::getInstance().setAll(value);
}


