/*
 * c_led.h
 *
 *  Created on: Mar 21, 2019
 *      Author: Kevin
 */

#ifndef C_LED_H_
#define C_LED_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool led_init(void);
void led_set(uint8_t ledNum, bool on);
void led_toggle(uint8_t ledNum);
void led_set_all(uint8_t value);

#ifdef __cplusplus
}
#endif



#endif /* C_LED_H_ */
