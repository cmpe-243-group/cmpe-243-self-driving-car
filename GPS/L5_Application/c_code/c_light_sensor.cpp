#include "c_light_sensor.h"
#include "singleton_template.hpp"
#include "light_sensor.hpp"

bool light_sensor_init()
{
    return Light_Sensor::getInstance().init();
}

uint16_t light_sensor_get_raw_value()
{
    return Light_Sensor::getInstance().getRawValue();
}

uint8_t light_sensor_get_percent_value(void)
{
    return Light_Sensor::getInstance().getPercentValue();
}




