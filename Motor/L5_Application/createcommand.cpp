/*
 * createcommand.cpp
 *
 *  Created on: Apr 3, 2019
 *      Author: Uma
 */

//createcommand.cpp
#include<stdio.h>
#include <stdlib.h>
#include "command_handler.hpp"
#include "io.hpp"
#include "lpc_pwm.hpp"


PWM *servo;
PWM *dc;

CMD_HANDLER_FUNC(SFHandler)
{

int num = cmdParams;
servo = new PWM(PWM::pwm3, num);


return true;
}

CMD_HANDLER_FUNC(DFHandler)
{

int num = cmdParams;
dc = new PWM(PWM::pwm1, num);


return true;
}

CMD_HANDLER_FUNC(SHandler)
{

float num = atof(cmdParams.c_str());
servo->set(num);
printf("servo is set to %f\n", num);


return true;
}

CMD_HANDLER_FUNC(DHandler)
{

float num = atof(cmdParams.c_str());
dc->set(num);
printf("dc is set to %f\n", num);


return true;
}



