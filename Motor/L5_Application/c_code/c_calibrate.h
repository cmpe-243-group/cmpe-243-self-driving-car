/*
 * calibrate.h
 *
 *  Created on: Apr 9, 2019
 *      Author: Uma
 */

#ifndef C_CALIBRATE_H_
#define C_CALIBRATE_H_
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

bool c_calibrate_sw(int count);


#ifdef __cplusplus
}
#endif

#endif /* C_CALIBRATE_H_ */
