/*
 * motor_class.h
 *
 *  Created on: Mar 29, 2019
 *      Author: Uma
 */

#ifndef MOTOR_CLASS_HPP_
#define MOTOR_CLASS_HPP_
#include "lpc_pwm.hpp"
#include <stdint.h>
#include <stdbool.h>

class Motor {
public:
    PWM *servo, *dc;
    void motor_init(void);
    void motor_execute(void);
    void set_speed(float msec);

    //servo motor functions
    void steer_left(void);
    void steer_right(void);
    void steer_soft_left(void);
    void steer_soft_right(void);
    void steer(float driftVal);
    void align(void);


    //helper functions
    void servo_pwm_set(float pwm);
    void dc_pwm_set(float pwm);


    //dc motor functions
    void move_forward(void);
    void move_reverse(void);
    void stop(void);
    void brake(void);

    //calibration functions
    void dc_set_neutral(void);
    void dc_set_fwdMax(void);
    void dc_set_revMax(void);

private:
    void pid_logic(int present_rps, int diff_rps);
};

#endif /* MOTOR_CLASS_HPP_ */
