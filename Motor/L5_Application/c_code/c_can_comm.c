/*
 * motor_rx.cpp
 *
 *  Created on: Apr 17, 2019
 *      Author: Uma
 */
#include "lcd.h"
#include <c_code/c_can_comm.h>

//To handle MIA if no command is being received from master
const uint32_t MOTOR_CMD__MIA_MS = 2000;
const MOTOR_CMD_t MOTOR_CMD__MIA_MSG = { 0 };

static MOTOR_CMD_t motor_cmd_msg = { 0 };
static GPS_TARGET_HEADING_t target_distance = { 0};

static can_msg_t indicate_mia = { 0 };

static int CAN_init_value = 0;


bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);
    return CAN_tx(can1, &can_msg, 0);
}

void CAN_Init(void)
{

    if (CAN_init(can1, 100, 20, 100, NULL, NULL)) {
        CAN_init_value = 21;
    }
    CAN_bypass_filter_accept_all_msgs();
    CAN_reset_bus(can1);
}

STEER_CMD_enum_E get_steer(void) {
    return motor_cmd_msg.STEER_CMD_enum;
}

void CAN_Recieve_100Hz(void)
{
    const bool mia = dbc_handle_mia_MOTOR_CMD(&motor_cmd_msg, 10);
    indicate_mia.data.bytes[0] = 0;
    if (mia) {
            CAN_tx(can1, &indicate_mia, 0);
            c_stop();
            c_ledlight(2);
    }

    can_msg_t Rx_data = { 0 };

    while (CAN_rx(can1, &Rx_data, 0)) {
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = Rx_data.frame_fields.data_len;
        can_msg_hdr.mid = Rx_data.msg_id;

        dbc_decode_MOTOR_CMD(&motor_cmd_msg, Rx_data.data.bytes, &can_msg_hdr);
        dbc_decode_GPS_TARGET_HEADING(&target_distance,Rx_data.data.bytes, &can_msg_hdr);
    }


}

GPS_TARGET_HEADING_t CAN_Receive_gps_distance_command(void){

    return target_distance;
}


MOTOR_CMD_t CAN_Receive_get_motor_command()
{
    return motor_cmd_msg;
}

void CAN_Send_100Hz()
{
    RPM_VALUE_CMD_t rps_value = { 0 };
    rps_value.RPM_VALUE = c_get_rps();
    rps_value.CAN_INIT_MSG = CAN_init_value;
    rps_value.RECEIVED_STEER_CMD = motor_cmd_msg.STEER_CMD_enum;
    rps_value.MOTOR_HEARTBEAT = !motor_cmd_msg.mia_info.is_mia;
    dbc_encode_and_send_RPM_VALUE_CMD(&rps_value);

}

