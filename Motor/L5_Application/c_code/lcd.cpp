/*
 * lcd_class.cpp
 *
 *  Created on: May 4, 2019
 *      Author: Uma
 */

#include "lcd.h"
#include "uart2.hpp"
#include "utilities.h"
#include <stdio.h>


Uart2* uartobj= NULL;
bool lcd_uart_init(){
    uartobj = &Uart2::getInstance();
    uartobj->init(38400,100,600);
    return true;
}
void lcd_print(char* c,int block=1, int line=0){
   uartobj->printf("$GOTO:%i:%i\n %s",block,line,c);
}

//Serial LCD driver is being initialized
bool lcd_init(){
    delay_ms(100);
    uartobj->putChar('0xF0');
    delay_ms(100);
    delay_ms(1000);
    uartobj->printf("$GOTO:3:0\n Automophiles");
    return true;
}

void speed_print(char* s){
    uartobj->printf("$GOTO:0:1\n Speed:%smsec\n",s);
}

void distance_print(char *sp){
    uartobj->printf("$GOTO:0:3\n Dist:%sm\n",sp);
}



