/*
 ` * calibrate.cpp
 *
 *  Created on: Apr 9, 2019
 *      Author: Uma
 */
#include <c_code/c_calibrate.h>
#include <c_code/c_motor_class.h>
#include <stdio.h>
#include <stdbool.h>
#include "io.hpp"
#include "LED_Display.hpp"
#include "switches.hpp"

bool c_calibrate_sw(int count)
{
    bool res = false;
    res = Switches::getInstance().getSwitch(1);
    //to define the full forward and reverse value,
    //thereby defining the range of dc motor values
    if (res == true) {
        if (count % 30 == 0) {
            c_setNeutral();
            LED_Display::getInstance().setNumber(1);
        }
        if (count % 30 == 10) {
            c_setfwd(); //set forward max value
            LED_Display::getInstance().setNumber(2);
        }
        if (count % 30 == 20) {
            c_setrev();
            LED_Display::getInstance().setNumber(3);
        }
        return true;
    }
    else {
        return false;
    }
}

