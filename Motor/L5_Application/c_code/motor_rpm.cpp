/*
 * motor_rpm.cpp
 *
 *  Created on: Apr 18, 2019
 *      Author: Uma
 */


#include <c_code/c_motor_rpm.h>
#include <c_code/motor_rpm.hpp>
#include "eint.h"
#include <stdio.h>
#include "io.hpp"
#include "printf_lib.h"



static int rotations=0;
static int rps=0;

void rpm_init(void){
        eint3_enable_port2(1, eint_rising_edge, rpm_callback);
        LD.init();
        LE.init();
}

void rpm_callback(){
   rotations++;
}

int get_rps(void){
    return rps;
}

void rps_run_100Hz(int count){
   // scale down from 100Hz to 2hz
   if(count % (100 / 2) == 0){
       rps = rotations *2;//every 1/2 second
       rotations = 0;
   }
}



