/*
 * rpm_wrapper.cpp
 *
 *  Created on: Apr 20, 2019
 *      Author: Uma
 */
#include <c_code/c_motor_rpm.h>
#include "io.hpp"
#include "printf_lib.h"

void c_LED_display(int num){
    LED_Display::getInstance().setNumber(num);
}

void c_rpm_init(void){
    rpm_init();
}
void c_rpm_callback(void){
    rpm_callback();
}

int c_get_rps(void){
    return get_rps();
}

void c_rps_run_100Hz(int count){
     rps_run_100Hz(count);
}

void c_ledlight(int num){
    LED::getInstance().toggle(num);
}

