/*
 * motor_rx.h
 *
 *  Created on: Apr 17, 2019
 *      Author: Uma
 */

#ifndef C_CAN_COMM_H_
#define C_CAN_COMM_H_


#include <stdint.h>
#include <c_code/c_motor_class.h>
#include <c_code/c_motor_rpm.h>
#include "can.h"
#include <string.h>
#include "_can_dbc/generated_can.h"


void CAN_Init(void);
void CAN_Recieve_100Hz(void);
void CAN_Send_100Hz(void);
MOTOR_CMD_t CAN_Receive_get_motor_command(void);
GPS_TARGET_HEADING_t CAN_Receive_gps_distance_command(void);





#endif /* C_CAN_COMM_H_ */
