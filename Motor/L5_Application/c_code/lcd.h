/*
 * lcd.h
 *
 *  Created on: May 4, 2019
 *      Author: Uma
 */

#ifndef LCD_H_
#define LCD_H_
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

bool lcd_uart_init();
void lcd_print(char *c,int block, int line);
bool lcd_init();
void speed_print(char* s);
void distance_print(char *sp);

#ifdef __cplusplus
}
#endif


#endif /* LCD_H_ */
