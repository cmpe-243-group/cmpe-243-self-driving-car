/*
 * motor_class.cpp
 *
 *  Created on: Mar 29, 2019
 *      Author: Uma
 */
#include <c_code/motor_class.hpp>
#include <c_code/motor_rpm.hpp>
#include <stdio.h>
#include "io.hpp"
#include "printf_lib.h"

static float dc_present_pwm = 0;
static float servo_present_pwm = 0;
const float max_pwm = 21;

static int target_rps=0;

const float right_value = 20;
const float left_value = 5;
const float align_value = 14.3;

const float forward_value = 17.1;
const float fast_forward_value = 21.0;
const float reverse_value = 10;
const float neutral_value = 15;
const float stop_value = 15;
const float brake_value = 15.6;

const float fwd_max_value = 30;
const float rev_max_value = 1;

void Motor::motor_init(void)
{
    dc = new PWM(PWM::pwm1, 100);
    servo = new PWM(PWM::pwm3, 100);
    dc_pwm_set(neutral_value);
    servo_pwm_set(align_value);

}

void Motor::align(void)
{
    servo_pwm_set(align_value);
}

void Motor::steer_left(void)
{
    servo_pwm_set(left_value);
}

void Motor::steer_right(void)
{
    servo_pwm_set(right_value);
}

void Motor::steer(float driftVal)
{
    servo_pwm_set(align_value + driftVal);
    dc_pwm_set(dc_present_pwm);
}

void Motor::stop(void)
{
    dc_pwm_set(stop_value);
    //setting the rps to 0 so that PID logic
    //does not get executed
    target_rps=0;
}

void Motor::brake(void)
{
    dc_pwm_set(brake_value);
    //setting the rps to 0 so that PID logic
    //does not get executed
    target_rps =0;
}


void Motor::move_forward(void)
{
    //multiple move forwards overrides my PID
    if (dc_present_pwm == stop_value) {
        dc_pwm_set(forward_value);

    }
    steer(0.0);
}

void Motor::move_reverse(void)
{
    dc_pwm_set(reverse_value);
}

void Motor::dc_set_neutral()
{
    dc_pwm_set(neutral_value);

}

void Motor::dc_set_fwdMax()
{
    dc_pwm_set(fwd_max_value);
}

void Motor::dc_set_revMax()
{
    dc_pwm_set(rev_max_value);
}

//increase/decrease the motor speed upHill/downHill using RPM sensor
//calculating rotations per second(rps)
void Motor::motor_execute(void)
{
    static int diff_rps;
    float next_pwm;
    int present_rps = get_rps();
    LD.setNumber(present_rps);
    if (target_rps != 0) {
        if (present_rps < target_rps) {
            //difference in rps
            diff_rps = target_rps - present_rps;
            //increasing the PWM gradually
                next_pwm = dc_present_pwm + (0.5 * diff_rps);
                if (next_pwm >= fast_forward_value) {
                    next_pwm = fast_forward_value;
                }
                dc_pwm_set(next_pwm);
        }
        else if (present_rps == target_rps && (dc_present_pwm > 20)) {
            dc_pwm_set(forward_value);
        }
        else if (present_rps > target_rps) {
            //difference in rps
            diff_rps = present_rps - target_rps;
            //increasing the PWM gradually,
            //found 0.8 coefficient value by trial and errors
                next_pwm = dc_present_pwm - (0.8 * diff_rps);
                if (next_pwm <= forward_value ) {
                    next_pwm = forward_value;
                }
                dc_pwm_set(next_pwm);

        }

    }
    else if (present_rps) {
        dc_pwm_set(dc_present_pwm);
    }
}


void Motor::set_speed(float msec)
{
   target_rps = msec * 4;
}

//change the servo PWM only if new PWM value shows up,
//else continue with the existing PWM
void Motor::servo_pwm_set(float pwm)
{
    if (servo_present_pwm != pwm) {
        servo->set(pwm);
        servo_present_pwm = pwm;
    }
}

//change the dc PWM only if new PWM value shows up,
//else continue with the existing PWM
void Motor::dc_pwm_set(float pwm)
{
    if (dc_present_pwm != pwm) {
        dc->set(pwm);
        dc_present_pwm = pwm;
    }
}
