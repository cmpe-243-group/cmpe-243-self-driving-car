/*
 * rpm_wrapper.h
 *
 *  Created on: Apr 20, 2019
 *      Author: Uma
 */

#ifndef C_MOTOR_RPM_H_
#define C_MOTOR_RPM_H_

#include <c_code/motor_rpm.hpp>

#ifdef __cplusplus
extern "C" {
#endif

void c_ledlight(int num);
void c_LED_display(int num);
void c_rpm_init(void);
void c_rpm_callback(void);
int  c_get_rps(void);
void c_rps_run_100Hz(int count);


#ifdef __cplusplus
}
#endif










#endif /* C_MOTOR_RPM_H_ */
