/*
 * motor_09Apr.cpp

 *
 *  Created on: Apr 5, 2019
 *      Author: Uma
 */
#include <c_code/c_motor_class.h>
#include <c_code/c_motor_rpm.h>
#include <c_code/motor_class.hpp>
#include <c_code/motor_rpm.hpp>
#include <stdio.h>
#include <printf_lib.h>



static Motor m1;
void c_motor_init(void){
    m1.motor_init();
}

void c_align(void){
    m1.align();
}

void c_move_forward(void){
    m1.move_forward();
}

void c_steer_left(void){
    m1.steer_left();
}

void c_steer_right(void){
    m1.steer_right();
}

void c_stop(void){
    m1.stop();
}

void c_brake(void){
    m1.brake();
}

void c_move_reverse(void){
    m1.move_reverse();
}

void c_setNeutral(void){
    m1.dc_set_neutral();
}

void c_setfwd(void){
    m1.dc_set_fwdMax();
}

void c_setrev(void){
    m1.dc_set_revMax();
}

void c_steer(float driftVal){
    m1.steer(driftVal);
}

void  c_motor_execute(void){
    m1.motor_execute();
}

void c_set_speed(float msec){
   m1.set_speed(msec);
}



