/*
 * motor_09Apr.h
 *
 *  Created on: Apr 5, 2019
 *      Author: Uma
 */

#ifndef C_MOTOR_CLASS_H_
#define C_MOTOR_CLASS_H_
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif



void c_motor_init(void);
void c_align(void);
void c_move_forward(void);
void c_steer_left(void);
void c_steer_right(void);
void c_stop(void);
void c_brake(void);
void c_move_reverse(void);
void c_setNeutral(void);
void c_setfwd(void);
void c_setrev(void);
void c_motor_execute(void);
void c_steer(float driftVal);
void c_set_speed(float msec);
uint8_t c_get_rpm(void);




#ifdef __cplusplus
}
#endif




#endif /* C_MOTOR_CLASS_H_ */
