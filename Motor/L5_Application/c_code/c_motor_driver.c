/*
 * motor_driver.c

 *
 *  Created on: Apr 25, 2019
 *      Author: Uma
 */

#include "c_motor_driver.h"
#include "c_motor_rpm.h"
#include "c_code/c_motor_class.h"
#include "lcd.h"
#include <stdio.h>
#include "utilities.h"

static float speed = 0;
static bool rev_cmd = false;

//Executing any of the functions based on master commands,
//Also printing command name on LCD
void drive_motor(MOTOR_CMD_t motor_cmd)
{
    switch (motor_cmd.STEER_CMD_enum) {
        case steer_straight:
            c_move_forward();
            c_steer(0.0);
            lcd_print("Cmd is:straight \n", 0, 2);
            break;
        case stop:
            c_stop();
            lcd_print("Cmd is:Stop     \n", 0, 2);
            break;
        case steer_left:
            c_steer(-6.0);
            lcd_print("Cmd is:Left     \n", 0, 2);
            break;
        case steer_right:
            c_steer(6.0);
            lcd_print("Cmd is:Right    \n", 0, 2);
            break;
        case slight_left:
            c_steer(-3.00);
            lcd_print("Cmd is:sl_l    \n", 0, 2);
            break;
        case slight_right:
            c_steer(3.00);
            lcd_print("Cmd is:sl_r    \n", 0, 2);
            break;
        case reverse:
            set_rev_flag();
            lcd_print("Cmd is:reverse  \n", 0, 2);
            break;
        case left_reverse:
            c_steer(-6.0);
            set_rev_flag();
            lcd_print("Cmd is:l_rev   \n", 0, 2);
            break;
        case right_reverse:
            c_steer(6.0);
            set_rev_flag();
            lcd_print("Cmd is:r_rev    \n", 0, 2);
            break;
        case brake:
            c_brake();
            lcd_print("Cmd is:brake    \n", 0, 2);
            break;
        default:
            c_move_forward();
            lcd_print("Cmd is:keepGoing\n", 0, 2);
            break;
    }

    if (motor_cmd.SPEED_CMD) {
        c_set_speed(motor_cmd.SPEED_CMD);
        speed = motor_cmd.SPEED_CMD;
    }
}

float get_speed()
{
    return speed;

}

void set_rev_flag()
{
    rev_cmd = true;
}

int reduce_10Hz_rev1_frequency(uint32_t count)
{
    return ((count % 3) == 2);
}

int reduce_10Hz_rev2_frequency(uint32_t count)
{
    return ((count % 3) == 1);
}

bool get_rev_status()
{
    if (rev_cmd == true) {
        return true;
    }
    else {
        return false;
    }
}

void rev_execute(int count)
{
    c_stop();
    if (reduce_10Hz_rev1_frequency(count)) {
        c_move_reverse();
    }
    if (reduce_10Hz_rev2_frequency(count)) {
        c_move_reverse();
        led_display_set(c_get_rps());
    }
    rev_cmd = false;
}

void print_1Hz_tar_dist(void)
{
    char distance[6];
    GPS_TARGET_HEADING_t tar = CAN_Receive_gps_distance_command();
    sprintf(distance, "%.2f", tar);
    distance_print(distance);
}

void print_1Hz_speed(void)
{
    char s[6];
    float msec = get_speed();
    sprintf(s, "%.2f", msec);
    speed_print(s);
}
