/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */
#include  "c_calibrate.h"
#include  "c_motor_class.h"
#include  "c_motor_rpm.h"
#include  "c_motor_driver.h"
#include  "c_can_comm.h"
#include  <stdint.h>
#include  <stdbool.h>
#include  "string.h"
#include  <stdio.h>
#include "lcd.h"
#include "printf_lib.h"
#include  "../periodic_scheduler/periodic_callback.h"

bool C_period_init(void)
{
    //all init functions are called here
    c_motor_init();
    c_rpm_init();
    CAN_Init();
    lcd_uart_init();
    lcd_init();
    return true;
}

bool C_period_reg_tlm(void)
{
    return true;
}

void C_period_1Hz(uint32_t count)
{
    (void) count;
    if (CAN_is_bus_off(can1)) {
        CAN_reset_bus(can1);
    }

    bool cal = c_calibrate_sw(count);

    //target distance remaining to destination and speed printed on LCD
    print_1Hz_tar_dist();
    print_1Hz_speed();

}

void C_period_10Hz(uint32_t count)
{

    if (get_rev_status() == true) {
        rev_execute(count);
    }

}

int reduce_100Hz_frequency(uint32_t count)
{
    return ((count % 25) == 24);
}

void C_period_100Hz(uint32_t count)
{
    c_rps_run_100Hz(count);
    CAN_Recieve_100Hz();
    CAN_Send_100Hz();
    MOTOR_CMD_t motor_cmd = CAN_Receive_get_motor_command();

    if (get_rev_status() == false) {
        drive_motor(motor_cmd);
    }

    if (reduce_100Hz_frequency(count)) {
        if (get_rev_status() == false) {
            c_motor_execute();
        }

    }

}

void C_period_1000Hz(uint32_t count)
{
    (void) count;

}

