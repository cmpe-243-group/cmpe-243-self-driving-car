/*
 * c_motor_driver.h
 *
 *  Created on: Apr 26, 2019
 *      Author: Uma
 */

#ifndef C_MOTOR_DRIVER_H_
#define C_MOTOR_DRIVER_H_

#include <c_code/c_can_comm.h>





void drive_motor(MOTOR_CMD_t motor_cmd);
float get_speed();
void rev_execute(int count);
void set_rev_flag();
bool get_rev_status();
void print_1Hz_tar_dist(void);
void print_1Hz_speed(void);




#endif /* C_MOTOR_DRIVER_H_ */
