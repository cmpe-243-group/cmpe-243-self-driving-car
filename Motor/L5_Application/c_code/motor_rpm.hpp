/*
 * motor_rpm.h
 *
 *  Created on: Apr 18, 2019
 *      Author: Uma
 */

#ifndef MOTOR_RPM_H_
#define MOTOR_RPM_H_


void rpm_init(void);
void rpm_callback(void);
int get_rps(void);
void rps_run_100Hz(int count);





#endif /* MOTOR_RPM_H_ */
