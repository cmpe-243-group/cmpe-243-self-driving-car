/*
 * master_helper.h
 *
 *  Created on: Apr 18, 2019
 *      Author: Sag
 */

#ifndef MASTER_FUNCTIONS_H_
#define MASTER_FUNCTIONS_H_

#if 1
#ifdef __cplusplus
extern "C"{
#endif

#include <stdint.h>
#include <stdbool.h>
#include "_can_dbc/generated_can.h"

typedef enum {
    try_right = 3,
    try_left = 1,
    try_straight = 2
} try_to_steer_E;

uint8_t receive_and_process_sensor(uint32_t count);

void master_rx(void);

void init_sucess(void);

void set_debug_messages(void);

void process_received_data(void);

try_to_steer_E process_target_heading(float current_heading, float target_heading);

void master_tx(void);

int16_t get_x_axis();

int16_t get_y_axis();

int16_t get_z_axis();

float get_rpm_cmd(uint8_t rpm_val);

uint8_t receive_rpm_val(void);

STEER_CMD_enum_E get_steer_command(uint16_t l, uint16_t c, uint16_t r,uint8_t count);

void decide_reverse_mode(int rps,uint16_t l,uint16_t r);

#endif /* MASTER_HELPER_H_ */


#ifdef __cplusplus
}
#endif
#endif
