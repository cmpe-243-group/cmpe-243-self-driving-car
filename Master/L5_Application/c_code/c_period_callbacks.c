#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "c_period_callbacks.h"
#include "c_code/master_functions.h"
#include "string.h"
#include "c_code/helper.h"
#include "printf_lib.h"
#include "can.h"

bool C_period_init(void) {
    bool init_can = canInit();
    if(init_can)init_sucess();
    return init_can;
}

bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;

    resetCanIfOff();
}

void C_period_10Hz(uint32_t count) {
    (void) count;
}

void C_period_100Hz(uint32_t count) {
    //(void) count;

    master_rx();
    process_received_data();
    master_tx();
}


void C_period_1000Hz(uint32_t count) {
    (void) count;
}