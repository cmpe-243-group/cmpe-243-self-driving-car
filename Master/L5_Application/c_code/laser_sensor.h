#ifndef LASER_SENSOR_H_
#define LASER_SENSOR_H_

#if 1
#ifdef __cplusplus
extern "C"{
#endif

#include <stdint.h>
#include <stdbool.h>
//#include "gpio.hpp"

#define DEFAULT_SENSOR_ADDR 0x52

enum Laser_Sensor_Register{
    I2C_SLAVE_DEVICE_ADDRESS                    = 0x8A,
    SYSRANGE_START                              = 0x00,
    RESULT_RANGE_STATUS                         = 0x14,
};

typedef enum {
    center = 0,
    left = 1,
    right = 3,
} sensors_t;

typedef struct {
    uint8_t center;
    uint8_t left;
    uint8_t right;
    uint8_t cur;
} Laser_addresses_S;

typedef struct{
      uint16_t left_sensor_val;
      uint16_t right_sensor_val;
      uint16_t center_sensor_val;
      uint16_t prev_left;
      uint16_t prev_right;
      uint16_t prev_cen;
  } Laser_values_S;

void temp_one_sensor(void);

void init_sensor_addresses(void);

bool i2c_init_sensors(uint8_t I2CAddr_Laser_sensor);

bool i2c_init(unsigned int speedInKhz);

void init_sensors(void);

uint16_t get_sensor_val(sensors_t l);

void reset_GPIO(void);

void sendToMaster(void);

bool check_sensor_response(uint8_t device_address);

void take_readings(int count);

bool set_laser_address(void);

uint8_t readReg_laser(uint8_t device_address,uint8_t register_address);

bool writeReg_laser(uint8_t deviceAddress, uint8_t registerAddress, uint8_t value);

uint16_t readRangeSingleMillimeters(uint8_t adder);




#ifdef __cplusplus
}
#endif
#endif

#endif
