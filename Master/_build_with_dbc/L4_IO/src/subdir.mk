################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../L4_IO/src/gpio.cpp \
../L4_IO/src/io_source.cpp \
../L4_IO/src/storage.cpp 

OBJS += \
./L4_IO/src/gpio.o \
./L4_IO/src/io_source.o \
./L4_IO/src/storage.o 

CPP_DEPS += \
./L4_IO/src/gpio.d \
./L4_IO/src/io_source.d \
./L4_IO/src/storage.d 


# Each subdirectory must supply rules for building sources it contributes
L4_IO/src/%.o: ../L4_IO/src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -ffunction-sections -fdata-sections -Wall -Wshadow -Wlogical-op -Wfloat-equal -DBUILD_CFG_MPU=0 -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\newlib" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L0_LowLevel" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L1_FreeRTOS" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L1_FreeRTOS\include" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L1_FreeRTOS\portable" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L1_FreeRTOS\portable\no_mpu" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L2_Drivers" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L2_Drivers\base" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L3_Utils" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L3_Utils\tlm" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L4_IO" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L4_IO\fat" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L4_IO\wireless" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L5_Application" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L5_Application\c_code" -std=gnu++11 -fabi-version=0 -fno-exceptions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


