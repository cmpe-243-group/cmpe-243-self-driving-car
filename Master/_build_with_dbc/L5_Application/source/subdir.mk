################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../L5_Application/source/high_level_init.cpp \
../L5_Application/source/remote.cpp \
../L5_Application/source/terminal.cpp 

OBJS += \
./L5_Application/source/high_level_init.o \
./L5_Application/source/remote.o \
./L5_Application/source/terminal.o 

CPP_DEPS += \
./L5_Application/source/high_level_init.d \
./L5_Application/source/remote.d \
./L5_Application/source/terminal.d 


# Each subdirectory must supply rules for building sources it contributes
L5_Application/source/%.o: ../L5_Application/source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -ffunction-sections -fdata-sections -Wall -Wshadow -Wlogical-op -Wfloat-equal -DBUILD_CFG_MPU=0 -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\newlib" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L0_LowLevel" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L1_FreeRTOS" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L1_FreeRTOS\include" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L1_FreeRTOS\portable" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L1_FreeRTOS\portable\no_mpu" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L2_Drivers" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L2_Drivers\base" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L3_Utils" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L3_Utils\tlm" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L4_IO" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L4_IO\fat" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L4_IO\wireless" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L5_Application" -I"C:\Users\Kevin\Documents\CMPE243\cmpe-243-self-driving-car\Master\L5_Application\c_code" -std=gnu++11 -fabi-version=0 -fno-exceptions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


