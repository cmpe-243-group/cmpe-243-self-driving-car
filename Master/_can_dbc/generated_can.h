/// DBC file: ../../243.dbc    Self node: 'MASTER'  (ALL = 0)
/// This file can be included by a source file, for example: #include "generated.h"
#ifndef __GENEARTED_DBC_PARSER
#define __GENERATED_DBC_PARSER
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>



/// Extern function needed for dbc_encode_and_send()
extern bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]);

/// Missing in Action structure
typedef struct {
    uint32_t is_mia : 1;          ///< Missing in action flag
    uint32_t mia_counter_ms : 31; ///< Missing in action counter
} dbc_mia_info_t;

/// CAN message header structure
typedef struct { 
    uint32_t mid; ///< Message ID of the message
    uint8_t  dlc; ///< Data length of the message
} dbc_msg_hdr_t; 

static const dbc_msg_hdr_t MOTOR_CMD_HDR =                        {  100, 2 };
static const dbc_msg_hdr_t RPM_VALUE_CMD_HDR =                    {  105, 2 };
static const dbc_msg_hdr_t COMPASS_CMD_HDR =                      {  110, 4 };
static const dbc_msg_hdr_t GPS_CURRENT_LAT_LONG_HDR =             {  120, 8 };
static const dbc_msg_hdr_t SENSOR_STATUS_HDR =                    {  200, 8 };
static const dbc_msg_hdr_t BRIDGE_STOP_HDR =                      {  300, 1 };
static const dbc_msg_hdr_t BRIDGE_GO_HDR =                        {  310, 1 };




/// Message: MOTOR_CMD from 'MASTER', DLC: 2 byte(s), MID: 100
typedef struct {
    uint8_t MOTOR_CMD;                        ///< B7:0   Destination: MOTOR
    float SPEED;                              ///< B15:8   Destination: MOTOR

    // No dbc_mia_info_t for a message that we will send
} MOTOR_CMD_t;


/// Message: RPM_VALUE_CMD from 'MOTOR', DLC: 2 byte(s), MID: 105
typedef struct {
    uint8_t RPM_VALUE;                        ///< B7:0   Destination: MASTER
    uint8_t DEBUG_MOTOR_;                     ///< B15:8   Destination: MASTER

    dbc_mia_info_t mia_info;
} RPM_VALUE_CMD_t;


/// Message: COMPASS_CMD from 'GPS', DLC: 4 byte(s), MID: 110
typedef struct {
    float HEADING;                            ///< B31:0   Destination: BRIDGE,MASTER

    dbc_mia_info_t mia_info;
} COMPASS_CMD_t;


/// Message: GPS_CURRENT_LAT_LONG from 'GPS', DLC: 8 byte(s), MID: 120
typedef struct {
    float CUR_LAT;                            ///< B31:0   Destination: BRIDGE,MASTER
    float CUR_LONG;                           ///< B63:32   Destination: BRIDGE,MASTER

    dbc_mia_info_t mia_info;
} GPS_CURRENT_LAT_LONG_t;


/// Message: SENSOR_STATUS from 'SENSOR', DLC: 8 byte(s), MID: 200
typedef struct {
    uint16_t SENSOR_FRONT;                    ///< B15:0   Destination: BRIDGE,MASTER
    uint16_t SENSOR_LEFT;                     ///< B31:16   Destination: BRIDGE,MASTER
    uint16_t SENSOR_RIGHT;                    ///< B47:32   Destination: BRIDGE,MASTER
    uint8_t SENSOR_HEARTBEAT : 1;             ///< B48:48   Destination: MASTER

    dbc_mia_info_t mia_info;
} SENSOR_STATUS_t;


/// Message: BRIDGE_STOP from 'BRIDGE', DLC: 1 byte(s), MID: 300
typedef struct {
    uint8_t BRIDGE_STOP : 1;                  ///< B0:0   Destination: MASTER

    dbc_mia_info_t mia_info;
} BRIDGE_STOP_t;


/// Message: BRIDGE_GO from 'BRIDGE', DLC: 1 byte(s), MID: 310
typedef struct {
    uint8_t BRIDGE_GO : 1;                    ///< B0:0   Destination: MASTER

    dbc_mia_info_t mia_info;
} BRIDGE_GO_t;


/// @{ These 'externs' need to be defined in a source file of your project
extern const uint32_t                             RPM_VALUE_CMD__MIA_MS;
extern const RPM_VALUE_CMD_t                      RPM_VALUE_CMD__MIA_MSG;
extern const uint32_t                             COMPASS_CMD__MIA_MS;
extern const COMPASS_CMD_t                        COMPASS_CMD__MIA_MSG;
extern const uint32_t                             GPS_CURRENT_LAT_LONG__MIA_MS;
extern const GPS_CURRENT_LAT_LONG_t               GPS_CURRENT_LAT_LONG__MIA_MSG;
extern const uint32_t                             SENSOR_STATUS__MIA_MS;
extern const SENSOR_STATUS_t                      SENSOR_STATUS__MIA_MSG;
extern const uint32_t                             BRIDGE_STOP__MIA_MS;
extern const BRIDGE_STOP_t                        BRIDGE_STOP__MIA_MSG;
extern const uint32_t                             BRIDGE_GO__MIA_MS;
extern const BRIDGE_GO_t                          BRIDGE_GO__MIA_MSG;
/// @}


/// Encode MASTER's 'MOTOR_CMD' message
/// @returns the message header of this message
static inline dbc_msg_hdr_t dbc_encode_MOTOR_CMD(uint8_t bytes[8], MOTOR_CMD_t *from)
{
    uint32_t raw;
    bytes[0]=bytes[1]=bytes[2]=bytes[3]=bytes[4]=bytes[5]=bytes[6]=bytes[7]=0;

    raw = ((uint32_t)(((from->MOTOR_CMD)))) & 0xff;
    bytes[0] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B0

    raw = ((uint32_t)(((from->SPEED) / 0.1) + 0.5)) & 0xff;
    bytes[1] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B8

    return MOTOR_CMD_HDR;
}

/// Encode and send for dbc_encode_MOTOR_CMD() message
static inline bool dbc_encode_and_send_MOTOR_CMD(MOTOR_CMD_t *from)
{
    uint8_t bytes[8];
    const dbc_msg_hdr_t hdr = dbc_encode_MOTOR_CMD(bytes, from);
    return dbc_app_send_can_msg(hdr.mid, hdr.dlc, bytes);
}



/// Not generating code for dbc_encode_RPM_VALUE_CMD() since the sender is MOTOR and we are MASTER

/// Not generating code for dbc_encode_COMPASS_CMD() since the sender is GPS and we are MASTER

/// Not generating code for dbc_encode_GPS_CURRENT_LAT_LONG() since the sender is GPS and we are MASTER

/// Not generating code for dbc_encode_SENSOR_STATUS() since the sender is SENSOR and we are MASTER

/// Not generating code for dbc_encode_BRIDGE_STOP() since the sender is BRIDGE and we are MASTER

/// Not generating code for dbc_encode_BRIDGE_GO() since the sender is BRIDGE and we are MASTER

/// Not generating code for dbc_decode_MOTOR_CMD() since 'MASTER' is not the recipient of any of the signals

/// Decode MOTOR's 'RPM_VALUE_CMD' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_RPM_VALUE_CMD(RPM_VALUE_CMD_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != RPM_VALUE_CMD_HDR.dlc || hdr->mid != RPM_VALUE_CMD_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->RPM_VALUE = ((raw));
    raw  = ((uint32_t)((bytes[1]))); ///< 8 bit(s) from B8
    to->DEBUG_MOTOR_ = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode GPS's 'COMPASS_CMD' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_COMPASS_CMD(COMPASS_CMD_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != COMPASS_CMD_HDR.dlc || hdr->mid != COMPASS_CMD_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    raw |= ((uint32_t)((bytes[1]))) << 8; ///< 8 bit(s) from B8
    raw |= ((uint32_t)((bytes[2]))) << 16; ///< 8 bit(s) from B16
    raw |= ((uint32_t)((bytes[3]))) << 24; ///< 8 bit(s) from B24
    to->HEADING = ((raw * 0.1));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode GPS's 'GPS_CURRENT_LAT_LONG' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_GPS_CURRENT_LAT_LONG(GPS_CURRENT_LAT_LONG_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != GPS_CURRENT_LAT_LONG_HDR.dlc || hdr->mid != GPS_CURRENT_LAT_LONG_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    raw |= ((uint32_t)((bytes[1]))) << 8; ///< 8 bit(s) from B8
    raw |= ((uint32_t)((bytes[2]))) << 16; ///< 8 bit(s) from B16
    raw |= ((uint32_t)((bytes[3]))) << 24; ///< 8 bit(s) from B24
    if (raw & (1 << 31)) { // Check signed bit
        to->CUR_LAT = ((((0xFFFFFFFF << 31) | raw) * 0.1));
    } else {
        to->CUR_LAT = ((raw * 0.1));
    }
    raw  = ((uint32_t)((bytes[4]))); ///< 8 bit(s) from B32
    raw |= ((uint32_t)((bytes[5]))) << 8; ///< 8 bit(s) from B40
    raw |= ((uint32_t)((bytes[6]))) << 16; ///< 8 bit(s) from B48
    raw |= ((uint32_t)((bytes[7]))) << 24; ///< 8 bit(s) from B56
    if (raw & (1 << 31)) { // Check signed bit
        to->CUR_LONG = ((((0xFFFFFFFF << 31) | raw) * 0.1));
    } else {
        to->CUR_LONG = ((raw * 0.1));
    }

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode SENSOR's 'SENSOR_STATUS' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_SENSOR_STATUS(SENSOR_STATUS_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != SENSOR_STATUS_HDR.dlc || hdr->mid != SENSOR_STATUS_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    raw |= ((uint32_t)((bytes[1]))) << 8; ///< 8 bit(s) from B8
    to->SENSOR_FRONT = ((raw));
    raw  = ((uint32_t)((bytes[2]))); ///< 8 bit(s) from B16
    raw |= ((uint32_t)((bytes[3]))) << 8; ///< 8 bit(s) from B24
    to->SENSOR_LEFT = ((raw));
    raw  = ((uint32_t)((bytes[4]))); ///< 8 bit(s) from B32
    raw |= ((uint32_t)((bytes[5]))) << 8; ///< 8 bit(s) from B40
    to->SENSOR_RIGHT = ((raw));
    raw  = ((uint32_t)((bytes[6]) & 0x01)); ///< 1 bit(s) from B48
    to->SENSOR_HEARTBEAT = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode BRIDGE's 'BRIDGE_STOP' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_BRIDGE_STOP(BRIDGE_STOP_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != BRIDGE_STOP_HDR.dlc || hdr->mid != BRIDGE_STOP_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]) & 0x01)); ///< 1 bit(s) from B0
    to->BRIDGE_STOP = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode BRIDGE's 'BRIDGE_GO' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_BRIDGE_GO(BRIDGE_GO_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != BRIDGE_GO_HDR.dlc || hdr->mid != BRIDGE_GO_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]) & 0x01)); ///< 1 bit(s) from B0
    to->BRIDGE_GO = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Handle the MIA for MOTOR's RPM_VALUE_CMD message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_RPM_VALUE_CMD(RPM_VALUE_CMD_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= RPM_VALUE_CMD__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = RPM_VALUE_CMD__MIA_MSG;
        msg->mia_info.mia_counter_ms = RPM_VALUE_CMD__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for GPS's COMPASS_CMD message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_COMPASS_CMD(COMPASS_CMD_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= COMPASS_CMD__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = COMPASS_CMD__MIA_MSG;
        msg->mia_info.mia_counter_ms = COMPASS_CMD__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for GPS's GPS_CURRENT_LAT_LONG message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_GPS_CURRENT_LAT_LONG(GPS_CURRENT_LAT_LONG_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= GPS_CURRENT_LAT_LONG__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = GPS_CURRENT_LAT_LONG__MIA_MSG;
        msg->mia_info.mia_counter_ms = GPS_CURRENT_LAT_LONG__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for SENSOR's SENSOR_STATUS message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_SENSOR_STATUS(SENSOR_STATUS_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= SENSOR_STATUS__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = SENSOR_STATUS__MIA_MSG;
        msg->mia_info.mia_counter_ms = SENSOR_STATUS__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for BRIDGE's BRIDGE_STOP message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_BRIDGE_STOP(BRIDGE_STOP_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= BRIDGE_STOP__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = BRIDGE_STOP__MIA_MSG;
        msg->mia_info.mia_counter_ms = BRIDGE_STOP__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for BRIDGE's BRIDGE_GO message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_BRIDGE_GO(BRIDGE_GO_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= BRIDGE_GO__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = BRIDGE_GO__MIA_MSG;
        msg->mia_info.mia_counter_ms = BRIDGE_GO__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

#endif
