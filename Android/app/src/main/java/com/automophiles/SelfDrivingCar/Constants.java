package com.automophiles.SelfDrivingCar;


public interface Constants
{

    //Our Xbee device address
    public static final String MY_XBEE_ADDRESS = "20:17:11:27:71:13";
    //bluetooth UI button states
    public static final int BLUETOOTH_BTN_CONNECTING = 1;
    public static final int BLUETOOTH_BTN_SELECT_DEST = 2;
    public static final int BLUETOOTH_BTN_SEND_DESTINATION = 3;
    public static final int BLUETOOTH_BTN_START = 4;
    public static final int BLUETOOTH_BTN_STOP = 5;

    //bluetooth service message types
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_HEADING = 6;
    public static final int MESSAGE_LATLNG = 7;
    public static final int MESSAGE_SENSORS = 8;
    public static final int MESSAGE_RPM = 9;
    public static final int MESSAGE_TARGET = 10;
    public static final int MESSAGE_FIX = 11;
    public static final int MESSAGE_GPS_CHKPT = 12;

    //key names
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    public static final String HEADING = "heading";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String LEFT_SENSOR = "left_sensor";
    public static final String FRONT_SENSOR = "front_sensor";
    public static final String RIGHT_SENSOR = "right_sensor";
    public static final String RPM_VALUE = "rpm_value";
    public static final String TARGET_HEADING = "target heading";
    public static final String TARGET_DISTANCE = "target distance";
    public static final String GPS_FIX = "gps_fix";
    public static final String GPS_NUM_CHECKPTS = "gps_num_checkpoints";
    public static final String GPS_CHKPT_ARRAY = "gps_chkpt_array";

    //toolbar states
    public static final int NOT_CONNECTED = 1;
    public static final int CONNECTING = 2;
    public static final int CONNECTED = 3;

    public static final double[][] gps_checkpoints = { //400 something bytes
            //Z clark hall eng building route
            {37.336270, -121.881944}, //0 southwest eng building
            {37.336185, -121.881882}, //1 southeast clark hall
            {37.336097, -121.881817}, //2 Chavez1
            {37.336005, -121.881998}, //3 north corner CCP [NOT CORNER]
            {37.336477, -121.881523}, //4 middle engineering student union west [NOT CORNER]
            //east student union corner route
            {37.337053, -121.880369}, //5 middle engineering student union east [NOT CORNER]
            {37.337267, -121.879901}, //6 northeast student union
            {37.337070, -121.879762}, //7 east student union [NOT CORNER]
            //arch fountain route
            {37.335476, -121.881439}, //8 east CCP [NOT CORNER]
            {37.335656,-121.881547}, //9 east middle CCP
            {37.335746, -121.881344}, //10 south arch fountain
            {37.335861, -121.881223}, //11 east arch fountain [NOT CORNER eh?]
            //around health center southeast corner route
            {37.334378, -121.881298}, //12 south health center [NOT CORNER]
            {37.334619, -121.880795}, //13 southeast health center above event center fountain
            {37.334875, -121.880990}, //14 east health center [NOT CORNER]
            //southwest event center corner route also connected to 14, possibly change this
            {37.334605, -121.880617}, //15 southwest event center corner
            {37.334867, -121.880035}, //16 south event center [NOT CORNER]
            //southeast event center corner route
            {37.335283, -121.879181}, //17 south event center right [NOT CORNER]
            {37.335479, -121.878746}, //18 southeast event center corner
            {37.335837, -121.879012}, //19 east valley fountain [NOT CORNER]
            //power plant corner route
            {37.336315, -121.879085}, //20 west business center [NOT CORNER]
            {37.336140, -121.878958}, //21 business center corner
            {37.336299, -121.878579}, //22 south business center [NOT CORNER]
            //north art building route
            {37.336185, -121.880542}, //23 north art building [NOT CORNER]
            {37.336363, -121.880144}, //24 art building corner
            {37.336275, -121.880069}, //25 in art building [NOT CORNER]
            //northeast parking lot route
            {37.337414, -121.879743}, //26 bottom left parking lot corner near atms
            {37.337682, -121.879937}, //27 top left parking lot corner
            {37.337891, -121.879490}, //28 top right parking lot corner
            {37.337614, -121.879284}, //29 bottom right parking lot corner

};


}
