package com.automophiles.SelfDrivingCar;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Formatter;
import java.util.UUID;

public class BluetoothService {
    private static final String TAG = "BluetoothService";
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String SensorsTAG = "Sensors";

    //members
    private final BluetoothAdapter mAdapter;
    private Handler mHandler;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState;
    private int mNewState;


    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_CONNECTING = 1; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 2;  // now connected to a remote device

    //BluetoothService Constructor
    public BluetoothService(Context context, Handler handler)
    {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mNewState = mState;
        mHandler = handler;
    }

    private synchronized void updateToolBarStatus()
    {
        mState = getState();
        Log.d(TAG, "updateToolBarStatus" + mState + " -> " + mNewState);

        mNewState = mState;

        //Give new connection state to handler so UI can update
        mHandler.obtainMessage(Constants.MESSAGE_STATE_CHANGE,mNewState,-1).sendToTarget();
    }

    //return current connection state
    public synchronized int getState()
    {
        return mState;
    }

    //Start connect thread to initiate a connection to a remote device
    public synchronized void connect(BluetoothDevice device)
    {
        Log.d(TAG, "connect to:" + device);

        //Cancel thread attempting to make a connection
        if(mState == STATE_CONNECTING)
        {
            if(mConnectThread != null)
            {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        //cancel connected threads currently managing a connection
        if(mConnectedThread != null)
        {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        //start thread to connect with device from arg
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();

        updateToolBarStatus();

    }

    //start ConnectedThread to manage a Bluetooth connection
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device)
    {
        Log.d(TAG, "Bluetooth connected");

        //cancel thread that completed the connection
        if(mConnectThread != null)
        {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        //cancel any thread currently running a connection
        if(mConnectedThread != null)
        {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        //start the thread to manage the connection
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        //Tell BluetoothFragment that connection successful
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        Log.d(TAG, "connected: device.getName()" + device.getName());
        bundle.putString(Constants.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        //update toolbar and button
        updateToolBarStatus();
    }

    //stop all threads
    public synchronized void stop()
    {
        Log.d(TAG, "stop");

        if(mConnectThread != null)
        {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if(mConnectedThread != null)
        {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        mState = STATE_NONE;
    }

    //write to ConnectedThread
    public void write(byte[] out)
    {
        ConnectedThread r;
        synchronized (this)
        {
            if(mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }

        //unsynchronized write
        r.write(out);
    }

    //tell BluetoothFragment that connection attempt failed
    private void connectionFailed()
    {
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Unable to connect device");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        mState = STATE_NONE;

        //keep restarting connection
        BluetoothDevice device = mAdapter.getRemoteDevice(Constants.MY_XBEE_ADDRESS);
        connect(device);
    }

    //tell BLuetoothFragment that connection was lost
    private void connectionLost()
    {
        Log.d(TAG,"connectionLost");
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        mState = STATE_NONE;

        updateToolBarStatus();

        //should attempt to reconnect, here or from BluetoothFragment connectDevice()?
        BluetoothDevice device = mAdapter.getRemoteDevice(Constants.MY_XBEE_ADDRESS);
        //Log.d(TAG, "connectionLost: getRemoteDevice returned deviceName:" + device.getName() + " ,deviceAddress:" + device.getAddress());
        connect(device);
    }

    private class ConnectThread extends Thread
    {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device)
        {
            mmDevice = device;
            BluetoothSocket tmp = null;

            //Get a BluetoothSocket for a connection with the given BluetoothDevice
            try
            {
                tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e)
            {
                Log.e(TAG, "Socket create failed", e);
            }
            mmSocket = tmp;
            mState = STATE_CONNECTING;

        }

        public void run()
        {
            Log.i(TAG, "Begin ConnectThread");
            mAdapter.cancelDiscovery();

            //Attempt a connection to the BluetoothSocket
            try
            {
                mmSocket.connect();
            }catch (IOException e)
            {
                //attempt to close the socket
                try
                {
                    mmSocket.close();
                }catch (IOException e2)
                {
                    Log.e(TAG, "unable to close()", e2);
                }
                connectionFailed();
                return;
            }

            //connection established at this point so reset mConnectThread to null
            synchronized (BluetoothService.this)
            {
                mConnectThread = null;

                //start the connected thread to manage connection
                connected(mmSocket,mmDevice);
            }
        }

        public void cancel()
        {
            try
            {
                mmSocket.close();
            }catch (IOException e)
            {
                Log.e(TAG, "close() of socket failed", e);
            }
        }
    }

    //This thread handles all incoming and outgoing transmissions
    private class ConnectedThread extends Thread
    {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer;

        //constructor
        public ConnectedThread(BluetoothSocket socket)
        {
            Log.d(TAG, "create ConnectedThread");
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            //retrieve input and output streams for provided socket
            try
            {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            }catch (IOException e)
            {
                Log.e(TAG, "temp sockets not created", e);
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
            mState = STATE_CONNECTED;
        }

        public void run()
        {
            Log.d(TAG, "Begin mConnectedThread");
            mmBuffer = new byte[2000];
            byte[] headingBytes = new byte[4];
            byte[] latBytes = new byte[4];
            byte[] longBytes = new byte[4];
            byte[] leftSensorBytes = new byte[4];
            byte[] frontSensorBytes = new byte[4];
            byte[] rightSensorBytes = new byte[4];
            byte[] rpmBytes = new byte[4];
            byte[] targetHeadingBytes = new byte[4];
            byte[] targetDistanceBytes = new byte[4];
            byte[] gpsFixBytes = new byte[2];
            byte[] gpsNextCheckpointBytes = new byte[4];
            //byte[] gpsNumCheckpoints = new byte[4];
            //byte[] gpsCheckpointIndexBytes = new byte[24];
            int numBytes = 0;

            //continually read input stream from socket
            while(mState == STATE_CONNECTED)
            {
                try
                {
                    Log.d(TAG, "mConnectedThread run");
                    //read a byte at a time
                    numBytes = mmInStream.read(mmBuffer);
                    Log.d(TAG, "Read from buffer" + mmBuffer + " numBytes: " + numBytes);
                    if(numBytes > 0)
                    {
                        //start of heading frame
                        if(mmBuffer[numBytes-1] == 0x48) // "H"
                        {
                            Log.d(TAG, "Heading recognized");
                            mmInStream.read(headingBytes, 0, 4); //read the next 4 bytes into headingBytes
                            Formatter formatter = new Formatter();
                            for (byte b : headingBytes)
                            {
                                formatter.format("%02x", b);
                            }
                            Log.d(TAG, "Heading: Read from buffer " + formatter.toString());
                            float heading = ByteBuffer.wrap(headingBytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                            Log.d(TAG, "Heading: converted to float is " + heading);
                            if(heading > 0 && heading < 360)
                            {
                                Message msg = mHandler.obtainMessage(Constants.MESSAGE_HEADING);
                                Bundle bundle = new Bundle();
                                bundle.putFloat(Constants.HEADING, heading);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                            }
                        }
                        else if(mmBuffer[numBytes-1] == 0x4C) // "L" : start of cur car lat lng frame
                        {
                            Log.d(TAG, "LatLng recognized");
                            mmInStream.read(latBytes, 0, 4); //read the next 4 bytes into latBytes
                            mmInStream.read(longBytes, 0, 4);//read the next 4 bytes into longBytes
                            Formatter formatter = new Formatter();
                            for(byte b : latBytes)
                            {
                                formatter.format("%02x", b);
                            }
                            float lat = ByteBuffer.wrap(latBytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                            Log.d(TAG, "Lat: converted to float is " + lat);
                            for(byte b : longBytes)
                            {
                                formatter.format("%02x", b);
                            }
                            float lng = ByteBuffer.wrap(longBytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                            Log.d(TAG, "Lng: converted to float is " + lng);
                            if((lat > 36 && lat < 38) && (lng > -122 && lng < -120))
                            {
                                Message msg = mHandler.obtainMessage(Constants.MESSAGE_LATLNG);
                                Bundle bundle = new Bundle();
                                bundle.putFloat(Constants.LAT, lat);
                                bundle.putFloat(Constants.LNG, lng);
                                msg.setData(bundle);
                                mHandler.sendMessage(msg);
                            }
                        }
                        else if(mmBuffer[numBytes-1] == 0x53) // 'S'
                        {
                            Log.d(SensorsTAG, "Sensors recognized");
                            mmInStream.read(leftSensorBytes, 0, 2);
                            mmInStream.read(frontSensorBytes, 0, 2);
                            mmInStream.read(rightSensorBytes, 0, 2);
                            Formatter formatter = new Formatter();
                            for (byte b : leftSensorBytes)
                            {
                                formatter.format("%02x", b);
                            }
                            Log.d(SensorsTAG, "L: " + formatter.toString());
                            //convert byte array to int
                            int leftSensorValue = ByteBuffer.wrap(leftSensorBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
                            int frontSensorValue = ByteBuffer.wrap(frontSensorBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
                            int rightSensorValue = ByteBuffer.wrap(rightSensorBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
                            Log.d(TAG, "Sensors after decode: L:" + leftSensorValue + " F: " + frontSensorValue + " R: " + rightSensorValue);
                            Message msg = mHandler.obtainMessage(Constants.MESSAGE_SENSORS);
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.LEFT_SENSOR, leftSensorValue);
                            bundle.putInt(Constants.FRONT_SENSOR, frontSensorValue);
                            bundle.putInt(Constants.RIGHT_SENSOR, rightSensorValue);
                            msg.setData(bundle);
                            mHandler.sendMessage(msg);
                        }
                        else if(mmBuffer[numBytes-1] == 0x4F) //start of checkpoint frame
                        {
                            mmInStream.read(gpsNextCheckpointBytes, 0, 1);
                            Log.d(TAG, "GPS Checkpoint index frame recognized");
                            int gpsNextCheckpoint = ByteBuffer.wrap(gpsNextCheckpointBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
                            Log.d(TAG, "gpsNextCheckpointIdx after decode is " + gpsNextCheckpoint);
                            /*
                            mmInStream.read(gpsNumCheckpoints, 0, 1);
                            int numCheckpoints = ByteBuffer.wrap(gpsNumCheckpoints).order(ByteOrder.LITTLE_ENDIAN).getInt();
                            Log.d(TAG, "numCheckpoints for route: " + numCheckpoints);
                            int[] checkpointIdx = new int[5];

                            for(int i = 0; i < numCheckpoints; i++)
                            {
                                mmInStream.read(gpsCheckpointIndexBytes,i , i+1);
                                checkpointIdx[i] = gpsCheckpointIndexBytes[i] & 0xFF;
                                //checkpointIdx[i] = ByteBuffer.wrap(Arrays.copyOfRange(gpsCheckpointIndexBytes, i, i+1)).order(ByteOrder.LITTLE_ENDIAN).getInt();
                                Log.d(TAG, "Received checkpoint index " + checkpointIdx[i]);
                            }
                            Message msg = mHandler.obtainMessage(Constants.MESSAGE_GPS_CHKPT);
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.GPS_NUM_CHECKPTS, numCheckpoints);
                            bundle.putIntArray(Constants.GPS_CHKPT_ARRAY, checkpointIdx);
                            msg.setData(bundle);
                            mHandler.sendMessage(msg);
                            */

                        }
                        else if(mmBuffer[numBytes-1] == 0x52) // 'R'
                        {
                            Log.d(TAG, "RPM recognized");
                            mmInStream.read(rpmBytes, 0, 1);
                            Formatter formatter = new Formatter();
                            for(byte b : rpmBytes)
                            {
                                formatter.format("%02x", b);
                            }
                            Log.d(TAG, "RPM: " + formatter.toString());
                            //convert rpmBytes to int
                            int rpmValue = ByteBuffer.wrap(rpmBytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
                            Log.d(TAG, "rpm after decode: " + rpmValue);
                            Message msg = mHandler.obtainMessage(Constants.MESSAGE_RPM);
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.RPM_VALUE, rpmValue);
                            msg.setData(bundle);
                            mHandler.sendMessage(msg);
                        }
                        else if(mmBuffer[numBytes -1] == 0x5A) //'Z' : target heading
                        {
                            Log.d(TAG, "Target recognized");
                            mmInStream.read(targetHeadingBytes, 0, 4);
                            mmInStream.read(targetDistanceBytes, 0, 4);
                            Formatter formatter = new Formatter();
                            for(byte b : targetHeadingBytes)
                            {
                                formatter.format("%02x", b);
                            }
                            Log.d(TAG, "Target Heading: " + formatter.toString());
                            formatter = new Formatter();
                            for(byte b : targetDistanceBytes)
                            {
                                formatter.format("%02x", b);
                            }
                            Log.d(TAG, "Target Distance: " + formatter.toString());
                            float targetHeading = ByteBuffer.wrap(targetHeadingBytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                            float targetDistance = ByteBuffer.wrap(targetDistanceBytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();

                            Log.d(TAG, "After decode: Target Heading: " + targetHeading + " Distance: " + targetDistance + " m");
                            Message msg = mHandler.obtainMessage(Constants.MESSAGE_TARGET);
                            Bundle bundle = new Bundle();
                            bundle.putFloat(Constants.TARGET_HEADING, targetHeading);
                            bundle.putFloat(Constants.TARGET_DISTANCE, targetDistance);
                            msg.setData(bundle);
                            mHandler.sendMessage(msg);
                        }
                        else if(mmBuffer[numBytes-1] == 0x46) // 'F'
                        {
                            boolean isFix = false;
                            Log.d(TAG, "GPS Fix frame recognized");
                            mmInStream.read(gpsFixBytes, 0, 2);
                            if(gpsFixBytes[0] == 0x1)
                            {
                                isFix = true;
                            }
                            else if(gpsFixBytes[0] == 0x0)
                            {
                                isFix = false;
                            }
                            Message msg = mHandler.obtainMessage(Constants.MESSAGE_FIX);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(Constants.GPS_FIX, isFix);
                            msg.setData(bundle);
                            mHandler.sendMessage(msg);
                        }

                        }
                }catch (IOException e)
                {
                    Log.d(TAG, "Input stream closed");
                    connectionLost();
                    break;
                }
            }
        }

        //write to outstream
        public void write(byte[] buffer)
        {
            try
            {
                mmOutStream.write(buffer);

                //share the sent message back to UI fragment here if necessary
            }catch (IOException e)
            {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel()
        {
            try
            {
                mmSocket.close();
            }catch (IOException e)
            {
                Log.e(TAG, "close() of connected socket failed", e);
            }
        }

    }

}
