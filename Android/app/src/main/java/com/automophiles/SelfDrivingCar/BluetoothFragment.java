package com.automophiles.SelfDrivingCar;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Formatter;
import java.util.Set;


public class BluetoothFragment extends Fragment {

    private static final String TAG = "BluetoothFragment";
    private Button mButton;

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int MY_PERMISSIONS_REQUEST_BLUETOOTH_ADMIN = 2;

    private static int mButtonState;

    //button states
    private static final int CONNECTING = 0;
    private static final int SELECT_DESTINATION = 1;
    private static final int SEND_DESTINATION = 3;
    private static final int START = 4;
    private static final int STOP = 5;
    private String mConnectedDeviceName = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothService mBluetoothService = null;
    private LatLng mDestination = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.bluetooth_fragment, container, false);

        mButton = v.findViewById(R.id.bluetooth_command_btn);
        mButton.setClickable(false);

        bluetoothInit();


        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(mButtonState)
                {
                    case Constants.BLUETOOTH_BTN_SEND_DESTINATION:
                        //send L -> LAT (4 bytes) -> LNG (4 bytes)
                        byte[] latBytes = new byte[4];
                        byte[] lngBytes = new byte[4];
                        ByteBuffer.wrap(latBytes).putFloat((float)mDestination.latitude);
                        ByteBuffer.wrap(lngBytes).putFloat((float)mDestination.longitude);
                        byte[] bytesToSend = new byte[latBytes.length + lngBytes.length + 1];
                        bytesToSend[0] = 0x4C;
                        System.arraycopy(latBytes, 0, bytesToSend, 1, latBytes.length);
                        System.arraycopy(lngBytes, 0, bytesToSend, latBytes.length + 1, lngBytes.length);
                        Formatter formatter = new Formatter();
                        for(byte b : bytesToSend)
                        {
                            formatter.format("%02x", b);
                        }
                        Log.d(TAG, "byte array to be sent: " + formatter.toString());
                        sendBytes(bytesToSend);
                        updateButton(Constants.BLUETOOTH_BTN_START);
                        break;
                    case Constants.BLUETOOTH_BTN_START:
                        byte[] go_msg = {0x61}; //'a'
                        sendBytes(go_msg);
                        updateButton(Constants.BLUETOOTH_BTN_STOP);
                        break;
                    case Constants.BLUETOOTH_BTN_STOP:
                        byte[] stop_msg = {0x62}; //'b'
                        sendBytes(stop_msg);
                        updateButton(Constants.BLUETOOTH_BTN_START);
                        break;
                    default:
                        Log.e(TAG, "setOnClickListener dun fucked up, mButtonState is:" + mButtonState);
                        break;
                }


            }
        });

        return v;
    }

    public void bluetoothInit()
    {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothService = new BluetoothService(getActivity(), mHandler);

        if(mBluetoothAdapter == null)
        {
            Log.d(TAG, "Bluetooth not supported");
            //Device doesn't support Bluetooth

        }
        if(!mBluetoothAdapter.isEnabled())
        {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        //bluetooth permission should be granted at this point
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if(pairedDevices.size() > 0) {
            //There are paired devices. Get the name and address of each paired device
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress();
                Log.d(TAG, "Paired deviceName:" + deviceName + " deviceHardwareAddress:" + deviceHardwareAddress);
                if(deviceHardwareAddress.equals(Constants.MY_XBEE_ADDRESS))
                {
                    Log.d(TAG,"hardware address match");
                    connectDevice(device);
                }

            }
        }
        else
        {
            mBluetoothAdapter.startDiscovery();
            // Register for broadcasts when a device is discovered.
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            getActivity().registerReceiver(receiver, filter);
        }
    }

    private void sendBytes(byte[] msg)
    {
        Log.d(TAG, "sendBytes" + msg[0]);
        if(mBluetoothService.getState() != BluetoothService.STATE_CONNECTED)
        {
            Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        if(msg.length > 0)
        {
            mBluetoothService.write(msg);
        }
    }

    //handler that gets info back from BluetoothService
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            FragmentActivity activity = getActivity();
            switch(msg.what)
            {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch(msg.arg1)
                    {
                        case BluetoothService.STATE_CONNECTING:
                            updateButton(Constants.BLUETOOTH_BTN_CONNECTING);
                            mButtonState = Constants.BLUETOOTH_BTN_CONNECTING;
                            ((MainActivity)getActivity()).updateToolBarStatus(Constants.CONNECTING);
                            break;
                        case BluetoothService.STATE_CONNECTED:
                            if(mButtonState != Constants.BLUETOOTH_BTN_SEND_DESTINATION)
                            {
                                updateButton(Constants.BLUETOOTH_BTN_SELECT_DEST);
                                mButtonState = Constants.BLUETOOTH_BTN_SELECT_DEST;
                                ((MainActivity)getActivity()).updateToolBarStatus(Constants.CONNECTED);

                            }
                            break;
                        case BluetoothService.STATE_NONE:
                            //what to set button when bluetooth service is not connected?
                            ((MainActivity)getActivity()).resetDebugValues();
                            ((MainActivity)getActivity()).updateToolBarStatus(Constants.NOT_CONNECTED);
                            break;
                    }
                    break;
                case Constants.MESSAGE_HEADING:
                    ((MainActivity)getActivity()).updateToolBarHeading(msg.getData().getFloat(Constants.HEADING));
                    break;
                case Constants.MESSAGE_LATLNG:
                    ((MainActivity)getActivity()).updateCurrentLocation(msg.getData().getFloat(Constants.LAT),msg.getData().getFloat(Constants.LNG));
                    break;
                case Constants.MESSAGE_SENSORS:
                    ((MainActivity)getActivity()).updateDebugSensorValues(msg.getData().getInt(Constants.LEFT_SENSOR), msg.getData().getInt(Constants.FRONT_SENSOR), msg.getData().getInt(Constants.RIGHT_SENSOR));
                    break;
                case Constants.MESSAGE_RPM:
                    ((MainActivity)getActivity()).updateDebugRPMValues(msg.getData().getInt(Constants.RPM_VALUE));
                    break;
                case Constants.MESSAGE_TARGET:
                    ((MainActivity)getActivity()).updateDebugTargetHeadingDistance(msg.getData().getFloat(Constants.TARGET_HEADING), msg.getData().getFloat(Constants.TARGET_DISTANCE));
                    break;
                case Constants.MESSAGE_FIX:
                    ((MainActivity)getActivity()).updateDebugGPSFix(msg.getData().getBoolean(Constants.GPS_FIX));
                    break;
                case Constants.MESSAGE_GPS_CHKPT:
                    ((MainActivity)getActivity()).updateCheckpoints(msg.getData().getIntArray(Constants.GPS_CHKPT_ARRAY), msg.getData().getInt(Constants.GPS_NUM_CHECKPTS));
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    if(activity != null)
                    {
                        Toast.makeText(activity, "Connected to " + mConnectedDeviceName,Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    if(activity != null)
                    {
                        if(mBluetoothService.getState() == BluetoothService.STATE_CONNECTED)
                        {
                            byte[] bytes = msg.getData().getByteArray(Constants.TOAST);
                            try
                            {
                                String result = new String(bytes, "ASCII");
                                Toast.makeText(activity, result,Toast.LENGTH_SHORT).show();
                            } catch(UnsupportedEncodingException e)
                            {
                                Log.d(TAG, "Unsupported encoding");
                            }
                        }

                    }
                    break;
            }
        }
    };
    //make this universal and pass an argument? Possible states of button: "Connecting..." -> "Select Destination" -> "Send Checkpoints" -> "Go" -> "Stop"(kill switch) -> back to select destination?
    public void updateButton(final int state)
    {
        switch(state)
        {
            case CONNECTING:
                mButton.setText(R.string.bluetooth_btn_default);
                break;
            case SELECT_DESTINATION:
                mButton.setText(R.string.bluetooth_btn_select);
                mButtonState = Constants.BLUETOOTH_BTN_SELECT_DEST;
                mButton.setClickable(false);
                break;
            case SEND_DESTINATION:
                mButton.setText(R.string.bluetooth_btn_send);
                mButton.setClickable(true);
                mButtonState = Constants.BLUETOOTH_BTN_SEND_DESTINATION;
                break;
            case START:
                mButton.setText(R.string.bluetooth_btn_start);
                mButtonState = Constants.BLUETOOTH_BTN_START;
                break;
            case STOP:
                mButton.setText(R.string.bluetooth_btn_stop);
                mButtonState = Constants.BLUETOOTH_BTN_STOP;
                break;
            default:
                break;

        }
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceHardwareAddress = device.getAddress(); // MAC address
                if(deviceHardwareAddress.equals(Constants.MY_XBEE_ADDRESS))
                {
                    Log.d(TAG,"hardware address match");
                    connectDevice(device);
                }
            }
        }
    };

    public void updateBTDestination(LatLng dest)
    {
        mDestination = dest;
    }

    public void sendHeadlightToggle(boolean isOn)
    {
        Log.d(TAG, "sendHeadlightToggle");
        if(isOn)
        {
            byte[] headlight_msg = {0x68};
            sendBytes(headlight_msg);
        }
        else
        {
            byte[] headlight_msg = {0x69};
            sendBytes(headlight_msg);
        }
    }

    private void connectDevice(BluetoothDevice device)
    {
        mBluetoothService.connect(device);
    }

    public void onDestroy()
    {
        super.onDestroy();

        //unregister the ACTION_FOUND receiver
        getActivity().unregisterReceiver(receiver);
        if(mBluetoothService != null)
        {
            mBluetoothService.stop();
        }
    }
}
