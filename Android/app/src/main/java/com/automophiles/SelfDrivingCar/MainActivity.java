package com.automophiles.SelfDrivingCar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

import java.math.BigDecimal;

import static com.automophiles.SelfDrivingCar.Constants.BLUETOOTH_BTN_SEND_DESTINATION;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback{

    private final static String TAG = "MainActivity";
    private Toolbar mToolbar = null;
    private GoogleMap mMap = null;
    private TextView mHeading = null;
    private TextView mToolBarConnectionStatus = null;
    private TextView mDebugCoordsText = null, mDebugLeftSensorText = null, mDebugFrontSensorText = null, mDebugRightSensorText = null, mDebugRPMText = null,
        mDebugDestCoordsText = null, mDebugTargetHeadingText = null, mDebugTargetDistanceText = null, mDebugGPSFixTest = null;
    private float mLat = 0, mLng = 0;
    PopupWindow mPopupWindow = null;
    LinearLayout mPopUpLayout = null;
    private Button mDebugButton = null, mCloseDebugButton = null, mHeadlightToggleButton = null;
    private int mLeftSensorValue = 0, mFrontSensorValue = 0, mRightSensorValue = 0, mRPMValue;
    private float mTargetHeading = 0, mTargetDistance = 0;
    private boolean mGPSFix = false;
    private boolean isHeadlightOn = false;
    private float mCheckpointLat = 0, mCheckpointLng = 0;
    // southwest corner, northeast corner
    private LatLngBounds mSJSU = new LatLngBounds(new LatLng(37.331, -121.883), new LatLng(37.338, -121.879));
    private Marker mCurCarLocation = null;
    private Marker[] mRouteCheckpoints = null;
    private LatLng[] mRouteCheckpointLatLng = null;
    private LatLng mCurCarLocationLatLng = null;
    private Marker mDestination = null;
    private FragmentManager fm = getSupportFragmentManager();
    private boolean isMapReady = false;
    private Polyline mCarToCheckpoint = null;
    private LatLng[][] routes; //2d array of "routes" each consisting of LatLng points

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Not Connected");
        mDebugButton = findViewById(R.id.debug_button);
        mPopUpLayout = findViewById(R.id.mainActivity);

        clearCheckpoints();

        //Bluetooth fragment logic
        Fragment bluetoothFragment = fm.findFragmentById(R.id.bluetoothCommandFragment);
        if(bluetoothFragment == null) {
            bluetoothFragment = new BluetoothFragment();
            fm.beginTransaction()
                    .add(R.id.bluetoothCommandFragment, bluetoothFragment)
                    .commit();

        }

        mToolBarConnectionStatus = findViewById(R.id.toolbarConnectionStatus);
        mHeading = findViewById(R.id.headingText);

        mDebugButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mPopupWindow == null)
                {
                    LayoutInflater layoutInflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View customView = layoutInflater.inflate(R.layout.debug_popup, null);
                    mHeadlightToggleButton = customView.findViewById(R.id.headlightToggleButton);
                    mCloseDebugButton = customView.findViewById(R.id.debugPopupCloseButton);
                    mDebugCoordsText = customView.findViewById(R.id.debugPopupCoords);
                    mDebugDestCoordsText = customView.findViewById(R.id.debugDestCoords);
                    mDebugTargetHeadingText = customView.findViewById(R.id.debugTargetHeading);
                    mDebugTargetDistanceText = customView.findViewById(R.id.debugTargetDistance);
                    mDebugLeftSensorText = customView.findViewById(R.id.debugPopupLeftSensorText);
                    mDebugFrontSensorText = customView.findViewById(R.id.debugPopupFrontSensorText);
                    mDebugRightSensorText = customView.findViewById(R.id.debugPopupRightSensorText);
                    mDebugRPMText = customView.findViewById(R.id.debugPopupRPM);
                    mDebugGPSFixTest = customView.findViewById(R.id.debugGPSFix);

                    mDebugCoordsText.setText("Car Coords: " + mLat + "\u00B0 N, " + mLng + "\u00B0 W");
                    if(mDestination != null)
                    {
                        mDebugDestCoordsText.setText("Dest Coords: " + (float)mDestination.getPosition().latitude + "\u00B0 N, " + (float)mDestination.getPosition().longitude + "\u00B0 W");

                    }
                    if(mGPSFix)
                    {
                        mDebugGPSFixTest.setText("GPS Fix: Yes" );
                    }
                    else
                    {
                        mDebugGPSFixTest.setText("GPS Fix: No");
                    }
                    if(mTargetHeading >= 0 && mTargetHeading <= 360)
                    {
                        mDebugTargetHeadingText.setText("Target Heading: " + mTargetHeading + "\u00B0");
                    }
                    if(mTargetDistance >= 0)
                    {
                        mDebugTargetDistanceText.setText("Target Distance: " + mTargetDistance + " m");
                    }
                    if(mLeftSensorValue >= 0 && mLeftSensorValue <= 8192)
                    {
                        mDebugLeftSensorText.setText("L: " + mLeftSensorValue + " mm");
                    }
                    if(mFrontSensorValue >= 0 && mFrontSensorValue <= 8192)
                    {
                        mDebugFrontSensorText.setText("F: " + mFrontSensorValue + " mm");
                    }
                    if(mRightSensorValue >= 0 && mRightSensorValue <= 8192)
                    {
                        mDebugRightSensorText.setText("R: " + mRightSensorValue + " mm");
                    }
                    mDebugRPMText.setText("Speed: " + mRPMValue + " rpm");

                    mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    mPopupWindow.showAtLocation(mPopUpLayout, Gravity.CENTER, 0, 0);

                    mCloseDebugButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d(TAG, "Close debug button clicked");
                            mPopupWindow.dismiss();
                            mPopupWindow=null;
                        }
                    });

                    mHeadlightToggleButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d(TAG, "headlight toggle button clicked");
                            BluetoothFragment bluetoothFragment = (BluetoothFragment) fm.findFragmentById(R.id.bluetoothCommandFragment);
                            isHeadlightOn = !isHeadlightOn; //toggle this value
                            bluetoothFragment.sendHeadlightToggle(isHeadlightOn);
                        }
                    });
                }

            }
        });




    }

    public void updateToolBarStatus(int state)
    {
        switch(state)
        {
            case Constants.NOT_CONNECTED:
                Log.d(TAG, "Toolbar title set to Not Connected");
                //mToolbar.setTitle("Not Connected");
                mToolBarConnectionStatus.setText("Not Connected");
                break;
            case Constants.CONNECTING:
                Log.d(TAG, "Toolbar title set to Connecting");
                //mToolbar.setTitle("Connecting");
                mToolBarConnectionStatus.setText("Connecting");
                break;
            case Constants.CONNECTED:
                Log.d(TAG, "Toolbar title set to Connected");
                //mToolbar.setTitle("Connected");
                mToolBarConnectionStatus.setText("Connected");
                break;
            default:
                break;
        }
    }

    public void updateToolBarHeading(float curHeading)
    {
        BigDecimal bd = new BigDecimal(Float.toString(curHeading));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        curHeading = bd.floatValue();
        Log.d(TAG, "updateToolBarHeading " + curHeading);
        if(curHeading != 0.0)
        {
            mHeading.setText(curHeading + "\u00B0");
        }
    }

    public void updateDebugSensorValues(int left, int front, int right)
    {
        Log.d(TAG, "updateDebugSensorValues L: " + left + " F: " + front + " R: " + right);
        //input filtering here?
        mLeftSensorValue = left;
        mFrontSensorValue = front;
        mRightSensorValue = right;
        if(mDebugLeftSensorText != null)
        {
            if(mLeftSensorValue >= 0 && mLeftSensorValue <= 8192)
            {
                mDebugLeftSensorText.setText("L: " + mLeftSensorValue + " mm");
            }
        }
        if(mDebugFrontSensorText != null)
        {
            if(mFrontSensorValue >= 0 && mFrontSensorValue <= 8192)
            {
                mDebugFrontSensorText.setText("F: " + mFrontSensorValue + " mm");
            }
        }

        if(mDebugRightSensorText != null)
        {
            if(mRightSensorValue >= 0 && mRightSensorValue <= 8192)
            {
                mDebugRightSensorText.setText("R: " + mRightSensorValue + " mm");
            }
        }

    }

    public void updateDebugRPMValues(int rpm)
    {
        Log.d(TAG, "updateDebugRPMValues rpm: " + rpm);
        mRPMValue = rpm;
        if(mDebugRPMText != null)
        {
            mDebugRPMText.setText("Speed: " + mRPMValue + " rpm");
        }
    }

    public void updateDebugTargetHeadingDistance(float heading, float distance)
    {
        Log.d(TAG, "updateDebugTargetHeadingDistance heading: " + heading + " distance " + distance);
        BigDecimal bd = new BigDecimal(Float.toString(heading));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        heading = bd.floatValue();
        bd = new BigDecimal(Float.toString(distance));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        distance = bd.floatValue();
        mTargetHeading = heading;
        mTargetDistance = distance;
        if(mDebugTargetHeadingText != null)
        {
            if(mTargetHeading > 0.0 && mTargetHeading <= 360.0)
            {
                mDebugTargetHeadingText.setText("Target Heading: " + mTargetHeading + "\u00B0");
            }
        }
        if(mDebugTargetDistanceText != null)
        {
            if(mTargetDistance > 0.0)
            {
                mDebugTargetDistanceText.setText("Target Distance: " + mTargetDistance + " m");

            }
        }
    }

    public void updateDebugGPSFix(boolean isFix)
    {
        Log.d(TAG, "updateDebugGPSFix " + isFix);
        mGPSFix = isFix;
        if(mDebugGPSFixTest != null)
        {
            if(mGPSFix)
            {
                mDebugGPSFixTest.setText("GPS Fix: Yes" );
            }
            else
            {
                mDebugGPSFixTest.setText("GPS Fix: No" );
            }
        }
    }

    private void clearCheckpoints()
    {
        mRouteCheckpoints = null;
        mRouteCheckpointLatLng = null;
    }

    public void updateCheckpoints(int[] checkpoints, int numCheckpoints)
    {
        Log.d(TAG, "updateCheckpoints numCheckpoints " + numCheckpoints);

        for(int i = 0; i < numCheckpoints; i++)
        {
            mRouteCheckpointLatLng[i] = new LatLng(Constants.gps_checkpoints[checkpoints[i]][0], Constants.gps_checkpoints[checkpoints[i]][1]);
            if(mRouteCheckpoints[i] != null)
            {
                mRouteCheckpoints[i].remove();
            }
            mRouteCheckpoints[i] = mMap.addMarker(new MarkerOptions()
            .position(mRouteCheckpointLatLng[i])
            .title("Checkpoint")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        }
        /*
        mNextCheckpointLatLng = new LatLng(mCheckpointLat, mCheckpointLng);
        if(mNextCheckpoint != null)
        {
            mNextCheckpoint.remove();
        }
                    mCurCarLocation = mMap.addMarker(new MarkerOptions()
            .position(mCurCarLocationLatLng)
            .title("Car")
            .icon(BitmapDescriptorFactory.fromBitmap(carIcon)));
        mNextCheckpoint = mMap.addMarker(new MarkerOptions()
        .position(mNextCheckpointLatLng)
        .title("Next checkpoint")
        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        */

    }

    public void updateCurrentLocation(double latitude, double longitude)
    {
        Log.d(TAG, "updateCurrentLocation mLat: " + latitude + " mLng: " + longitude);
        mCurCarLocationLatLng = new LatLng(latitude, longitude);
        mLat = (float)latitude;
        mLng = (float)longitude;

        if(mDebugCoordsText != null)
        {
            mDebugCoordsText.setText("Coords: " + mLat + "\u00B0 N, " + mLng + "\u00B0 W");
        }

        if(isMapReady)
        {
            //TODO: add a custom marker for the car
            if(mCurCarLocation != null)
            {
                mCurCarLocation.remove();
            }
            if(mCarToCheckpoint != null)
            {
                mCarToCheckpoint.remove();
            }
            Bitmap carIcon = BitmapFactory.decodeResource(getResources(), R.drawable.baseline_directions_car_black_36dp);
            mCurCarLocation = mMap.addMarker(new MarkerOptions()
            .position(mCurCarLocationLatLng)
            .title("Car")
            .icon(BitmapDescriptorFactory.fromBitmap(carIcon)));
/*
        if(mNextCheckpointLatLng != null )
        {
            mCarToCheckpoint = mMap.addPolyline(new PolylineOptions()
                    .clickable(true)
                    .add(mCurCarLocationLatLng, mNextCheckpointLatLng)
                    .width(15)
                    .color(Color.RED));
        }
        */

        }
        else
        {
            Log.d(TAG, "ERROR: updateCurrentLocation called when onMapReady not finished");
        }
    }

    private void updateDebugDestCoords(double latitude, double longitude)
    {
        Log.d(TAG, "updateDebugDestCoords: Lat:" + latitude + " Lng: " + longitude);
        if(mDebugDestCoordsText != null)
        {
            mDebugDestCoordsText.setText("Dest Coords: " + (float)latitude + "\u00B0 N, " + (float)longitude + "\u00B0 W");
        }
    }

    public void resetDebugValues()
    {
        mLeftSensorValue = 0;
        mRightSensorValue = 0;
        mFrontSensorValue = 0;
        mTargetDistance = 0;
        mTargetHeading = 0;
        mLat = 0;
        mLng = 0;
        mRPMValue = 0;
        updateCurrentLocation(mLat, mLng);
        updateDebugDestCoords(mLat, mLng);
        updateDebugSensorValues(mLeftSensorValue, mFrontSensorValue, mRightSensorValue);
        updateDebugTargetHeadingDistance(mTargetHeading, mTargetDistance);
        updateDebugRPMValues(mRPMValue);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //TODO: Mess with these zoom restrictions until it looks nice
        mMap.setMinZoomPreference(18.0f);
        mMap.setMaxZoomPreference(20.0f);
       // mMap.setMapType(mMap.MAP_TYPE_SATELLITE);

        //build a cameraPosition
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(37.337166, -121.881715))      // Sets the center of map to SJSU campus
                .zoom(17)                       // Sets zoom to 16
                .bearing(330)                   // Sets direction of camera
                .tilt(0)                        // no tilt
                .build();                       // build cameraPosition object from these parameters

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition((cameraPosition)));
        //mMap.setLatLngBoundsForCameraTarget(mSJSU);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        isMapReady = true;


        //TODO: On user click, create a marker at location, capture the coordinates of that marker; disable this if bluetooth not connected
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                    //clear all markers
                    mMap.clear();
                    //add new marker at map click, need to store mLat long
                    mDestination = mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title("Destination")
                    );
                    //need to communicate to bluetoothFragment that a destination has been picked, the fragment should update button
                    BluetoothFragment bluetoothFragment = (BluetoothFragment) fm.findFragmentById(R.id.bluetoothCommandFragment);
                    bluetoothFragment.updateButton(BLUETOOTH_BTN_SEND_DESTINATION);
                    bluetoothFragment.updateBTDestination(latLng);
                    updateDebugDestCoords(latLng.latitude, latLng.longitude);
            }
        });
    }
}