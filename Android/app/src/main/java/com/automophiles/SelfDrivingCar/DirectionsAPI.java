package com.automophiles.SelfDrivingCar;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

//Helper class to handle interactions with Directions API in MainActivity
public class DirectionsAPI {

    private static final String TAG = "DirectionsAPI";
    private final String prefixHTTPString = "https://maps.googleapis.com/maps/api/directions/json?";
    private final String suffixHTTPString = "&mode=walking&key=";
    private URL mURL;
    private InputStream mStream;
    private HttpsURLConnection mConnection;

    //constructor
    public DirectionsAPI(){
        mURL = null;
        mStream = null;
        mConnection = null;
    }

    public void formatQueryString(LatLng origin, LatLng dest)
    {
        String queryString = (prefixHTTPString +  "origin=" + Double.toString(origin.latitude) + "," + Double.toString(origin.longitude)
                + "&destination=" + Double.toString(dest.latitude) + "," + Double.toString(dest.longitude)
                + suffixHTTPString);
        try
        {
            mURL = new URL(queryString);
        } catch(MalformedURLException e)
        {
            Log.e(TAG, "formatQueryString");
        }
    }

    //returns a JSON array that needs to be parsed
    //TODO: Look into how Volley library works to handle http requests
    public JSONArray sendDirectionsAPIQuery() throws IOException
    {
        JSONArray result = null;
        try
        {
            mConnection = (HttpsURLConnection) mURL.openConnection();
            mConnection.setReadTimeout(3000);
            mConnection.setConnectTimeout(3000);
            mConnection.setRequestMethod("GET");
            mConnection.setDoInput(true);
            mConnection.connect();
            mStream = mConnection.getInputStream();
        } finally
        {
            if(mStream != null)
            {
                mStream.close();
            }
            if(mConnection != null)
            {
                mConnection.disconnect();
            }
        }

        return result;


    }
}
