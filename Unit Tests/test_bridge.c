#include "unity.h"
#include <stdbool.h>
#include "Mockcan.h"
#include "Mockc_uart3.h"
#include "Mockc_led.h"
#include "_can_dbc/generated_can.h"
#include "bridge.h"

#define BRIDGE

void test_bridge_init(void)
{
	uart3_init_ExpectAndReturn(9600, 10, 10, true);
	led_init_ExpectAndReturn(true);
	CAN_init_ExpectAndReturn(can1, 100, 500, 500, NULL, NULL, true);
	CAN_bypass_filter_accept_all_msgs_Expect();
	CAN_reset_bus_Expect(can1);
	TEST_ASSERT_TRUE(bridge_init() == true);
}

void test_bridge_1Hz(void)
{
	CAN_is_bus_off_ExpectAndReturn(can1, true);
	CAN_reset_bus_Expect(can1);
	bridge_1Hz();
}


void test_bridge_100Hz(void)
{
	COMPASS_CMD_t compass_cmd_msg = {0};
	uart3_getchar_ExpectAndReturn(NULL, 0, true);
	uart3_getchar_IgnoreArg_byte();
	can_msg_t can_msg;
	can_msg.frame_fields.data_len = 4;
	can_msg.msg_id = 110;
	dbc_msg_hdr_t can_msg_hdr;
	can_msg_hdr.dlc = can_msg.frame_fields.data_len;
	can_msg_hdr.mid = can_msg.msg_id;
	CAN_rx_ExpectAndReturn(can1, NULL, 0, true);
	CAN_rx_IgnoreArg_msg();
	//led_set_Expect(1, false);
	dbc_decode_COMPASS_CMD(&compass_cmd_msg, can_msg.data.bytes, &can_msg_hdr);
	CAN_rx_ExpectAndReturn(can1, NULL, 0, false);
	CAN_rx_IgnoreArg_msg();
	//led_set_Expect(2, true);
	bridge_100Hz();
	
	
	
}
