#include "unity.h"
#include <stdint.h>
#include <stdbool.h>
#include "c_period_callbacks.h"
#include "Mockcan.h"
#include "Mockc_motor_class.h"
#include "Mockc_motor_driver.h"
#include "Mockc_motor_rpm.h"
#include "Mockc_calibrate.h"
#include "Mockc_can_comm.h"
#include "Mocklcd.h"





void test_C_period_init(void) {
	c_motor_init_Expect();
	c_rpm_init_Expect();
	CAN_Init_Expect();
	lcd_uart_init_ExpectAndReturn(1);
    lcd_init_ExpectAndReturn(1);
	TEST_ASSERT_TRUE(C_period_init());
}


void test_C_period_1Hz(void){
	c_calibrate_sw_ExpectAndReturn(0,true);
	CAN_is_bus_off_ExpectAndReturn(can1,true);
	CAN_reset_bus_Expect(can1);
	C_period_1Hz(0);
	
	c_calibrate_sw_ExpectAndReturn(0,false);
	CAN_is_bus_off_ExpectAndReturn(can1,false);
	print_1Hz_tar_dist_Expect();
    print_1Hz_speed_Expect();
	C_period_1Hz(0);
	
	c_calibrate_sw_ExpectAndReturn(1,false);
	CAN_is_bus_off_ExpectAndReturn(can1,false);
	print_1Hz_tar_dist_Expect();
    print_1Hz_speed_Expect();
	C_period_1Hz(1);
}


void test_C_period_10Hz(void){
	get_rev_status_ExpectAndReturn(true);
	rev_execute_Expect(10);
	C_period_10Hz(10);
	
	get_rev_status_ExpectAndReturn(false);
	C_period_10Hz(14);
	
	get_rev_status_ExpectAndReturn(true);
	rev_execute_Expect(100);
	C_period_10Hz(100);
	
	
}	
	

void test_C_period_100hz(void){
	MOTOR_CMD_t motor_cmd;
	
	c_rps_run_100Hz_Expect(30);
	CAN_Recieve_100Hz_Expect();
	CAN_Send_100Hz_Expect();
	CAN_Receive_get_motor_command_ExpectAndReturn(motor_cmd) ;
	get_rev_status_ExpectAndReturn(false);
	drive_motor_Expect(motor_cmd);
	C_period_100Hz(30);
	
	c_rps_run_100Hz_Expect(60);
	CAN_Recieve_100Hz_Expect();
	CAN_Send_100Hz_Expect();
	CAN_Receive_get_motor_command_ExpectAndReturn(motor_cmd) ;
	get_rev_status_ExpectAndReturn(true);
	C_period_100Hz(60);
	
	c_rps_run_100Hz_Expect(24);
	CAN_Recieve_100Hz_Expect();
	CAN_Send_100Hz_Expect();
	CAN_Receive_get_motor_command_ExpectAndReturn(motor_cmd) ;
	get_rev_status_ExpectAndReturn(false);
	drive_motor_Expect(motor_cmd);
	get_rev_status_ExpectAndReturn(false);
	c_motor_execute_Expect();
	C_period_100Hz(24);
	
	
	c_rps_run_100Hz_Expect(49);
	CAN_Recieve_100Hz_Expect();
	CAN_Send_100Hz_Expect();
	CAN_Receive_get_motor_command_ExpectAndReturn(motor_cmd) ;
	get_rev_status_ExpectAndReturn(false);
	drive_motor_Expect(motor_cmd);
	get_rev_status_ExpectAndReturn(false);
	c_motor_execute_Expect();
	C_period_100Hz(49);
	
	
}