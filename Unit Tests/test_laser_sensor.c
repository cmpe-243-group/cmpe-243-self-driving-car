#include "unity.h" // Single Unity Test Framework include
#include "Mockcan.h"
#include "laser_sensor.h"
#include "Mockwrapper_laser.h"
#include "Mockwrapper_io.h"
#include "Mockprintf_lib.h"


Laser_addresses_S sen;
void setup(void){

    Laser_addresses_S *sensors_ptr = &sen;
    sensors_ptr->left = 0x44;
    sensors_ptr->center = 0x55;
    sensors_ptr->right = 0x66;

}

void tear_down(void){

}

void test_init_sensor_addresses(void){
    Laser_addresses_S sensors_ptr={0};
    Laser_addresses_S *ptr = &sensors_ptr;

    init_sensor_addresses();
    TEST_ASSERT_EQUAL(0,ptr->center);
    ptr->center = 2;
    TEST_ASSERT_EQUAL(2,ptr->center);
}

void test_reset_laser_xShut(void){
    reset_Xshut_Expect();
    reset_laser_xShut();
}

void test_init_i2c_device(void){
    i2c_init_ExpectAndReturn(200,true);
    init_i2c_device(200);
}

void test_set_laser_address(void){
    setLeftHigh_Expect();
    check_device_response_ExpectAndReturn(0x52,true);
    writeReg_laser_ExpectAndReturn(0x52,138,((0x44 >> 1) & 0x7F),true);
    //check_device_response_ExpectAndReturn(0x52,false);
    //TEST_ASSERT_TRUE(check_i2c_response(0x52));
    //TEST_ASSERT_TRUE(write_i2c_reg(0x52,138,((0x44 >> 1) & 0x7F)));

    setRightHigh_Expect();
    check_device_response_ExpectAndReturn(0x52,true);
    writeReg_laser_ExpectAndReturn(0x52,138,((0x66 >> 1) & 0x7F),true);

    setCenterHigh_Expect();
    check_device_response_ExpectAndReturn(0x52,true);
    writeReg_laser_ExpectAndReturn(0x52,138,((0x55 >> 1) & 0x7F),true);

    set_laser_address();
}

void test_init_sensors(void){
    i2c_init_sensors_ExpectAndReturn(0x55,true);
    i2c_init_sensors_ExpectAndReturn(0x44,true);
    i2c_init_sensors_ExpectAndReturn(0x66,true);
    init_sensors();
}

void test_take_readings(void){
    readRangeSingleMillimeters_ExpectAndReturn(0x66,100);
    take_readings(12);

    readRangeSingleMillimeters_ExpectAndReturn(0x44,100);
    take_readings(22);

    readRangeSingleMillimeters_ExpectAndReturn(0x55,100);
    take_readings(32);
}

void test_sensor_send_on_can(void){
    Laser_values_S laser;
    Laser_values_S *ptr = &laser;
    ptr->left_sensor_val = 100;
    TEST_ASSERT_EQUAL(get_sensor_val(1),100);

    ptr->center_sensor_val = 100;
    TEST_ASSERT_EQUAL(get_sensor_val(0),100);

    ptr->right_sensor_val = 100;
    TEST_ASSERT_EQUAL(get_sensor_val(3),100);
    //CAN_tx_IgnoreArg_can_msg
    //CAN_tx_ExpectAndReturn(can1,)
    sensor_send_on_can();
}
