#include "unity.h"
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include "Mockc_led_display.h"
#include "Mockc_switches.h"
#include "Mockc_i2c2.h"
#include "Mockc_led.h"
#include "Mockcan_lib.h"
#include "compass.h"


void test_compass_init(void)
{
	led_display_init_ExpectAndReturn(true);
	switches_init_ExpectAndReturn(true);
	i2c2_init_ExpectAndReturn(100, true);
	i2c2_write_reg_Expect(COMPASS_DEVICE_ADDR, CTRL5, 0x04);
	i2c2_write_reg_Expect(COMPASS_DEVICE_ADDR, CTRL7, 0x00);
	geo_can_tx_compass_init_debug_Expect();
	TEST_ASSERT_TRUE(compass_init(100));
	
	led_display_init_ExpectAndReturn(true);
	switches_init_ExpectAndReturn(true);
	i2c2_init_ExpectAndReturn(100, false);
	TEST_ASSERT_FALSE(compass_init(100));
}

void test_compass_get_heading(void)
{
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_X_H_M, 0xAA);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_X_L_M, 0xBB);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_Y_H_M, 0xCC);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_Y_L_M, 0xDD);	
	double val = compass_get_heading();
	TEST_ASSERT_TRUE(val > 0.0 && val < 360.0);	
}

void test_compass_100Hz(void)
{
	switches_get_ExpectAndReturn(1, true);
	led_set_Expect(4, true);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_X_H_M, 0xAA);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_X_L_M, 0xBB);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_Y_H_M, 0xCC);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_Y_L_M, 0xDD);	
		i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_X_H_M, 0xAA);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_X_L_M, 0xBB);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_Y_H_M, 0xCC);
	i2c2_read_reg_ExpectAndReturn(COMPASS_DEVICE_ADDR, OUT_Y_L_M, 0xDD);
	geo_can_tx_compass_man_cal_debug_Expect();
	led_display_set_Expect(0);
	led_display_set_IgnoreArg_num();
	geo_can_tx_heading_Expect(0);
	geo_can_tx_heading_IgnoreArg_heading();
	compass_100Hz();
}
