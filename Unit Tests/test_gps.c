#include "unity.h"
#include <stdbool.h>

#include "printf_lib.h"
#include "Mockcan.h"
#include "Mockc_uart2.h"
#include "Mockc_led.h"
#include "Mockcompass.h"
#include "Mockcan_lib.h"
#include "_can_dbc/generated_can.h"
#include "gps.h"

#define GPS
#define COMPASS

void test_gps_init(void)
{
	led_init_ExpectAndReturn(true);
	led_set_all_Expect(0x0);
	uart2_init_ExpectAndReturn(9600, 100, 100, true);
	
	uart2_putline_Expect(PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ, 0);
	uart2_putline_IgnoreArg_timeout();
	uart2_putline_Expect(PMTK_API_SET_FIX_CTL_200_MILLIHERTZ, 0);
	uart2_putline_IgnoreArg_timeout();
	uart2_putline_Expect(GPS_GPGGA_SENTENCES_ONLY, 0);
	uart2_putline_IgnoreArg_timeout();
	uart2_putline_Expect(GPS_SET_UPDATE_RATE_10Hz, 0);
	uart2_putline_IgnoreArg_timeout();
	uart2_putline_Expect(GPS_SET_BAUD_RATE_57600, 0);
	uart2_putline_IgnoreArg_timeout();
	geo_can_tx_gps_init_debug_Expect();
	uart2_init_ExpectAndReturn(57600, 100, 100, true);
	compass_init_ExpectAndReturn(100, true);
	TEST_ASSERT_TRUE(gps_init(100));
}

void test_gps_1Hz(void)
{
	geo_can_1Hz_Expect();
	gps_1Hz();
}

void test_gps_100Hz(void)
{
	//if gpsFix false
	char invalidBuffer[80] = "$GPRMC,,V,,,,,,,,,,N*53";
	uart2_gets_ExpectAndReturn(NULL, 80, 0, true);
	uart2_gets_IgnoreArg_pBuff();
	uart2_gets_ReturnThruPtr_pBuff(invalidBuffer);
	gps_parse(invalidBuffer);
	led_set_Expect(1, false);
	gps_100Hz();
	//if gpsFix true
	char validBuffer[80] = "$GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E*62";
	uart2_gets_ExpectAndReturn(NULL, 80, 0, true);
	uart2_gets_IgnoreArg_pBuff();
	uart2_gets_ReturnThruPtr_pBuff(validBuffer);
	gps_parse(validBuffer);
	//u0_dbg_printf_ExpectAndReturn("gps fix\n", true);
	led_set_Expect(1, true);
	gps_100Hz();
	

}