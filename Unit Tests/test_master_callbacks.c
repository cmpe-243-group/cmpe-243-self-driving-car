//master
#include "unity.h"
#include <stdbool.h>
#include "Mockcan.h"
#include "Mockmaster_functions.h"
#include "Mockwrapper_io.h"
#include "_can_dbc/generated_can.h"

void test_C_period_init(void){
	canInit_ExpectAndReturn(true);
	TEST_ASSERT_TRUE(C_period_init())
}

void tes_C_period_1Hz(void) {
    (void) count;
    resetCanIfOffExpect();
    C_period_1Hz(2);
}

void tes_C_period_100Hz(void) {
	SENSOR_STATUS_t sensor = {0};
    RPM_VALUE_CMD_t rpm = {0};
    MOTOR_CMD_t motor_cmd = {0};
    receive_rpm_val_ExpectAndReturn(NULL,true);
    receive_rpm_val_IgnoreArg_rpm_value_cmd_t();

    receive_sensor_ExpectAndReturn(NULL,true);
    receive_sensor_IgnoreArg_sensor_status_t();
    send_on_can_ExpectAndReturn(0x22,NULL);
    send_on_can_IgnoreArg_motor_cmd_t();

}

void test_process_received_data(void){
    TEST_ASSERT_EQUAL(steer_straight,get_steer_command(1300,1500,1400,1800)); // ALL FREE, STRAIGHT
    TEST_ASSERT_EQUAL(slight_right,get_steer_command(500,1500,1400,1800));
    TEST_ASSERT_EQUAL(slight_left,get_steer_command(1300,1500,700,1800));
    TEST_ASSERT_EQUAL(steer_left,get_steer_command(1300,1500,300,1800));
    TEST_ASSERT_EQUAL(steer_right,get_steer_command(400,1500,900,1800));
    TEST_ASSERT_EQUAL(brake,get_steer_command(1500,400,900,1800));
}
