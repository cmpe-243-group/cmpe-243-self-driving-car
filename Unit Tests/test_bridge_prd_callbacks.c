#include "unity.h"
#include "_can_dbc/generated_can.h"
#include "c_period_callbacks.h"
#include <string.h>

#include "Mockbridge.h"

#define BRIDGE

void test_c_period_init(void)
{
	bridge_init_ExpectAndReturn(true);
	C_period_init();
}

void test_c_period_1hz(void)
{
	bridge_1Hz_Expect();
	C_period_1Hz(0);
}

void test_c_period_100hz(void)
{
	bridge_100Hz_Expect();
	C_period_100Hz(0);
}