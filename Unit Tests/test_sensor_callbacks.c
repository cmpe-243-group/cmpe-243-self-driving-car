#include "unity.h" // Single Unity Test Framework include
#include "c_period_callbacks.h"
#include "Mocklaser_sensor.h"

void test_C_period_init(void){
    canInit_ExpectAndReturn(true);
    init_sensor_addresses_Expect();
    reset_laser_xShut_Expect();
    init_i2c_device_ExpectAndReturn(200,true);
    set_laser_address_ExpectAndReturn(true);
    init_sensors_Expect();
    C_period_init();

    canInit_ExpectAndReturn(false);
    init_sensor_addresses_Expect();
    reset_laser_xShut_Expect();
    init_i2c_device_ExpectAndReturn(200,true);
    set_laser_address_ExpectAndReturn(true);
    init_sensors_Expect();
    C_period_init();

}

void test_C_period_reg_tlm(void){
    C_period_reg_tlm();
}

void test_C_period_1Hz(void){
    resetCanIfOff_Expect();
    C_period_1Hz(1);
}

void test_C_period_100Hz(void){
    take_readings_Expect(2);
    sensor_send_on_can_Expect();
    C_period_100Hz(2);
}
