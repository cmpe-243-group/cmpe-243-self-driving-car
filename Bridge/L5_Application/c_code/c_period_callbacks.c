/**
 * @file

 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */



#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "shared.h"
#include "FreeRTOS.h"
//#include "printf_lib.h"
#ifdef BRIDGE
#include "bridge.h"
#endif

bool C_period_init(void) {
#ifdef BRIDGE
    return bridge_init();
#endif
}
bool C_period_reg_tlm(void) {
    return true;
}

//every 1000ms
void C_period_1Hz(uint32_t count) {
    (void) count;
#ifdef BRIDGE
    bridge_1Hz();
#endif

}

//every 100ms
void C_period_10Hz(uint32_t count) {
    (void) count;
    bridge_10Hz();
}

//every 10ms
void C_period_100Hz(uint32_t count) {
    (void) count;
#ifdef BRIDGE
    //changed to 25Hz to see if app received bytes are more stable
    /*
    if(count % 4 == 0)
    {
        bridge_25Hz();
    }
    */
#endif
}

//every 1ms
void C_period_1000Hz(uint32_t count) {
    (void) count;
}
