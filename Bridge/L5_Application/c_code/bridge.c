
#include "shared.h"
#include "bridge.h"

#ifdef BRIDGE
static RPM_VALUE_CMD_t rpm_value_msg = {0};
static COMPASS_CMD_t compass_cmd_msg = {0};
static GPS_CURRENT_LAT_LONG_t cur_latlng_msg = {0};
static SENSOR_READINGS_t cur_sensor_status_msg = {0};
static GPS_TARGET_HEADING_t cur_target_msg = {0};
static GPS_FIX_DEBUG_t gps_fix_msg = {0};

const uint32_t COMPASS_CMD__MIA_MS = 1000;
const COMPASS_CMD_t COMPASS_CMD__MIA_MSG = {0};

const uint32_t                             RPM_VALUE_CMD__MIA_MS = 1000;
const RPM_VALUE_CMD_t                      RPM_VALUE_CMD__MIA_MSG = {0};
const uint32_t                             SENSOR_READINGS__MIA_MS = 1000;
const SENSOR_READINGS_t                    SENSOR_READINGS__MIA_MSG = {0};
const uint32_t                             GPS_CURRENT_LAT_LONG__MIA_MS = 1000;
const GPS_CURRENT_LAT_LONG_t               GPS_CURRENT_LAT_LONG__MIA_MSG = {37.336069, -121.881826}; //center of SJSU
const uint32_t                             GPS_TARGET_HEADING__MIA_MS = 1000;
const GPS_TARGET_HEADING_t                 GPS_TARGET_HEADING__MIA_MSG = {0};
const uint32_t                             GPS_FIX_DEBUG__MIA_MS = 1000;
const GPS_FIX_DEBUG_t                      GPS_FIX_DEBUG__MIA_MSG = {0};
const uint32_t                             GPS_CHECKPOINT_INDEX__MIA_MS = 1000;
const GPS_CHECKPOINT_INDEX_t               GPS_CHECKPOINT_INDEX__MIA_MSG = {0};

float destLat = 0;
float destLng = 0;

bool bridge_init()
{
    //init led display
    led_display_init();
    //init uart2 for hc-06 module
    uart3_init(UART_INIT_BAUDRATE, UART_RX_QUEUE_SIZE, UART_TX_QUEUE_SIZE);
    //init onboard leds for mia
    led_init();
    //headlight_init();
    //headlight_set(true);
    //Transmitter: init CAN1 with 100k baud rate, 25 Rx Queue entries, 25 Tx Queue entries
    if(CAN_init(can1, CAN_BAUDRATE, CAN_RX_QUEUE_SIZE, CAN_TX_QUEUE_SIZE, NULL, NULL))
    {
        //for both CAN controllers, set up filter to accept all messages
        CAN_bypass_filter_accept_all_msgs();
        //Make CAN1 active on CAN bus by sending reset
        CAN_reset_bus(can1);
        bridge_CAN_tx_init_debug();
        return true;
    }
    return false;
}

void bridge_1Hz(void)
{
    if(CAN_is_bus_off(can1))
    {
        CAN_reset_bus(can1);
    }
    bridge_CAN_tx_heartbeat();
}


void swap_endian(char byte_array[], char result[])
{
    result[0] = byte_array[3];
    result[1] = byte_array[2];
    result[2] = byte_array[1];
    result[3] = byte_array[0];
}


void bridge_10Hz(void)
{
    char byte;
    char latBytes[4], lngBytes[4];
    char swappedLatBytes[4], swappedLngBytes[4];
    uint8_t j = 0;
       if(uart3_getchar(&byte, 0))
       {
           switch(byte)
           {
               case 'L':
                   for(; j < 4; j++)
                   {
                       uart3_getchar(latBytes + j, 0);
                       //u0_dbg_printf("%x", latBytes[j]);
                   }
                   u0_dbg_printf("\n");
                   for(j = 0; j < 4; j++)
                   {
                       uart3_getchar(lngBytes + j, 0);
                       //u0_dbg_printf("%x", lngBytes[j]);
                   }
                   swap_endian(latBytes, swappedLatBytes);
                   swap_endian(lngBytes, swappedLngBytes);
                   memcpy(bytes_to_float.bytes, swappedLatBytes, sizeof(bytes_to_float.bytes));
                   destLat = bytes_to_float.a;
                   //u0_dbg_printf("Dest Lat: %f\n", destLat);
                   memcpy(bytes_to_float.bytes, swappedLngBytes, sizeof(bytes_to_float.bytes));
                   destLng = bytes_to_float.a;
                   //u0_dbg_printf("Dest Lng: %f\n", destLng);
                   //u0_dbg_printf("\n");
                   bridge_CAN_tx_dest();
                   break;
               case 'a':
                   bridge_go();
                   led_set(1, true);
                   break;
               case 'b':
                   bridge_stop();
                   led_set(1, false);
                   break;
               case 'h': //68 headlight on
                   led_set(4, true);
                   //headlight_set(true);
                   bridge_CAN_tx_headlight(true);
                   break;
               case 'i': //69 headlight off
                   led_set(4, false);
                   //headlight_set(false);
                   bridge_CAN_tx_headlight(false);
                   break;
               default:
                   //printf("Byte: %#x\n", byte);
                   break;
           }
       }
    //receive heading from gps
    can_msg_t can_msg;
    while(CAN_rx(can1, &can_msg, 0))
    {
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;

        if(can_msg.msg_id == HEADING_CAN_MID)
        {
            //led_set(2, false);
            dbc_decode_COMPASS_CMD(&compass_cmd_msg, can_msg.data.bytes, &can_msg_hdr);
            //led_display_set(compass_cmd_msg.HEADING/10);
            float_to_bytes.a = compass_cmd_msg.HEADING;

            /*
             * TRANSMIT TO PHONE: HEADING
             */
            uart3_putchar(0x48, 0); //send 'H' start of heading frame
            //led_set(3, true);
            for(int i = 0; i < 4; i++)
            {
                uart3_putchar(float_to_bytes.bytes[i], 0);
            }
        }
        else if(can_msg.msg_id == RPM_CAN_MID)
        {
            dbc_decode_RPM_VALUE_CMD(&rpm_value_msg, can_msg.data.bytes, &can_msg_hdr);
            uart3_putchar(0x52, 0); //send 'R' start of RPM frame
            uart3_putchar(rpm_value_msg.RPM_VALUE, 0); //only 1 byte
        }
        else if(can_msg.msg_id == GPS_FIX_MID)
        {
            dbc_decode_GPS_FIX_DEBUG(&gps_fix_msg, can_msg.data.bytes, &can_msg_hdr);
            uart3_putchar(0x46, 0); //send 'F' start of Fix frame
            (gps_fix_msg.GPS_FIX_DEBUG) ? uart3_putchar(0x1, 0) : uart3_putchar(0x0, 0);
            uart3_putchar(0x0, 0); //send a dummy byte so android app can still read into byte array

        }
        else if(can_msg.msg_id == LATLNG_CAN_MID)
        {
            led_set(2, false);
            dbc_decode_GPS_CURRENT_LAT_LONG(&cur_latlng_msg, can_msg.data.bytes, &can_msg_hdr);
            //u0_dbg_printf("curLat: %f, curLng: %f\n", cur_latlng_msg.CUR_LAT, cur_latlng_msg.CUR_LONG);
            //led_display_set(fabs(cur_latlng_msg.CUR_LONG/10));
            //Put transmission of car latlng and sensor values outside of CAN_rx loop so they also send MIA values as well
            /**
             * TRANSMIT TO PHONE: GPS LATLNG
             */
            float_to_bytes.a = cur_latlng_msg.CUR_LAT;
            uart3_putchar(0x4C, 0); //send an 'L' start of frame
            for(int i = 0; i < 4; i++)
            {
                uart3_putchar(float_to_bytes.bytes[i],0);
            }
            float_to_bytes.a = cur_latlng_msg.CUR_LONG;
            for(int i = 0; i < 4; i++)
            {
                uart3_putchar(float_to_bytes.bytes[i],0);
            }
        }
        else if(can_msg.msg_id == TARGET_CAN_MID)
        {
            dbc_decode_GPS_TARGET_HEADING(&cur_target_msg, can_msg.data.bytes, &can_msg_hdr);
            //led_display_set(cur_target_msg.TARGET_HEADING/10);
            float_to_bytes.a = cur_target_msg.TARGET_HEADING;
            uart3_putchar(0x5A, 0); //send a 'Z' start of frame
            for(int i = 0; i < 4; i++)
            {
                uart3_putchar(float_to_bytes.bytes[i],0);
            }
            float_to_bytes.a = cur_target_msg.DISTANCE;
            for(int i = 0; i < 4; i++)
            {
                uart3_putchar(float_to_bytes.bytes[i],0);
            }
        }
        else if(can_msg.msg_id == SENSORS_CAN_MID)
        {
            led_set(3, false);
            dbc_decode_SENSOR_READINGS(&cur_sensor_status_msg, can_msg.data.bytes, &can_msg_hdr);
            /**
             * TRANSMIT TO PHONE: SENSORS
             */
            //send S -> left value (2bytes) -> front value (2bytes) -> right value (2bytes)
            uint8_t bytes[2] = {0};
            bytes[0] =  cur_sensor_status_msg.SENSOR_LEFT & 0xFF;
            bytes[1] = (cur_sensor_status_msg.SENSOR_LEFT >> 8) & 0xFF;
            uart3_putchar(0x53, 0); // 'S' start of sensor frame
            for(int i = 0; i < 2; i++)
            {
                uart3_putchar(bytes[i], 0);
            }
            bytes[0] = cur_sensor_status_msg.SENSOR_FRONT & 0xFF;
            bytes[1] = (cur_sensor_status_msg.SENSOR_FRONT >> 8) & 0xFF;
            for(int i = 0; i < 2; i++)
            {
                uart3_putchar(bytes[i], 0);
            }
            bytes[0] = cur_sensor_status_msg.SENSOR_RIGHT & 0xFF;
            bytes[1] = (cur_sensor_status_msg.SENSOR_RIGHT >> 8) & 0xFF;
            for(int i = 0; i < 2; i++)
            {
                uart3_putchar(bytes[i], 0);
            }
        }
    }

    if(dbc_handle_mia_COMPASS_CMD(&compass_cmd_msg, MIA_100HZ_INCR))
    {
        led_set(2, true);
    }

    if(dbc_handle_mia_GPS_CURRENT_LAT_LONG(&cur_latlng_msg, MIA_100HZ_INCR))
    {
        led_set(2, true);
    }

    if(dbc_handle_mia_SENSOR_READINGS(&cur_sensor_status_msg, MIA_100HZ_INCR))
    {
        led_set(2, true);
    }
}

void bridge_go(void)
{
    BRIDGE_GO_t bridge_go_msg = {0};
    dbc_encode_and_send_BRIDGE_GO(&bridge_go_msg);
}

void bridge_stop(void)
{
    BRIDGE_STOP_t bridge_stop_msg = {0};
    dbc_encode_and_send_BRIDGE_STOP(&bridge_stop_msg);
}

void bridge_CAN_tx_headlight(bool isOn)
{
    BRIDGE_HEADLIGHTS_t bridge_headlight_msg = {0};
    if(isOn)
    {
        bridge_headlight_msg.HEADLIGHT_VALUE = 1;
    }
    dbc_encode_and_send_BRIDGE_HEADLIGHTS(&bridge_headlight_msg);
}

void bridge_CAN_tx_dest(void)
{
    static BRIDGE_DEST_t bridge_dest_msg = {0};
    bridge_dest_msg.DEST_LAT = destLat;
    bridge_dest_msg.DEST_LNG = destLng;
    dbc_encode_and_send_BRIDGE_DEST(&bridge_dest_msg);
}

void bridge_CAN_tx_heartbeat(void)
{
    BRIDGE_HEARTBEAT_t bridge_heartbeat_msg = {1};
    dbc_encode_and_send_BRIDGE_HEARTBEAT(&bridge_heartbeat_msg);
}

void bridge_CAN_tx_init_debug(void)
{
    BRIDGE_INIT_DEBUG_t bridge_init_debug_msg = {0};
    dbc_encode_and_send_BRIDGE_INIT_DEBUG(&bridge_init_debug_msg);
}

bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8])
{
    can_msg_t can_msg = { 0 };
    can_msg.msg_id                = mid;
    can_msg.frame_fields.data_len = dlc;
    memcpy(can_msg.data.bytes, bytes, dlc);

    return CAN_tx(can1, &can_msg, 0);
}



#endif
