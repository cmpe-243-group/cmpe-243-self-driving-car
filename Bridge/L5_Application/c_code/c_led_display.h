#ifndef C_LED_DISPLAY_H_
#define C_LED_DISPLAY_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

bool led_display_init(void);
void led_display_set(char num);
void led_display_set_right_digit(char alpha);
void led_display_set_left_digit(char alpha);

#ifdef __cplusplus
}
#endif
#endif /* C_LED_DISPLAY_H_ */
