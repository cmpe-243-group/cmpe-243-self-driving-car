#include "c_switches.h"
#include "singleton_template.hpp"
#include "switches.hpp"

bool switches_init()
{
    return Switches::getInstance().init();
}

bool switches_get(int num)
{
    return Switches::getInstance().getSwitch(num);
}
