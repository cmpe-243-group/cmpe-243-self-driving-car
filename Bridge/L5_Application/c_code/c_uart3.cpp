#include "c_uart3.h"
#include "uart3.hpp"

bool uart3_init(unsigned int baudRate, int rxQSize, int txQSize)
{
    return Uart3::getInstance().init(baudRate, rxQSize, txQSize);
}

bool uart3_getchar(char *byte, uint32_t timeout_ms)
{
    return Uart3::getInstance().getChar(byte, timeout_ms);
}

void uart3_putchar(char byte,  unsigned int timeout)
{
    Uart3::getInstance().putChar(byte, timeout);
}

void uart3_putline(const char* pBuff, unsigned int timeout=0xffffffff)
{
    Uart3::getInstance().putline(pBuff, timeout);
}





