/*
 * c_gpio.h
 *
 *  Created on: May 20, 2019
 *      Author: Kevin
 */

#ifndef C_GPIO_H_
#define C_GPIO_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

void headlight_init();
void headlight_set(bool on);

#ifdef __cplusplus
}
#endif
#endif /* C_GPIO_H_ */
