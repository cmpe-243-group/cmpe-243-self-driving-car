/*
 * bridge.h
 *
 *  Created on: Mar 29, 2019
 *      Author: Kevin
 */

#ifndef BRIDGE_H_
#define BRIDGE_H_
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
#include "printf_lib.h"
#include "c_led.h"
#include "c_led_display.h"
#include "c_uart3.h"
#include "can.h"
#include "c_gpio.h"
#include "_can_dbc/generated_can.h"

//init-related constants
#define UART_INIT_BAUDRATE  9600
#define UART_RX_QUEUE_SIZE  250
#define UART_TX_QUEUE_SIZE  250

//CAN-related constants
#define CAN_BAUDRATE        100
#define CAN_RX_QUEUE_SIZE   200
#define CAN_TX_QUEUE_SIZE   200
#define RPM_CAN_MID         105
#define HEADING_CAN_MID     110
#define LATLNG_CAN_MID      120
#define GPS_FIX_MID         130
#define TARGET_CAN_MID      145
#define CHECKPOINT_IDX_MID  150
#define SENSORS_CAN_MID     200
#define MIA_100HZ_INCR      10

#define LATLNG_DEBUG


union {
    float a;
    char bytes[4];
} float_to_bytes;

union{
    char bytes[4];
    float a;
} bytes_to_float;


union
{
  uint16_t a;
  char bytes[4];
} int_to_bytes;

bool bridge_init(void);
void bridge_1Hz(void);
void bridge_10Hz(void);
void bridge_go(void);
void bridge_stop(void);
void bridge_CAN_tx_headlight(bool isOn);
void bridge_CAN_tx_dest(void);
void bridge_CAN_tx_heartbeat(void);
void bridge_CAN_tx_init_debug(void);




#endif /* BRIDGE_H_ */
