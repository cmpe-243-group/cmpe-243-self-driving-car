#include "c_gpio.h"
#include "gpio.hpp"

GPIO headlight(P2_0);

void headlight_init()
{
    headlight.setAsOutput();
    //headlight.enableOpenDrainMode();
    headlight.setLow();

}

void headlight_set(bool on)
{
    (on) ? headlight.setHigh() : headlight.setLow();
}


